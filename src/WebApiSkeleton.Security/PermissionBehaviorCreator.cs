using WebApiSkeleton.Contracts.Base.ContractFeatureInterfaces;
using WebApiSkeleton.Security.PipelineBehaviors;

namespace WebApiSkeleton.Security;

/// <summary>
/// Class that provides <see cref="PermissionBehavior{TRequest,TResponse}"/> implementation for the given request type
/// </summary>
public static class PermissionBehaviorCreator
{
    public static (Type pipelineBehavior, Type permissionBehavior) GetPermissionPipelineBehaviorForRequest(
        Type requestType)
    {
        if (requestType.GetInterface(nameof(IAuthorizedRequest)) is null)
            throw new InvalidOperationException(
                "Cannot generate a permission behavior for non-authorizable request type");

        var resultTypeResponseInterface = requestType.GetInterface(typeof(IResultTypeResponseRequest<>).Name);
        if (resultTypeResponseInterface is null)
            throw new InvalidOperationException(
                $"Request that implements {nameof(IAuthorizedRequest)} must have a Result<T> return type: {requestType.Name}");

        var responseType = resultTypeResponseInterface.GetGenericArguments().First();
        var resultResponseType = typeof(Result<>).MakeGenericType(responseType);

        return (typeof(IPipelineBehavior<,>).MakeGenericType(requestType, resultResponseType),
            typeof(PermissionBehavior<,>).MakeGenericType(requestType, responseType));
    }
}