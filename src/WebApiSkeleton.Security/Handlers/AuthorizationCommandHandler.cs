﻿using Microsoft.IdentityModel.Tokens;
using WebApiSkeleton.Security.Core.Enums;
using WebApiSkeleton.Security.Core.Models.Output.Response;

namespace WebApiSkeleton.Security.Handlers;

internal sealed class AuthorizationCommandHandler : IRequestHandler<AuthorizationCommand, Result<AuthorizationResponse>>
{
    private readonly IPublisher _publisher;
    private readonly ISecurityService _securityService;
    private readonly TokenValidationParameters _tokenValidationParameters;

    public AuthorizationCommandHandler(ISecurityService securityService,
        IPublisher publisher,
        TokenValidationParameters tokenValidationParameters)
    {
        _securityService = securityService;
        _publisher = publisher;
        _tokenValidationParameters = tokenValidationParameters;
    }

    public async Task<Result<AuthorizationResponse>> Handle(AuthorizationCommand request,
        CancellationToken cancellationToken)
    {
        var result = await _securityService.AuthorizeAsync(request.Login, request.Password, request.DeviceId,
            cancellationToken);
        if (result.Result is AuthenticationResult.Success)
            await _publisher.Publish(new UserAuthorizedNotification(request.Login, request.DeviceId), cancellationToken);

        return result;
    }
}