﻿using WebApiSkeleton.Security.Core.Models.Output.Response;

namespace WebApiSkeleton.Security.Handlers;

internal sealed class RefreshCommandHandler : IRequestHandler<RefreshCommand, Result<AuthorizationResponse>>
{
    private readonly ISecurityService _securityService;

    public RefreshCommandHandler(ISecurityService securityService)
    {
        _securityService = securityService;
    }

    public async Task<Result<AuthorizationResponse>> Handle(RefreshCommand request,
        CancellationToken cancellationToken)
    {
        return await _securityService.RefreshAsync(request.AccessToken, request.RefreshToken, request.DeviceId,
            cancellationToken);
    }
}