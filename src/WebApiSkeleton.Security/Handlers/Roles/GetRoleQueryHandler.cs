using WebApiSkeleton.Security.Core.Models.Output;

namespace WebApiSkeleton.Security.Handlers.Roles;

internal sealed class GetRoleQueryHandler : IRequestHandler<GetRoleQuery, Result<Role?>>
{
    private readonly IContractDataGetter _dataGetter;

    public GetRoleQueryHandler(IContractDataGetter dataGetter)
    {
        _dataGetter = dataGetter;
    }

    public async Task<Result<Role?>> Handle(GetRoleQuery request, CancellationToken cancellationToken)
    {
        return await _dataGetter.GetRoleAsync(request.RoleNameOrId);
    }
}