using WebApiSkeleton.Security.Cache.Permission;
using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Services.Output;

namespace WebApiSkeleton.Security.Handlers.Roles;

internal sealed class UpdateRoleNameCommandHandler : IRequestHandler<UpdateRoleNameCommand, Result<Role>>
{
    private readonly IRoleService _roleService;
    private readonly IPublisher _publisher;

    public UpdateRoleNameCommandHandler(IRoleService roleService,
        IPublisher publisher)
    {
        _roleService = roleService;
        _publisher = publisher;
    }

    public async Task<Result<Role>> Handle(UpdateRoleNameCommand request, CancellationToken cancellationToken)
    {
        var role = await request.OldRoleNameOrId.Match(
            name => _roleService.UpdateRoleNameAsync(name, request.NewName, cancellationToken),
            id => _roleService.UpdateRoleNameAsync(id, request.NewName, cancellationToken));
        await _publisher.Publish(new CacheNeedsRefreshingNotification(PermissionCacheTarget.RolePermission, role.Id),
            cancellationToken);
        return role;
    }
}