﻿using WebApiSkeleton.Security.Cache.Permission;

namespace WebApiSkeleton.Security.Handlers.Roles;

internal sealed class RoleHasPermissionsQueryHandler : IRequestHandler<RoleHasPermissionsQuery, Result<bool>>
{
    private readonly IContractDataGetter _dataGetter;
    private readonly IPermissionCacheAccessor _cacheAccessor;

    public RoleHasPermissionsQueryHandler(IPermissionCacheAccessor cacheAccessor, IContractDataGetter dataGetter)
    {
        _cacheAccessor = cacheAccessor;
        _dataGetter = dataGetter;
    }

    public async Task<Result<bool>> Handle(RoleHasPermissionsQuery request, CancellationToken cancellationToken)
    {
        var roleName = (await _dataGetter.GetRoleAsync(request.RoleNameOrId))?.Name;
        if (roleName is null)
            return false;

        return (await _cacheAccessor.CheckRoleHasPermissionsAsync(request.Permissions, roleName)).IsEmpty;
    }
}