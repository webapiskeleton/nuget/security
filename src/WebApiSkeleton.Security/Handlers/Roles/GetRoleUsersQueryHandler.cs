using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Services.Output;

namespace WebApiSkeleton.Security.Handlers.Roles;

internal sealed class GetRoleUsersQueryHandler : IRequestHandler<GetRoleUsersQuery, Result<ListResult<User>>>
{
    private readonly IRoleService _roleService;

    public GetRoleUsersQueryHandler(IRoleService roleService)
    {
        _roleService = roleService;
    }

    public async Task<Result<ListResult<User>>> Handle(GetRoleUsersQuery request, CancellationToken cancellationToken)
    {
        var roleId = await request.RoleNameOrId.Match(async name => (await _roleService.GetRoleAsync(name))!.Id,
            Task.FromResult);

        return await _roleService.GetRoleUsersAsync(roleId, request.Pagination);
    }
}