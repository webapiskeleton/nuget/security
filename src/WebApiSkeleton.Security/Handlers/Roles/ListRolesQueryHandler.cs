using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Services.Output;

namespace WebApiSkeleton.Security.Handlers.Roles;

internal sealed class ListRolesQueryHandler : IRequestHandler<ListRolesQuery, Result<ListResult<Role>>>
{
    private readonly IRoleService _roleService;

    public ListRolesQueryHandler(IRoleService roleService)
    {
        _roleService = roleService;
    }

    public async Task<Result<ListResult<Role>>> Handle(ListRolesQuery request,
        CancellationToken cancellationToken)
    {
        return await _roleService.ListRolesAsync(request.Pagination);
    }
}