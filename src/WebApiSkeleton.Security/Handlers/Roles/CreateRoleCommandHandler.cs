using WebApiSkeleton.Security.Cache.Permission;
using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Services.Output;

namespace WebApiSkeleton.Security.Handlers.Roles;

internal sealed class CreateRoleCommandHandler : IRequestHandler<CreateRoleCommand, Result<Role>>
{
    private readonly IRoleService _roleService;
    private readonly IPublisher _publisher;

    public CreateRoleCommandHandler(IRoleService roleService,
        IPublisher publisher)
    {
        _roleService = roleService;
        _publisher = publisher;
    }

    public async Task<Result<Role>> Handle(CreateRoleCommand request, CancellationToken cancellationToken)
    {
        var role = await _roleService.CreateRoleAsync(request.Name, cancellationToken);
        await _publisher.Publish(new CacheNeedsRefreshingNotification(PermissionCacheTarget.RolePermission, role.Id),
            cancellationToken);
        return role;
    }
}