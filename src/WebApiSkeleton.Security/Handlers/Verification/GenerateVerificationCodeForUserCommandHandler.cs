using WebApiSkeleton.Security.Core.Verification;

namespace WebApiSkeleton.Security.Handlers.Verification;

internal sealed class GenerateVerificationCodeForUserCommandHandler : IRequestHandler<
    GenerateVerificationCodeForUserCommand, Result<VerificationCodeInformation>>
{
    private readonly IContractDataGetter _dataGetter;
    private readonly IEmailVerifier _verifier;

    public GenerateVerificationCodeForUserCommandHandler(IContractDataGetter dataGetter,
        IEmailVerifier verifier)
    {
        _dataGetter = dataGetter;
        _verifier = verifier;
    }

    public async Task<Result<VerificationCodeInformation>> Handle(GenerateVerificationCodeForUserCommand request,
        CancellationToken cancellationToken)
    {
        var userId = (await _dataGetter.GetUserAsync(request.Finder))!.Id;
        return await _verifier.GenerateVerificationCodeForUserAsync(userId, cancellationToken);
    }
}