using WebApiSkeleton.Security.Core.Verification;

namespace WebApiSkeleton.Security.Handlers.Verification;

internal sealed class UseVerificationCodeCommandHandler : IRequestHandler<UseVerificationCodeCommand, Result<None>>
{
    private readonly IContractDataGetter _dataGetter;
    private readonly IEmailVerifier _verifier;

    public UseVerificationCodeCommandHandler(IEmailVerifier verifier, 
        IContractDataGetter dataGetter)
    {
        _verifier = verifier;
        _dataGetter = dataGetter;
    }

    public async Task<Result<None>> Handle(UseVerificationCodeCommand request, CancellationToken cancellationToken)
    {
        var userId = (await _dataGetter.GetUserAsync(request.Finder))!.Id;
        await _verifier.UseVerificationCodeAsync(userId, cancellationToken);
        return new None();
    }
}