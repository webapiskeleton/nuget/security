using WebApiSkeleton.Security.Core.Services.Output;

namespace WebApiSkeleton.Security.Handlers.Users;

internal sealed class RemoveUserClaimsCommandHandler : IRequestHandler<RemoveUserClaimsCommand, Result<None>>
{
    private readonly IContractDataGetter _dataGetter;
    private readonly IUserCommandService _commandService;

    public RemoveUserClaimsCommandHandler(IUserCommandService commandService, 
        IContractDataGetter dataGetter)
    {
        _commandService = commandService;
        _dataGetter = dataGetter;
    }

    public async Task<Result<None>> Handle(RemoveUserClaimsCommand request, CancellationToken cancellationToken)
    {
        var userId = (await _dataGetter.GetUserAsync(request.UserLoginOrId))!.Id;
        await _commandService.RemoveClaimsAsync(userId, request.ClaimNames, cancellationToken);
        return new None();
    }
}