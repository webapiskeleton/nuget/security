using WebApiSkeleton.Security.Cache.Permission;
using WebApiSkeleton.Security.Core.Services.Output;

namespace WebApiSkeleton.Security.Handlers.Users;

internal sealed class RemoveUserFromRoleCommandHandler : IRequestHandler<RemoveUserFromRoleCommand, Result<None>>
{
    private readonly IContractDataGetter _dataGetter;
    private readonly IUserCommandService _commandService;
    private readonly IRoleService _roleService;
    private readonly IPublisher _publisher;

    public RemoveUserFromRoleCommandHandler(IUserCommandService commandService, 
        IContractDataGetter dataGetter,
        IRoleService roleService, 
        IPublisher publisher)
    {
        _commandService = commandService;
        _dataGetter = dataGetter;
        _roleService = roleService;
        _publisher = publisher;
    }

    public async Task<Result<None>> Handle(RemoveUserFromRoleCommand request, CancellationToken cancellationToken)
    {
        var userId = (await _dataGetter.GetUserAsync(request.UserLoginOrId))!.Id;
        var role = (await _dataGetter.GetRoleAsync(request.RoleNameOrId))!;
        await _commandService.RemoveUserFromRoleAsync(userId, role, cancellationToken);

        await _publisher.Publish(new CacheNeedsRefreshingNotification(PermissionCacheTarget.UserRole, userId),
            cancellationToken);

        return new None();
    }
}