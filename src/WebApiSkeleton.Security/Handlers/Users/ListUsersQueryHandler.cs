using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Services.Output;

namespace WebApiSkeleton.Security.Handlers.Users;

internal sealed class ListUsersQueryHandler : IRequestHandler<ListUsersQuery, Result<ListResult<User>>>
{
    private readonly IUserReadService _readService;

    public ListUsersQueryHandler(IUserReadService readService)
    {
        _readService = readService;
    }

    public async Task<Result<ListResult<User>>> Handle(ListUsersQuery request, CancellationToken cancellationToken)
    {
        return await _readService.ListUsersAsync(request.ListParam);
    }
}