using WebApiSkeleton.Security.Cache.Permission;
using WebApiSkeleton.Security.Core.Services.Output;

namespace WebApiSkeleton.Security.Handlers.Users;

internal sealed class AddUserToRoleCommandHandler : IRequestHandler<AddUserToRoleCommand, Result<None>>
{
    private readonly IContractDataGetter _dataGetter;
    private readonly IUserCommandService _commandService;
    private readonly IRoleService _roleService;
    private readonly IPublisher _publisher;

    public AddUserToRoleCommandHandler(IUserCommandService commandService, 
        IContractDataGetter dataGetter,
        IRoleService roleService, 
        IPublisher publisher)
    {
        _commandService = commandService;
        _dataGetter = dataGetter;
        _roleService = roleService;
        _publisher = publisher;
    }

    public async Task<Result<None>> Handle(AddUserToRoleCommand request, CancellationToken cancellationToken)
    {
        var userId = (await _dataGetter.GetUserAsync(request.UserLoginOrId))!.Id;
        var role = await request.RoleNameOrId.Match(_roleService.GetRoleAsync, _roleService.GetRoleAsync);
        await _commandService.AddUserToRoleAsync(userId, role!, cancellationToken);

        await _publisher.Publish(new CacheNeedsRefreshingNotification(PermissionCacheTarget.UserRole, userId),
            cancellationToken);

        return new None();
    }
}