using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Services.Output;

namespace WebApiSkeleton.Security.Handlers.Users;

internal sealed class SetUserPasswordCommandHandler : IRequestHandler<SetUserPasswordCommand, Result<None>>
{
    private readonly IUserCommandService _commandService;
    private readonly IContractDataGetter _dataGetter;

    public SetUserPasswordCommandHandler(IUserCommandService commandService, 
        IContractDataGetter dataGetter)
    {
        _commandService = commandService;
        _dataGetter = dataGetter;
    }

    public async Task<Result<None>> Handle(SetUserPasswordCommand request, CancellationToken cancellationToken)
    {
        var userId = (await _dataGetter.GetUserAsync(request.UserLoginOrId))!.Id;
        await _commandService.SetUserPasswordAsync(new Password { Value = request.Password, UserId = userId },
            cancellationToken);
        return new None();
    }
}