﻿using WebApiSkeleton.Security.Cache.Permission;

namespace WebApiSkeleton.Security.Handlers.Users;

internal sealed class UserHasPermissionQueryHandler : IRequestHandler<UserHasPermissionsQuery, Result<bool>>
{
    private readonly IContractDataGetter _dataGetter;
    private readonly IPermissionCacheAccessor _cacheAccessor;

    public UserHasPermissionQueryHandler(IPermissionCacheAccessor cacheAccessor,
        IContractDataGetter dataGetter)
    {
        _cacheAccessor = cacheAccessor;
        _dataGetter = dataGetter;
    }

    public async Task<Result<bool>> Handle(UserHasPermissionsQuery request, CancellationToken cancellationToken)
    {
        var userId = (await _dataGetter.GetUserAsync(request.UserLoginOrId))?.Id;
        if (userId is null)
            return false;

        return (await _cacheAccessor.CheckUserHasPermissionsAsync(request.Permissions, userId)).IsEmpty;
    }
}