using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Services.Output;

namespace WebApiSkeleton.Security.Handlers.Users;

internal sealed class GetUserRolesQueryHandler : IRequestHandler<GetUserRolesQuery, Result<IEnumerable<Role>>>
{
    private readonly IUserReadService _readService;

    public GetUserRolesQueryHandler(IUserReadService readService)
    {
        _readService = readService;
    }

    public async Task<Result<IEnumerable<Role>>> Handle(GetUserRolesQuery request, CancellationToken cancellationToken)
    {
        var user = await request.UserLoginOrId.Match(login => _readService.GetUserAsync(login),
            id => _readService.GetUserAsync(id));
        return user is null
            ? Array.Empty<Role>()
            : new Result<IEnumerable<Role>>(await _readService.GetUserRolesAsync(user.Id));
    }
}