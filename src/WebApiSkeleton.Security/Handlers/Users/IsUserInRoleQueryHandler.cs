using WebApiSkeleton.Security.Cache.Permission;
using WebApiSkeleton.Security.Core.Services.Output;

namespace WebApiSkeleton.Security.Handlers.Users;

internal sealed class IsUserInRoleQueryHandler : IRequestHandler<IsUserInRoleQuery, Result<bool>>
{
    private readonly IPermissionCacheAccessor _cacheAccessor;
    private readonly IUserReadService _readService;
    private readonly IRoleService _roleService;

    public IsUserInRoleQueryHandler(IPermissionCacheAccessor cacheAccessor, IRoleService roleService,
        IUserReadService readService)
    {
        _cacheAccessor = cacheAccessor;
        _roleService = roleService;
        _readService = readService;
    }

    public async Task<Result<bool>> Handle(IsUserInRoleQuery request, CancellationToken cancellationToken)
    {
        var roleName = await request.RoleNameOrId.Match<Task<string?>>(Task.FromResult<string?>,
            async id => (await _roleService.GetRoleAsync(id))?.Name);
        if (roleName is null)
            return false;

        var userId = await request.UserLoginOrId.Match<Task<int?>>(
            async login => (await _readService.GetUserAsync(login))?.Id, id => Task.FromResult<int?>(id));
        if (userId is null)
            return false;

        return (await _cacheAccessor.CheckUserHasRolesAsync(new[] { roleName }, userId)).IsEmpty;
    }
}