using WebApiSkeleton.Security.Core.Services.Output;

namespace WebApiSkeleton.Security.Handlers.Users;

internal sealed class AddUserClaimsCommandHandler : IRequestHandler<AddUserClaimsCommand, Result<None>>
{
    private readonly IContractDataGetter _dataGetter;
    private readonly IUserCommandService _commandService;

    public AddUserClaimsCommandHandler(IUserCommandService commandService, IContractDataGetter dataGetter)
    {
        _commandService = commandService;
        _dataGetter = dataGetter;
    }

    public async Task<Result<None>> Handle(AddUserClaimsCommand request, CancellationToken cancellationToken)
    {
        var userId = (await _dataGetter.GetUserAsync(request.UserLoginOrId))!.Id;
        await _commandService.AddClaimsAsync(userId, request.Claims, cancellationToken);
        return new None();
    }
}