using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Services.Output;

namespace WebApiSkeleton.Security.Handlers.Users;

internal sealed class UpdateUserCommandHandler : IRequestHandler<UpdateUserCommand, Result<User>>
{
    private readonly IUserCommandService _commandService;

    public UpdateUserCommandHandler(IUserCommandService commandService)
    {
        _commandService = commandService;
    }

    public async Task<Result<User>> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
    {
        return await _commandService.UpdateUserAsync(request.User, cancellationToken);
    }
}