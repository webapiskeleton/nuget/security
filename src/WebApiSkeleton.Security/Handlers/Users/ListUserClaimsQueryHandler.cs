using System.Security.Claims;
using WebApiSkeleton.Security.Core.Services.Output;

namespace WebApiSkeleton.Security.Handlers.Users;

internal sealed class ListUserClaimsQueryHandler : IRequestHandler<ListUserClaimsQuery, Result<IEnumerable<Claim>>>
{
    private readonly IUserReadService _readService;

    public ListUserClaimsQueryHandler(IUserReadService readService)
    {
        _readService = readService;
    }

    public async Task<Result<IEnumerable<Claim>>> Handle(ListUserClaimsQuery request,
        CancellationToken cancellationToken)
    {
        var user = await request.UserLoginOrId.Match(login => _readService.GetUserAsync(login),
            id => _readService.GetUserAsync(id));
        return user is null
            ? Array.Empty<Claim>()
            : new Result<IEnumerable<Claim>>(await _readService.ListUserClaimsAsync(user.Id));
    }
}