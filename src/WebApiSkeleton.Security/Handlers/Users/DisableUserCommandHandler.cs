using WebApiSkeleton.Security.Core.Services.Output;

namespace WebApiSkeleton.Security.Handlers.Users;

internal sealed class DisableUserCommandHandler : IRequestHandler<DisableUserCommand, Result<None>>
{
    private readonly IUserCommandService _commandService;
    private readonly IContractDataGetter _dataGetter;

    public DisableUserCommandHandler(IUserCommandService commandService, 
        IContractDataGetter dataGetter)
    {
        _commandService = commandService;
        _dataGetter = dataGetter;
    }

    public async Task<Result<None>> Handle(DisableUserCommand request, CancellationToken cancellationToken)
    {
        var userId = (await _dataGetter.GetUserAsync(request.UserLoginOrId))!.Id;
        await _commandService.DisableUserLoginAsync(userId, cancellationToken);
        return new None();
    }
}