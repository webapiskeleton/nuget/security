using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Services.Output;

namespace WebApiSkeleton.Security.Handlers.Users;

internal sealed class GetUserQueryHandler : IRequestHandler<GetUserQuery, Result<User?>>
{
    private readonly IUserReadService _readService;

    public GetUserQueryHandler(IUserReadService readService)
    {
        _readService = readService;
    }

    public async Task<Result<User?>> Handle(GetUserQuery request, CancellationToken cancellationToken)
    {
        return await request.UserLoginOrId.Match(
            login => _readService.GetUserAsync(login),
            id => _readService.GetUserAsync(id));
    }
}