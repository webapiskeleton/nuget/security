using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Services.Output;

namespace WebApiSkeleton.Security.Handlers.Permissions;

internal sealed class
    ListRolePermissionsQueryHandler : IRequestHandler<ListRolePermissionsQuery, Result<IEnumerable<GrantedPermission>>>
{
    private readonly IContractDataGetter _dataGetter;
    private readonly IPermissionService _permissionService;

    public ListRolePermissionsQueryHandler(IPermissionService permissionService,
        IContractDataGetter dataGetter)
    {
        _permissionService = permissionService;
        _dataGetter = dataGetter;
    }

    public async Task<Result<IEnumerable<GrantedPermission>>> Handle(ListRolePermissionsQuery request,
        CancellationToken cancellationToken)
    {
        var roleId = (await _dataGetter.GetRoleAsync(request.RoleNameOrId))?.Id;

        if (roleId is null)
            return Array.Empty<GrantedPermission>();

        var res = await _permissionService.ListRolePermissionsAsync(roleId.Value);
        return new Result<IEnumerable<GrantedPermission>>(res);
    }
}