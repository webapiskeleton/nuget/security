﻿
using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Services.Output;

namespace WebApiSkeleton.Security.Handlers.Permissions;

internal sealed class ListPermissionsQueryHandler : IRequestHandler<ListPermissionsQuery, Result<ListResult<Permission>>>
{
    private readonly IPermissionService _permissionService;

    public ListPermissionsQueryHandler(IPermissionService permissionService)
    {
        _permissionService = permissionService;
    }

    public async Task<Result<ListResult<Permission>>> Handle(ListPermissionsQuery request, CancellationToken cancellationToken)
    {
        return await _permissionService.ListPermissionsAsync(request.ListParam);
    }
}