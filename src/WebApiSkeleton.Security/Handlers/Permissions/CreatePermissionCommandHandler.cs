using WebApiSkeleton.Security.Core.Services.Output;

namespace WebApiSkeleton.Security.Handlers.Permissions;

internal sealed class CreatePermissionCommandHandler : IRequestHandler<CreatePermissionCommand, Result<None>>
{
    private readonly IPermissionService _permissionService;

    public CreatePermissionCommandHandler(IPermissionService permissionService)
    {
        _permissionService = permissionService;
    }

    public async Task<Result<None>> Handle(CreatePermissionCommand request, CancellationToken cancellationToken)
    {
        await _permissionService.CreatePermissionAsync(request.Name, cancellationToken);
        return new None();
    }
}