using System.Diagnostics;
using WebApiSkeleton.Security.Cache.Permission;
using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Services.Output;

namespace WebApiSkeleton.Security.Handlers.Permissions;

internal sealed class RemovePermissionsCommandHandler : IRequestHandler<RemovePermissionsCommand, Result<None>>
{
    private readonly IContractDataGetter _dataGetter;
    private readonly IPermissionService _permissionService;
    private readonly IPublisher _publisher;

    public RemovePermissionsCommandHandler(IPermissionService permissionService, IContractDataGetter dataGetter,
        IPublisher publisher)
    {
        _permissionService = permissionService;
        _dataGetter = dataGetter;
        _publisher = publisher;
    }

    public async Task<Result<None>> Handle(RemovePermissionsCommand request, CancellationToken cancellationToken)
    {
        var entity = await request.UserOrRoleFinder.Match<Task<IGrantable>>(
            async userFinder => (await _dataGetter.GetUserAsync(userFinder))!,
            async roleFinder => (await _dataGetter.GetRoleAsync(roleFinder))!
        );
        await _permissionService.RemovePermissionAsync(entity, request.Permissions, cancellationToken);

        await _publisher.Publish(new CacheNeedsRefreshingNotification(entity switch
        {
            User => PermissionCacheTarget.UserPermission,
            Role => PermissionCacheTarget.RolePermission,
            _ => throw new UnreachableException()
        }, entity switch
        {
            User u => u.Id,
            Role r => r.Id,
            _ => throw new UnreachableException()
        }), cancellationToken);

        return new None();
    }
}