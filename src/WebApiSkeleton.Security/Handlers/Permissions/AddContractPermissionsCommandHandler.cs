using WebApiSkeleton.Security.Cache.Permission;
using WebApiSkeleton.Security.Core.Models.DTO;
using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Services.Output;
using WebApiSkeleton.Security.Settings;

namespace WebApiSkeleton.Security.Handlers.Permissions;

internal sealed class
    AddContractPermissionsCommandHandler : IRequestHandler<AddContractPermissionsCommand, Result<None>>
{
    private readonly ContractPermissionStorageSettings _contractPermissionSettings;
    private readonly IPermissionService _permissionService;
    private readonly IPublisher _publisher;

    public AddContractPermissionsCommandHandler(IPermissionService permissionService,
        ContractPermissionStorageSettings contractPermissionSettings,
        IPublisher publisher)
    {
        _permissionService = permissionService;
        _contractPermissionSettings = contractPermissionSettings;
        _publisher = publisher;
    }

    public async Task<Result<None>> Handle(AddContractPermissionsCommand request, CancellationToken cancellationToken)
    {
        var permissions = request.Permissions.Select(x => new PermissionDto(x));
        await _permissionService.AddPermissionAsync(new CustomPermission
        {
            CustomPermissionTypeName = _contractPermissionSettings.CustomPermissionTypeName,
            Key = request.ContractName
        }, permissions, cancellationToken);

        await _publisher.Publish(new CacheNeedsRefreshingNotification(PermissionCacheTarget.ContractPermission,
            EntityName: request.ContractName), cancellationToken);

        return new None();
    }
}