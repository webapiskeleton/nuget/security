using WebApiSkeleton.Security.Cache.Permission;
using WebApiSkeleton.Security.Core.Models.DTO;
using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Services.Output;
using WebApiSkeleton.Security.Settings;

namespace WebApiSkeleton.Security.Handlers.Permissions;

internal sealed class
    RemoveContractPermissionsCommandHandler : IRequestHandler<RemoveContractPermissionsCommand, Result<None>>
{
    private readonly ContractPermissionStorageSettings _contractPermissionSettings;
    private readonly IPermissionService _permissionService;
    private readonly IPublisher _publisher;

    public RemoveContractPermissionsCommandHandler(IPermissionService permissionService,
        ContractPermissionStorageSettings contractPermissionSettings,
        IPublisher publisher)
    {
        _permissionService = permissionService;
        _contractPermissionSettings = contractPermissionSettings;
        _publisher = publisher;
    }

    public async Task<Result<None>> Handle(RemoveContractPermissionsCommand request,
        CancellationToken cancellationToken)
    {
        var permissions = request.Permissions.Select(x => new PermissionDto(x));
        await _permissionService.RemovePermissionAsync(new CustomPermission
        {
            CustomPermissionTypeName = _contractPermissionSettings.CustomPermissionTypeName,
            Key = nameof(request.ContractName)
        }, permissions, cancellationToken);

        await _publisher.Publish(new CacheNeedsRefreshingNotification(PermissionCacheTarget.ContractPermission,
            EntityName: request.ContractName), cancellationToken);

        return new None();
    }
}