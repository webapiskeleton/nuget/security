using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Services.Output;

namespace WebApiSkeleton.Security.Handlers.Permissions;

internal sealed class
    ListUserPermissionsQueryHandler : IRequestHandler<ListUserPermissionsQuery, Result<IEnumerable<GrantedPermission>>>
{
    private readonly IContractDataGetter _dataGetter;
    private readonly IPermissionService _permissionService;

    public ListUserPermissionsQueryHandler(IPermissionService permissionService,
        IContractDataGetter dataGetter)
    {
        _permissionService = permissionService;
        _dataGetter = dataGetter;
    }

    public async Task<Result<IEnumerable<GrantedPermission>>> Handle(ListUserPermissionsQuery request,
        CancellationToken cancellationToken)
    {
        var userId = (await _dataGetter.GetUserAsync(request.UserLoginOrId))?.Id;
        if (userId is null)
            return Array.Empty<GrantedPermission>();

        var res = await _permissionService.ListUserPermissionsAsync(userId.Value, request.IncludeRolePermissions);
        return new Result<IEnumerable<GrantedPermission>>(res);
    }
}