using System.Diagnostics;
using WebApiSkeleton.Security.Cache.Permission;
using WebApiSkeleton.Security.Core.Services.Output;

namespace WebApiSkeleton.Security.Handlers.Internal;

internal sealed class CacheNeedsRefreshingNotificationHandler : INotificationHandler<CacheNeedsRefreshingNotification>
{
    private readonly ICacheRefresher _cacheRefresher;
    private readonly IUserReadService _userReadService;
    private readonly IRoleService _roleService;

    public CacheNeedsRefreshingNotificationHandler(ICacheRefresher cacheRefresher,
        IRoleService roleService,
        IUserReadService userReadService)
    {
        _cacheRefresher = cacheRefresher;
        _roleService = roleService;
        _userReadService = userReadService;
    }

    public async Task Handle(CacheNeedsRefreshingNotification notification,
        CancellationToken cancellationToken)
    {
        switch (notification.Target)
        {
            case PermissionCacheTarget.UserRole:
                if (notification.EntityId is > 0)
                {
                    var user = await _userReadService.GetUserAsync(notification.EntityId.Value);
                    if (user is not null)
                        await _cacheRefresher.RefreshUserRolesAsync(user, cancellationToken);
                    return;
                }

                await _cacheRefresher.RefreshUsersRolesAsync(cancellationToken);
                return;
            case PermissionCacheTarget.UserPermission:
                if (notification.EntityId is > 0)
                {
                    var user = await _userReadService.GetUserAsync(notification.EntityId.Value);
                    if (user is not null)
                        await _cacheRefresher.RefreshUserCacheAsync(user, cancellationToken);
                    return;
                }

                await _cacheRefresher.RefreshUsersCacheAsync(cancellationToken);
                return;
            case PermissionCacheTarget.RolePermission:
                if (notification.EntityId is > 0)
                {
                    var role = await _roleService.GetRoleAsync(notification.EntityId.Value);
                    if (role is not null)
                        await _cacheRefresher.RefreshRoleCacheAsync(role, cancellationToken);
                    return;
                }

                await _cacheRefresher.RefreshRolesCacheAsync(cancellationToken);
                return;
            case PermissionCacheTarget.ContractPermission:
                if (notification.EntityName is not null)
                {
                    await _cacheRefresher.RefreshContractPermissionsAsync(notification.EntityName, cancellationToken);
                    return;
                }

                await _cacheRefresher.RefreshContractsPermissionsAsync(cancellationToken);
                return;
            default:
                throw new UnreachableException();
        }
    }
}