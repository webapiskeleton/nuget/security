﻿using WebApiSkeleton.Security.Cache.Permission;
using WebApiSkeleton.Security.Core.Enums;
using WebApiSkeleton.Security.Core.Models.Output.Response;
using WebApiSkeleton.Security.Core.Services.Output;

namespace WebApiSkeleton.Security.Handlers;

internal sealed class RegistrationCommandHandler : IRequestHandler<RegistrationCommand, Result<AuthorizationResponse>>
{
    private readonly ISecurityService _securityService;
    private readonly IPublisher _publisher;
    private readonly IUserReadService _userReadService;

    public RegistrationCommandHandler(ISecurityService securityService,
        IPublisher publisher,
        IUserReadService userReadService)
    {
        _securityService = securityService;
        _publisher = publisher;
        _userReadService = userReadService;
    }

    public async Task<Result<AuthorizationResponse>> Handle(RegistrationCommand request,
        CancellationToken cancellationToken)
    {
        var result = await _securityService.RegisterAsync(request.Credentials, request.DeviceId, cancellationToken);

        if (result.Result is not AuthenticationResult.Success) return result;

        var user = await _userReadService.GetUserAsync(request.Credentials.Username);
        await _publisher.Publish(new UserRegisteredEvent(user!.Id, request.Credentials), cancellationToken);
        await _publisher.Publish(new CacheNeedsRefreshingNotification(PermissionCacheTarget.UserRole, user.Id),
            cancellationToken);
        return result;
    }
}