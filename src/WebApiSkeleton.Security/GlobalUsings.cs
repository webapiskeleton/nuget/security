﻿// Global using directives

global using LanguageExt.Common;
global using MediatR;
global using OneOf;
global using WebApiSkeleton.SearchUtilities;
global using WebApiSkeleton.Security.Cache.Contract;
global using WebApiSkeleton.Security.Contracts;
global using WebApiSkeleton.Security.Core.Services;
global using None = OneOf.Types.None;