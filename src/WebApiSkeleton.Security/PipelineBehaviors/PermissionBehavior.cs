﻿using System.Reflection;
using System.Security.Authentication;
using Microsoft.Extensions.DependencyInjection;
using WebApiSkeleton.Contracts.Base.ContractFeatureInterfaces;
using WebApiSkeleton.Security.Attributes;
using WebApiSkeleton.Security.AuthorizationDescription;
using WebApiSkeleton.Security.AuthorizationRules;
using WebApiSkeleton.Security.Cache.Permission;
using WebApiSkeleton.Security.Core.UserIdentity;
using WebApiSkeleton.Security.Settings;
using HashSetDifference =
    (System.Collections.Generic.HashSet<string> AddedEntries,
    System.Collections.Generic.HashSet<string> RemovedEntries);

namespace WebApiSkeleton.Security.PipelineBehaviors;

/// <summary>
/// Pipeline behavior that handles security requirements of <see cref="IAuthorizedRequest{TResponse}"/>
/// </summary>
/// <typeparam name="TRequest">Request type</typeparam>
/// <typeparam name="TResponse">Request response type</typeparam>
/// <returns>
/// <see cref="TResponse"/> if permission is granted,
/// otherwise <see cref="AuthenticationException"/> if user was not authenticated
/// or <see cref="UnauthorizedAccessException"/> if user has no permissions or roles to access request
/// </returns>
internal sealed class PermissionBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, Result<TResponse>>
    where TRequest : IAuthorizedRequest
{
    private static Result<TResponse> AuthenticationFailure =>
        new(new AuthenticationException("User is not authorized to use this request"));

    private static Result<TResponse> AuthorizationFailure(SecurityAccessDescriptor descriptor) =>
        new(new UnauthorizedAccessDescriptionException { Descriptor = descriptor });

    private readonly HashSet<string> _requiredPermissions = [];
    private readonly HashSet<string> _requiredRoles = [];
    private readonly HashSet<AppliedAuthorizationRuleDescription> _appliedRules = [];

    private readonly IUserIdentity _userIdentity;
    private readonly IServiceProvider _serviceProvider;
    private readonly IPermissionCacheAccessor _permissionCacheAccessor;
    private readonly ContractPermissionStorageSettings _contractPermissionSettings;

    public PermissionBehavior(IUserIdentity userIdentity,
        IPermissionCacheAccessor permissionCacheAccessor,
        ContractPermissionStorageSettings contractPermissionSettings,
        IServiceProvider serviceProvider)
    {
        _userIdentity = userIdentity;
        _permissionCacheAccessor = permissionCacheAccessor;
        _contractPermissionSettings = contractPermissionSettings;
        _serviceProvider = serviceProvider;
    }

    public async Task<Result<TResponse>> Handle(TRequest request,
        RequestHandlerDelegate<Result<TResponse>> next,
        CancellationToken cancellationToken)
    {
        if (!_userIdentity.IsAuthenticated)
        {
            return AuthenticationFailure;
        }

        await InitializeRequirements(request);
        var resultDescriptor = new SecurityAccessDescriptorBuilder()
            .WithRules(_appliedRules)
            .WithDescriptor(await _permissionCacheAccessor.CheckUserHasRolesAsync(_requiredRoles))
            .WithDescriptor(await _permissionCacheAccessor.CheckUserHasPermissionsAsync(_requiredPermissions))
            .Build();

        if (!resultDescriptor.IsEmpty)
        {
            return AuthorizationFailure(resultDescriptor);
        }

        return await next();
    }

    private async Task InitializeRequirements(TRequest request)
    {
        if (_contractPermissionSettings.StoreContractPermissions)
        {
            var contractPermissions =
                await _permissionCacheAccessor.ListContractRequiredPermissions(typeof(TRequest).Name);
            foreach (var permission in contractPermissions)
            {
                _requiredPermissions.Add(permission);
            }
        }

        HandleAttribute(typeof(TRequest).GetCustomAttribute<SecurityRequirementsAttribute>());

        var rules = _serviceProvider
            .GetServices<IAuthorizationRule<TRequest>>()
            .DistinctBy(x => x.Name);
        foreach (var rule in rules)
        {
            await ApplyAuthorizationRule(request, rule);
        }
    }

    private async Task ApplyAuthorizationRule(TRequest request, IAuthorizationRule<TRequest> rule)
    {
        var savedHashSets = new
        {
            SavedRoles = _requiredRoles.ToArray(), SavedPermissions = _requiredPermissions.ToArray()
        };
        var ruleHandleResult = await rule.HandleRule(request, _requiredRoles, _requiredPermissions);

        var roleChanges = GetDifferences(savedHashSets.SavedRoles, _requiredRoles);
        var permissionChanges = GetDifferences(savedHashSets.SavedPermissions, _requiredPermissions);

        var appliedRuleDescription =
            new AppliedAuthorizationRuleDescription(rule.Name, ruleHandleResult.AllowCommand, ruleHandleResult.Message)
            {
                PermissionChanges = permissionChanges.RemovedEntries
                    .Select(x => new RulePermission(x, RuleAction.Removed))
                    .Concat(permissionChanges.AddedEntries.Select(x => new RulePermission(x, RuleAction.Added)))
                    .ToArray(),
                RoleChanges = roleChanges.RemovedEntries.Select(x => new RuleRole(x, RuleAction.Removed))
                    .Concat(roleChanges.AddedEntries.Select(x => new RuleRole(x, RuleAction.Added)))
                    .ToArray()
            };
        _appliedRules.Add(appliedRuleDescription);
    }

    private static HashSetDifference GetDifferences(IReadOnlyCollection<string> initialCollection,
        IReadOnlyCollection<string> modifiedCollection)
    {
        var deletedEntries = new HashSet<string>(initialCollection);
        deletedEntries.ExceptWith(modifiedCollection);
        var addedEntries = new HashSet<string>(modifiedCollection);
        addedEntries.ExceptWith(initialCollection);
        return (addedEntries, deletedEntries);
    }

    private void HandleAttribute(SecurityRequirementsAttribute? attribute)
    {
        if (attribute is null)
            return;
        foreach (var role in attribute.RequiredRoles)
        {
            _requiredRoles.Add(role);
        }

        foreach (var permission in attribute.RequiredPermissions)
        {
            _requiredPermissions.Add(permission);
        }
    }
}