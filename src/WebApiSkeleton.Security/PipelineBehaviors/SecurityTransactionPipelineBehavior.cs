﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using WebApiSkeleton.Contracts.Base.ContractFeatureInterfaces;
using WebApiSkeleton.Security.Core.Data;
using WebApiSkeleton.Security.Core.Settings;

namespace WebApiSkeleton.Security.PipelineBehaviors;

public sealed class SecurityTransactionPipelineBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    where TRequest : BaseSecurityRequest, ITransactRequest
{
    private readonly IdentityDatabaseOptions _options;
    private readonly IdentityContext _context;
    private readonly ILogger<SecurityTransactionPipelineBehavior<TRequest, TResponse>> _logger;

    public SecurityTransactionPipelineBehavior(IServiceProvider serviceProvider,
        IdentityDatabaseOptions options,
        ILogger<SecurityTransactionPipelineBehavior<TRequest, TResponse>> logger)
    {
        _options = options;
        _logger = logger;
        _context = serviceProvider.GetRequiredService<IdentityContext>();
    }

    public async Task<TResponse> Handle(TRequest request, RequestHandlerDelegate<TResponse> next,
        CancellationToken cancellationToken)
    {
        if (!_options.UseRequestTransactions)
        {
            try
            {
                return await next();
            }
            catch (Exception)
            {
                _logger.LogError("Request {RequestType} that could potentially use transaction incurred an error",
                    typeof(TRequest).Name);
                throw;
            }
        }

        await using var transaction = await _context.Database.BeginTransactionAsync(cancellationToken);
        try
        {
            var result = await next();
            await transaction.CommitAsync(cancellationToken);
            return result;
        }
        catch (Exception)
        {
            await transaction.RollbackAsync(cancellationToken);
            throw;
        }
    }
}