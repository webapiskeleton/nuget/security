using Microsoft.Extensions.Logging;
using StackExchange.Redis;
using WebApiSkeleton.DistributeLockUtilities.Settings;
using WebApiSkeleton.Security.Core.Enums;
using WebApiSkeleton.Security.Core.Models.Output.Response;
using WebApiSkeleton.Security.Core.Settings;

namespace WebApiSkeleton.Security.PipelineBehaviors;

/// <summary>
/// Pipeline behavior that checks if user is being brute forced from a concrete device
/// </summary>
internal sealed class BruteforceDetectionPipelineBehavior :
    IPipelineBehavior<AuthorizationCommand, Result<AuthorizationResponse>>
{
    private const string BruteforceDetectionPrefix = "bruteforce-detection";

    private readonly IDatabase _redisDatabase;
    private readonly SecuritySettings _securitySettings;
    private readonly ILogger<BruteforceDetectionPipelineBehavior> _logger;

    public BruteforceDetectionPipelineBehavior(RedisSettings redisSettings,
        SecuritySettings securitySettings,
        ILogger<BruteforceDetectionPipelineBehavior> logger)
    {
        _securitySettings = securitySettings;
        _logger = logger;
        _redisDatabase = redisSettings.ConnectionMultiplexer.GetDatabase(redisSettings.DatabaseNumber);
    }

    public async Task<Result<AuthorizationResponse>> Handle(AuthorizationCommand request,
        RequestHandlerDelegate<Result<AuthorizationResponse>> next,
        CancellationToken cancellationToken)
    {
        var hashKey = $"{BruteforceDetectionPrefix}:{request.Login}:{request.DeviceId}";

        var isBruteForced = await HandleBruteForceCheck(hashKey);
        if (isBruteForced)
        {
            _logger.LogWarning("User with login {Login} is being bruteforced", request.Login);
            return AuthorizationResponse.BruteforceDetected();
        }

        var res = await next();
        var result = res.Match(
            authorizationResult => authorizationResult.Result,
            _ => AuthenticationResult.AuthorizationFailed);
        if (result is not AuthenticationResult.Success) 
            return res;

        await _redisDatabase.KeyDeleteAsync(hashKey);
        return res;
    }

    private async Task<bool> HandleBruteForceCheck(string hashKey)
    {
        var currentDateTicks = DateTime.Now.Ticks;
        var hashExists = await _redisDatabase.KeyExistsAsync(hashKey);
        await _redisDatabase.HashSetAsync(hashKey, currentDateTicks, RedisValue.EmptyString);
        if (!hashExists)
        {
            await _redisDatabase.KeyExpireAsync(hashKey, _securitySettings.BruteforceCooldown);
        }

        var existingHash = await _redisDatabase.HashGetAllAsync(hashKey);
        return existingHash.Length >= _securitySettings.BruteforceAfter;
    }
}