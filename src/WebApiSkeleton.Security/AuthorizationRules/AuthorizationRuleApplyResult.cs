namespace WebApiSkeleton.Security.AuthorizationRules;

/// <summary>
/// Result of <see cref="IAuthorizationRule{TRequest}"/> being handled
/// </summary>
public sealed record AuthorizationRuleApplyResult
{
    /// <summary>
    /// Does rule allow request execution
    /// </summary>
    /// <remarks>
    /// False should be returned if command is forbidden to execute in any scenario.
    /// True means that command is not forbidden to execute, but all requirements are checked anyways
    /// </remarks>
    public bool AllowCommand { get; init; } = true;
    
    /// <summary>
    /// Additional optional message on rule executed
    /// </summary>
    public string Message { get; init; } = string.Empty;
}