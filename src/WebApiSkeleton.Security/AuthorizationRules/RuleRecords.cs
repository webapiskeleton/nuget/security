namespace WebApiSkeleton.Security.AuthorizationRules;

/// <summary>
/// Description of an applied authorization rule
/// </summary>
/// <param name="RuleName"></param>
/// <param name="AllowExecution"></param>
/// <param name="Message"></param>
public record AppliedAuthorizationRuleDescription(string RuleName, bool AllowExecution, string? Message)
{
    /// <summary>
    /// Added or removed permission requirements
    /// </summary>
    public IReadOnlyCollection<RulePermission> PermissionChanges { get; init; } = [];

    /// <summary>
    /// Added or removed role requirements
    /// </summary>
    public IReadOnlyCollection<RuleRole> RoleChanges { get; init; } = [];
}

public record RulePermission(string PermissionName, RuleAction Action);

public record RuleRole(string RoleName, RuleAction Action);

public enum RuleAction
{
    Added,
    Removed
}