using WebApiSkeleton.Contracts.Base.ContractFeatureInterfaces;

namespace WebApiSkeleton.Security.AuthorizationRules;

/// <summary>
/// Authorization rule that can affect authorization process by modifying permissions
/// and roles required for request execution in <see cref="HandleRule"/> method
/// </summary>
/// <typeparam name="TRequest">Type of authorized request</typeparam>
/// <example>
/// To execute some command user needs permission named "A",
/// but if he applies the command on self then permission is not needed.
/// The authorization rule checks if user runs the request on self
/// and removes the permission from required list and command successfully runs:
/// <code>
/// public sealed class SelfExecutionAuthorizationRule : IAuthorizationRule&lt;SomeCommand&gt;
/// {
///     private readonly IUserIdentity _userIdentity;
///
///     public SelfExecutionAuthorizationRule(IUserIdentity userIdentity)
///     {
///         _userIdentity = userIdentity;
///     }
///     public string Name => "Self execution rule";
///
///     public Task&lt;AuthorizationRuleApplyResult&gt; HandleRule(TRequest request, HashSet&lt;string&gt; requiredRoles,
///                                                         HashSet&lt;string&gt; requiredPermissions)
///     {
///         if (request.UserId == _userIdentity.UserId)
///         {
///             return new AuthorizationRuleApplyResult();
///         }
/// 
///         requiredPermissions.Remove("A");
///         return new AuthorizationRuleApplyResult { Message = "Permission A removed" };
///     }
/// }
/// </code>
/// </example>
public interface IAuthorizationRule<in TRequest> where TRequest : IAuthorizedRequest
{
    /// <summary>
    /// The name of current rule
    /// </summary>
    public string Name { get; }

    /// <summary>
    /// Optional description for the rule
    /// </summary>
    public string? RuleDescription => null;

    public Task<AuthorizationRuleApplyResult> HandleRule(TRequest request, HashSet<string> requiredRoles,
        HashSet<string> requiredPermissions);
}