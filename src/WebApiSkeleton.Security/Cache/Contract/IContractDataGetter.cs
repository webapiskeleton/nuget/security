using WebApiSkeleton.Security.Contracts.Helpers;
using WebApiSkeleton.Security.Core.Models.Output;

namespace WebApiSkeleton.Security.Cache.Contract;

/// <summary>
/// Request-scoped user and role entity cache by its finders
/// </summary>
public interface IContractDataGetter
{
    /// <summary>
    /// Get user from cache if present, otherwise query from database
    /// </summary>
    /// <param name="finder">User login or ID to find them in database</param>
    /// <returns>Found user or null if does not exist</returns>
    public Task<User?> GetUserAsync(UserLoginOrIdFinder finder);
    
    /// <summary>
    /// Get role from cache if present, otherwise query from database
    /// </summary>
    /// <param name="finder">Role name or ID to find it in database</param>
    /// <returns>Found role or null if does not exist</returns>
    public Task<Role?> GetRoleAsync(RoleNameOrIdFinder finder);
}