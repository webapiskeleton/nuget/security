using System.Collections.Concurrent;
using WebApiSkeleton.Security.Contracts.Helpers;
using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Services.Output;

namespace WebApiSkeleton.Security.Cache.Contract;

/// <inheritdoc/>
internal sealed class ContractDataGetter : IContractDataGetter
{
    private readonly ConcurrentDictionary<int, User?> _userIdCache = new();
    private readonly ConcurrentDictionary<string, User?> _userLoginCache = new();

    private readonly ConcurrentDictionary<int, Role?> _roleIdCache = new();
    private readonly ConcurrentDictionary<string, Role?> _roleNameCache = new();

    private readonly IUserReadService _readService;
    private readonly IRoleService _roleService;

    public ContractDataGetter(IUserReadService readService, IRoleService roleService)
    {
        _readService = readService;
        _roleService = roleService;
    }

    /// <inheritdoc/>
    public async Task<User?> GetUserAsync(UserLoginOrIdFinder finder)
    {
        return await finder.Match(async login =>
            {
                if (_userLoginCache.TryGetValue(login, out var user))
                {
                    return user;
                }

                user = await _readService.GetUserAsync(login);
                if (user is not null)
                {
                    _userLoginCache.TryAdd(user.Email, user);
                    _userLoginCache.TryAdd(user.Username, user);
                    _userIdCache.TryAdd(user.Id, user);
                }
                else
                {
                    _userLoginCache.TryAdd(login, null);
                }

                return user;
            },
            async id =>
            {
                if (_userIdCache.TryGetValue(id, out var user))
                {
                    return user;
                }

                user = await _readService.GetUserAsync(id);
                _userIdCache.TryAdd(id, user);
                if (user is not null)
                {
                    _userLoginCache.TryAdd(user.Email, user);
                    _userLoginCache.TryAdd(user.Username, user);
                    _userIdCache.TryAdd(user.Id, user);
                }
                else
                {
                    _userIdCache.TryAdd(id, null);
                }

                return user;
            });
    }

    /// <inheritdoc/>
    public async Task<Role?> GetRoleAsync(RoleNameOrIdFinder finder)
    {
        return await finder.Match(async name =>
            {
                if (_roleNameCache.TryGetValue(name, out var role))
                {
                    return role;
                }

                role = await _roleService.GetRoleAsync(name);
                _roleNameCache.TryAdd(name, role);
                if (role is not null) _roleIdCache.TryAdd(role.Id, role);
                return role;
            },
            async id =>
            {
                if (_roleIdCache.TryGetValue(id, out var role))
                {
                    return role;
                }

                role = await _roleService.GetRoleAsync(id);
                _roleIdCache.TryAdd(id, role);
                if (role is not null) _roleNameCache.TryAdd(role.Name, role);
                return role;
            });
    }
}