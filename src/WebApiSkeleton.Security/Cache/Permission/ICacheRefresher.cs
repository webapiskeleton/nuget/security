using WebApiSkeleton.Security.Core.Models.Output;

namespace WebApiSkeleton.Security.Cache.Permission;

internal interface ICacheRefresher
{
    public Task RefreshRolesCacheAsync(CancellationToken cancellationToken = default);
    public Task RefreshRoleCacheAsync(Role role, CancellationToken cancellationToken = default);
    
    public Task RefreshUsersCacheAsync(CancellationToken cancellationToken = default);
    public Task RefreshUserCacheAsync(User user, CancellationToken cancellationToken = default);

    public Task RefreshUsersRolesAsync(CancellationToken cancellationToken = default);
    public Task RefreshUserRolesAsync(User user, CancellationToken cancellationToken = default);
    public Task RefreshContractsPermissionsAsync(CancellationToken cancellationToken);
    public Task RefreshContractPermissionsAsync(string contractName, CancellationToken cancellationToken);
}