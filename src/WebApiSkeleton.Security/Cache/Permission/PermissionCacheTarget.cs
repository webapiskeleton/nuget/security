namespace WebApiSkeleton.Security.Cache.Permission;

public enum PermissionCacheTarget
{
    UserRole,
    UserPermission,
    RolePermission,
    ContractPermission,
}