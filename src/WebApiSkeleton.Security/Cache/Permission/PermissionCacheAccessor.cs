using Microsoft.Extensions.Logging;
using StackExchange.Redis;
using WebApiSkeleton.DistributeLockUtilities.Settings;
using WebApiSkeleton.Security.AuthorizationDescription;
using WebApiSkeleton.Security.Core.Enums;
using WebApiSkeleton.Security.Core.UserIdentity;

namespace WebApiSkeleton.Security.Cache.Permission;

internal sealed class PermissionCacheAccessor : IPermissionCacheAccessor
{
    private readonly IUserIdentity _userIdentity;
    private readonly IDatabase _cacheDatabase;
    private readonly ILogger<PermissionCacheAccessor> _logger;

    public PermissionCacheAccessor(RedisSettings redisSettings,
        IUserIdentity userIdentity,
        ILogger<PermissionCacheAccessor> logger)
    {
        _userIdentity = userIdentity;
        _logger = logger;
        _cacheDatabase = redisSettings.ConnectionMultiplexer.GetDatabase(redisSettings.DatabaseNumber);
    }

    /// <inheritdoc/>
    public async Task<SecurityAccessDescriptor> CheckUserHasRolesAsync(IEnumerable<string> roles,
        int? userId = null)
    {
        var insufficientRoles = new HashSet<string>();
        userId ??= _userIdentity.UserId;
        var hashKey = SecurityCacheNamings.GetUserRoleCacheName(userId.Value);
        foreach (var roleName in roles)
        {
            if (await _cacheDatabase.HashGetAsync(hashKey, roleName) != RedisValue.Null) continue;
            insufficientRoles.Add(roleName);
        }

        return new SecurityAccessDescriptor { InsufficientRoles = insufficientRoles };
    }

    /// <inheritdoc/>
    public async Task<SecurityAccessDescriptor> CheckUserHasPermissionsAsync(IEnumerable<string> permissions,
        int? userId = null)
    {
        var allowedPermissions = new HashSet<string>();
        var insufficientPermissions = new HashSet<string>();
        var restrictedPermissions = new HashSet<string>();
        userId ??= _userIdentity.UserId;
        var userRoles = await _cacheDatabase.HashGetAllAsync(SecurityCacheNamings.GetUserRoleCacheName(userId.Value));
        foreach (var permission in permissions)
        {
            var permissionRestricted = false;
            foreach (var role in userRoles)
            {
                var permissionEntries = await GetRolePermissionEntries(permission, role.Name!);
                if (permissionEntries.Any(x => x.Value == (int)PermissionMode.Restrict))
                {
                    restrictedPermissions.Add(permission);
                    permissionRestricted = true;
                    break;
                }

                if (permissionEntries.All(x => x.Value != (int)PermissionMode.Allow))
                    insufficientPermissions.Add(permission);

                if (permissionEntries.Any(x => x.Value == (int)PermissionMode.Allow))
                    allowedPermissions.Add(permission);
            }

            if (permissionRestricted)
                continue;

            var userPermissionEntries = (await _cacheDatabase
                    .HashGetAllAsync(SecurityCacheNamings.GetUserCacheName(userId.Value)))
                .Where(x => x.Name == permission)
                .ToArray();
            if (userPermissionEntries.Any(x => x.Value == (int)PermissionMode.Restrict))
            {
                restrictedPermissions.Add(permission);
                continue;
            }

            if (userPermissionEntries.All(x => x.Value != (int)PermissionMode.Allow))
                insufficientPermissions.Add(permission);

            if (userPermissionEntries.Any(x => x.Value == (int)PermissionMode.Allow))
                allowedPermissions.Add(permission);
        }

        insufficientPermissions.ExceptWith(allowedPermissions);
        return new SecurityAccessDescriptor
        {
            InsufficientPermissions = insufficientPermissions,
            RestrictedPermissions = restrictedPermissions
        };
    }

    /// <inheritdoc/>
    public async Task<SecurityAccessDescriptor> CheckRoleHasPermissionsAsync(IEnumerable<string> permissions,
        string roleName)
    {
        var restrictedPermissions = new HashSet<string>();
        var insufficientPermissions = new HashSet<string>();
        foreach (var permission in permissions)
        {
            var rolePermissions = await GetRolePermissionEntries(permission, roleName);
            if (rolePermissions.Any(x => x.Value == (int)PermissionMode.Restrict))
            {
                restrictedPermissions.Add(permission);
                continue;
            }

            if (rolePermissions.All(x => x.Value != (int)PermissionMode.Allow))
            {
                insufficientPermissions.Add(permission);
            }
        }

        return new SecurityAccessDescriptor
        {
            InsufficientPermissions = insufficientPermissions,
            RestrictedPermissions = restrictedPermissions
        };
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<string>> ListContractRequiredPermissions(string contractName)
    {
        var hashKey = SecurityCacheNamings.GetContractCacheName(contractName);
        var permissions = await _cacheDatabase.HashGetAllAsync(hashKey);
        return permissions.Select(x => x.Name.ToString());
    }

    private async Task<HashEntry[]> GetRolePermissionEntries(string permission, string roleName)
    {
        var hashKey = SecurityCacheNamings.GetRoleCacheName(roleName);
        var permissions = await _cacheDatabase.HashGetAllAsync(hashKey);
        var permissionEntries = permissions.Where(x => x.Name == permission).ToArray();
        return permissionEntries;
    }
}