using WebApiSkeleton.Security.AuthorizationDescription;

namespace WebApiSkeleton.Security.Cache.Permission;

internal interface IPermissionCacheAccessor
{
    /// <summary>
    /// Checks if provided user has all of passed roles
    /// </summary>
    /// <param name="roles">Role names</param>
    /// <param name="userId">User ID</param>
    /// <returns>True if user has all roles</returns>
    /// <remarks>If userId is null checks for current User identity</remarks>
    public Task<SecurityAccessDescriptor> CheckUserHasRolesAsync(IEnumerable<string> roles, int? userId = null);

    /// <summary>
    /// Checks if provided user has all of passed permissions
    /// </summary>
    /// <param name="permissions">Permission names</param>
    /// <param name="userId">User ID</param>
    /// <returns>True if user has all permissions</returns>
    /// <remarks>If userId is null checks for current User identity</remarks>
    public Task<SecurityAccessDescriptor> CheckUserHasPermissionsAsync(IEnumerable<string> permissions, int? userId = null);

    /// <summary>
    /// Checks if provided role has all of passed permissions
    /// </summary>
    /// <param name="permissions">Permission names</param>
    /// <param name="roleName">Role name</param>
    /// <returns>True if role has all permissions</returns>
    public Task<SecurityAccessDescriptor> CheckRoleHasPermissionsAsync(IEnumerable<string> permissions, string roleName);

    /// <summary>
    /// Lists all permissions that are required to execute the request
    /// </summary>
    /// <param name="contractName">Name of the contract executed</param>
    public Task<IEnumerable<string>> ListContractRequiredPermissions(string contractName);
}