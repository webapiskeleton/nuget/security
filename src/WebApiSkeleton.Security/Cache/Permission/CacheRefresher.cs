using StackExchange.Redis;
using WebApiSkeleton.DistributeLockUtilities.Services;
using WebApiSkeleton.DistributeLockUtilities.Settings;
using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Services.Output;
using WebApiSkeleton.Security.Settings;
using Role = WebApiSkeleton.Security.Core.Models.Output.Role;

namespace WebApiSkeleton.Security.Cache.Permission;

internal sealed class CacheRefresher : ICacheRefresher
{
    private readonly IDatabase _cacheDatabase;
    private readonly IRoleService _roleService;
    private readonly IPermissionService _permissionService;
    private readonly IUserReadService _userReadService;
    private readonly IDistributedLockService _distributedLockService;
    private readonly ContractPermissionStorageSettings _contractPermissionSettings;
    private readonly IPublisher _publisher;

    public CacheRefresher(RedisSettings redisSettings,
        IPermissionService permissionService,
        IRoleService roleService,
        IUserReadService userReadService,
        IDistributedLockService distributedLockService,
        IPublisher publisher,
        ContractPermissionStorageSettings contractPermissionSettings)
    {
        _permissionService = permissionService;
        _roleService = roleService;
        _userReadService = userReadService;
        _distributedLockService = distributedLockService;
        _publisher = publisher;
        _contractPermissionSettings = contractPermissionSettings;
        _cacheDatabase = redisSettings.ConnectionMultiplexer.GetDatabase(redisSettings.DatabaseNumber);
    }

    public async Task RefreshRolesCacheAsync(CancellationToken cancellationToken = default)
    {
        var roles = await _roleService.ListRolesAsync(PaginationInfo.Empty);
        foreach (var role in roles.Items)
        {
            await RefreshRoleCacheAsync(role, cancellationToken);
        }
    }

    public async Task RefreshRoleCacheAsync(Role role, CancellationToken cancellationToken = default)
    {
        var lockKey = $"{nameof(CacheRefresher)}:{nameof(Role)}-{role.Name}";
        await _distributedLockService.WithDistributedLockAsync(lockKey, async () =>
        {
            var permissions = await _permissionService.ListRolePermissionsAsync(role.Id);
            var hashKey = SecurityCacheNamings.GetRoleCacheName(role.Name);
            var transaction = _cacheDatabase.CreateTransaction();
#pragma warning disable 4014
            transaction.KeyDeleteAsync(hashKey);
            foreach (var permission in permissions)
            {
                transaction.HashSetAsync(hashKey, permission.Name, (int)permission.Mode);
            }
#pragma warning restore CS4014

            await transaction.ExecuteAsync();
            await _publisher.Publish(
                new PermissionCacheUpdatedNotification(PermissionCacheTarget.RolePermission, role.Id),
                cancellationToken);
        }, false);
    }

    public async Task RefreshUsersCacheAsync(CancellationToken cancellationToken = default)
    {
        var users = await _userReadService.ListUsersAsync(new ListParam<User>());
        foreach (var user in users.Items)
        {
            await RefreshUserCacheAsync(user, cancellationToken);
        }
    }

    public async Task RefreshUserCacheAsync(User user, CancellationToken cancellationToken = default)
    {
        var lockKey = $"{nameof(CacheRefresher)}:{nameof(User)}-{user.Username}";
        await _distributedLockService.WithDistributedLockAsync(lockKey, async () =>
        {
            var hashKey = SecurityCacheNamings.GetUserCacheName(user.Id);
            var permissions = await _permissionService.ListUserPermissionsAsync(user.Id);
            var transaction = _cacheDatabase.CreateTransaction();
#pragma warning disable 4014
            transaction.KeyDeleteAsync(hashKey);
            foreach (var permission in permissions)
            {
                transaction.HashSetAsync(hashKey, permission.Name, (int)permission.Mode);
            }
#pragma warning restore 4014

            await transaction.ExecuteAsync();
            await _publisher.Publish(
                new PermissionCacheUpdatedNotification(PermissionCacheTarget.UserPermission, user.Id),
                cancellationToken);
        }, false);
    }

    public async Task RefreshUsersRolesAsync(CancellationToken cancellationToken = default)
    {
        var users = await _userReadService.ListUsersAsync(new ListParam<User>());
        foreach (var user in users.Items)
        {
            await RefreshUserRolesAsync(user, cancellationToken);
        }
    }

    public async Task RefreshUserRolesAsync(User user, CancellationToken cancellationToken = default)
    {
        const string lockKey = $"{nameof(CacheRefresher)}:user-role";

        await _distributedLockService.WithDistributedLockAsync(lockKey, async () =>
        {
            var hashKey = SecurityCacheNamings.GetUserRoleCacheName(user.Id);
            var roles = await _userReadService.GetUserRolesAsync(user.Id);

            var transaction = _cacheDatabase.CreateTransaction();
#pragma warning disable 4014
            transaction.KeyDeleteAsync(hashKey);
            foreach (var role in roles)
            {
                transaction.HashSetAsync(hashKey, role.Name, RedisValue.EmptyString);
            }
#pragma warning restore 4014

            await transaction.ExecuteAsync();
            await _publisher.Publish(new PermissionCacheUpdatedNotification(PermissionCacheTarget.UserRole, user.Id),
                cancellationToken);
        }, false);
    }

    public async Task RefreshContractsPermissionsAsync(CancellationToken cancellationToken)
    {
        const string lockKey = $"{nameof(CacheRefresher)}:contract-permission";

        await _distributedLockService.WithDistributedLockAsync(lockKey, async () =>
        {
            var permissions =
                (await _permissionService.ListCustomPermissionsAsync(new ListParam<CustomGrantedPermission>
                {
                    Filters =
                    [
                        new FilterDefinition<CustomGrantedPermission>(x => x.TypeName,
                            FilterOperand.Equals,
                            _contractPermissionSettings.CustomPermissionTypeName)
                    ]
                })).ToArray();

            var contractNames = permissions.Select(x => x.Key).Distinct().ToList();
            var transaction = _cacheDatabase.CreateTransaction();
#pragma warning disable 4014
            // clear old cache
            contractNames.ForEach(x =>
            {
                var hashKey = SecurityCacheNamings.GetContractCacheName(x);
                transaction.KeyDeleteAsync(hashKey);
            });
            foreach (var permission in permissions)
            {
                var hashKey = SecurityCacheNamings.GetContractCacheName(permission.Key);
                transaction.HashSetAsync(hashKey, permission.Name, (int)permission.Mode);
            }
#pragma warning restore 4014

            await transaction.ExecuteAsync();
            await _publisher.Publish(new PermissionCacheUpdatedNotification(PermissionCacheTarget.ContractPermission),
                cancellationToken);
        });
    }

    public async Task RefreshContractPermissionsAsync(string contractName, CancellationToken cancellationToken)
    {
        const string lockKey = $"{nameof(CacheRefresher)}:contract-permission";

        await _distributedLockService.WithDistributedLockAsync(lockKey, async () =>
        {
            var permissions =
                await _permissionService.ListCustomPermissionsAsync(new ListParam<CustomGrantedPermission>
                {
                    Filters =
                    [
                        new FilterDefinition<CustomGrantedPermission>(x => x.TypeName, FilterOperand.Equals,
                            _contractPermissionSettings.CustomPermissionTypeName),
                        new FilterDefinition<CustomGrantedPermission>(x => x.Key,
                            FilterOperand.Equals, contractName)
                    ]
                });

            var transaction = _cacheDatabase.CreateTransaction();
#pragma warning disable 4014
            var hashKey = SecurityCacheNamings.GetContractCacheName(contractName);
            transaction.KeyDeleteAsync(hashKey);
            foreach (var permission in permissions)
            {
                transaction.HashSetAsync(hashKey, permission.Name, (int)permission.Mode);
            }
#pragma warning restore 4014

            await transaction.ExecuteAsync();
            await _publisher.Publish(new PermissionCacheUpdatedNotification(PermissionCacheTarget.ContractPermission),
                cancellationToken);
        }, false);
    }
}