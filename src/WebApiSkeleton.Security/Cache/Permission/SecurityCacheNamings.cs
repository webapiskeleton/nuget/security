namespace WebApiSkeleton.Security.Cache.Permission;

internal sealed class SecurityCacheNamings
{
    public static string GetRoleCacheName(string roleName) => $"role-cache:{roleName}";
    public static string GetUserCacheName(int userId) => $"user-cache:{userId}";
    public static string GetUserRoleCacheName(int userId) => $"user-role-cache:{userId}";
    public static string GetContractCacheName(string contractName) => $"contract-cache:{contractName}";
}