using WebApiSkeleton.Contracts.Base.ContractFeatureInterfaces;
using WebApiSkeleton.Security.Contracts.Helpers;
using WebApiSkeleton.Security.Core.Settings;
using WebApiSkeleton.Security.Core.Verification;

namespace WebApiSkeleton.Security.Contracts;

/// <summary>
/// Generates unique verification code and sessionId for given user, based on <see cref="VerificationSettings"/>
/// </summary>
/// <param name="Finder">User finder by login or ID</param>
public sealed record GenerateVerificationCodeForUserCommand(UserLoginOrIdFinder Finder)
    : IAuthorizedRequest<VerificationCodeInformation>, IValidatableRequest<VerificationCodeInformation>;

/// <summary>
/// Makes user's email verified
/// </summary>
/// <param name="Finder">User finder by login or ID</param>
/// <param name="VerificationCodeInformation">Verification code that is trying to be processed</param>
public sealed record UseVerificationCodeCommand(
    UserLoginOrIdFinder Finder,
    VerificationCodeInformation VerificationCodeInformation) : IAuthorizedRequest<None>, IValidatableRequest<None>;