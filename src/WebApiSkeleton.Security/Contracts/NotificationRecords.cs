using WebApiSkeleton.Security.Cache.Permission;
using WebApiSkeleton.Security.Core.Models.DTO;

namespace WebApiSkeleton.Security.Contracts;

/// <summary>
/// Notification that cached entities were changed and need to be updated
/// </summary>
/// <param name="Target"></param>
/// <param name="EntityId"></param>
/// <param name="EntityName"></param>
internal sealed record CacheNeedsRefreshingNotification(
    PermissionCacheTarget Target,
    int? EntityId = null,
    string? EntityName = null)
    : INotification;

/// <summary>
/// Notification about changing the cache of permissions
/// </summary>
/// <param name="Target"></param>
/// <param name="EntityId"></param>
/// <param name="EntityName"></param>
public sealed record PermissionCacheUpdatedNotification(
    PermissionCacheTarget Target,
    int? EntityId = null,
    string? EntityName = null)
    : INotification;

/// <summary>
/// User authorized (token pair is given)
/// </summary>
/// <param name="Login">User login (email or username)</param>
/// <param name="DeviceId">Device ID that has been authorized</param>
public sealed record UserAuthorizedNotification(string Login, string DeviceId) : INotification;

/// <summary>
/// User registered (entity created and token pair is given)
/// </summary>
/// <param name="UserId">Registered user ID</param>
/// <param name="Credentials">Credentials used to register user</param>
public sealed record UserRegisteredEvent(int UserId, SecurityRegistrationCredentials Credentials) : INotification;