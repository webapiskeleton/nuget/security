using WebApiSkeleton.Contracts.Base.ContractFeatureInterfaces;
using WebApiSkeleton.Security.Attributes;
using WebApiSkeleton.Security.Cache.Permission;
using WebApiSkeleton.Security.Contracts.Helpers;
using WebApiSkeleton.Security.Core.Models.DTO;
using WebApiSkeleton.Security.Core.Models.Output;

namespace WebApiSkeleton.Security.Contracts;

/// <summary>
/// List all permission types stored based on param
/// </summary>
/// <param name="ListParam">ListParam with filters, sort orders and pagination</param>
public sealed record ListPermissionsQuery(ListParam<Permission> ListParam) : IAuthorizedRequest<ListResult<Permission>>;

/// <summary>
/// List permissions of a given user
/// </summary>
/// <param name="UserLoginOrId">User finder by login or ID</param>
/// <param name="IncludeRolePermissions">Include user's role permissions in result</param>
public sealed record ListUserPermissionsQuery(UserLoginOrIdFinder UserLoginOrId, bool IncludeRolePermissions = false)
    : IAuthorizedRequest<IEnumerable<GrantedPermission>>;

/// <summary>
/// List permissions of a given role
/// </summary>
/// <param name="RoleNameOrId">Role find by name or ID</param>
public sealed record ListRolePermissionsQuery(RoleNameOrIdFinder RoleNameOrId)
    : IAuthorizedRequest<IEnumerable<GrantedPermission>>;

/// <summary>
/// Create permission with given name
/// </summary>
/// <param name="Name">Permission name</param>
public sealed record CreatePermissionCommand(string Name) : IAuthorizedRequest<None>, IValidatableRequest<None>;

/// <summary>
/// Add permissions to user or role
/// </summary>
/// <param name="UserOrRoleFinder">User or role finder</param>
/// <param name="Permissions">Permissions to add</param>
[UpdatesSecurityCache([PermissionCacheTarget.UserPermission, PermissionCacheTarget.RolePermission])]
public sealed record AddPermissionsCommand(UserOrRoleFinder UserOrRoleFinder, IEnumerable<PermissionDto> Permissions)
    : IAuthorizedRequest<None>, IValidatableRequest<None>;

/// <summary>
/// Remove permissions from user or role
/// </summary>
/// <param name="UserOrRoleFinder">User or role finder</param>
/// <param name="Permissions">Permissions to remove</param>
[UpdatesSecurityCache([PermissionCacheTarget.UserPermission, PermissionCacheTarget.RolePermission])]
public sealed record RemovePermissionsCommand(UserOrRoleFinder UserOrRoleFinder, IEnumerable<PermissionDto> Permissions)
    : IAuthorizedRequest<None>, IValidatableRequest<None>;

/// <summary>
/// Add permissions to contract
/// </summary>
/// <param name="ContractName">Contract name that permissions are added to</param>
/// <param name="Permissions">Permissions to add</param>
[UpdatesSecurityCache([PermissionCacheTarget.ContractPermission])]
public sealed record AddContractPermissionsCommand(string ContractName, IEnumerable<string> Permissions)
    : IAuthorizedRequest<None>;

/// <summary>
/// Remove permissions from contract
/// </summary>
/// <param name="ContractName">Contract name that permissions are removed from</param>
/// <param name="Permissions">Permissions to add</param>
[UpdatesSecurityCache([PermissionCacheTarget.ContractPermission])]
public sealed record RemoveContractPermissionsCommand(string ContractName, IEnumerable<string> Permissions)
    : IAuthorizedRequest<None>;