using WebApiSkeleton.Contracts.Base.ContractFeatureInterfaces;
using WebApiSkeleton.Security.Attributes;
using WebApiSkeleton.Security.Cache.Permission;
using WebApiSkeleton.Security.Core.Models.DTO;
using WebApiSkeleton.Security.Core.Models.Output.Response;

namespace WebApiSkeleton.Security.Contracts;

public abstract record BaseSecurityRequest(string DeviceId)
    : IValidatableRequest<AuthorizationResponse>, IDistributeLockableRequest, ITransactRequest;

/// <summary>
/// Authorize user by returning Access and Refresh token pair
/// </summary>
/// <param name="Login">User login (email or username)</param>
/// <param name="Password">User password</param>
/// <param name="DeviceId">Device ID to generate tokens for</param>
public sealed record AuthorizationCommand(string Login, string Password, string DeviceId)
    : BaseSecurityRequest(DeviceId);

/// <summary>
/// Refresh token pair by returning the new pair
/// </summary>
/// <param name="AccessToken">Previous access token</param>
/// <param name="RefreshToken">Previous refresh token</param>
/// <param name="DeviceId">Device ID to generate tokens for</param>
public sealed record RefreshCommand(string AccessToken, string RefreshToken, string DeviceId)
    : BaseSecurityRequest(DeviceId);

/// <summary>
/// Register user based on information from <see cref="Credentials"/> and generate token pair associated with <see cref="deviceId"/>
/// </summary>
/// <param name="Credentials">User information</param>
/// <param name="DeviceId">Device ID to generate tokens for</param>
[UpdatesSecurityCache([PermissionCacheTarget.UserRole])]
public sealed record RegistrationCommand(SecurityRegistrationCredentials Credentials, string DeviceId)
    : BaseSecurityRequest(DeviceId);