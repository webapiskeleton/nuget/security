using WebApiSkeleton.Contracts.Base.ContractFeatureInterfaces;
using WebApiSkeleton.Security.Attributes;
using WebApiSkeleton.Security.Cache.Permission;
using WebApiSkeleton.Security.Contracts.Helpers;
using WebApiSkeleton.Security.Core.Models.Output;

namespace WebApiSkeleton.Security.Contracts;

/// <summary>
/// Get the list of existing roles with pagination
/// </summary>
/// <param name="Pagination">Pagination info</param>
public sealed record ListRolesQuery(PaginationInfo Pagination) : IAuthorizedRequest<ListResult<Role>>;

/// <summary>
/// Get role information
/// </summary>
/// <param name="RoleNameOrId">Role finder by name or ID</param>
public sealed record GetRoleQuery(RoleNameOrIdFinder RoleNameOrId) : IAuthorizedRequest<Role?>;

/// <summary>
/// Get users that belong to the given role
/// </summary>
/// <param name="RoleNameOrId">Role finder by name or ID</param>
/// <param name="Pagination">Pagination parameters</param>
public sealed record GetRoleUsersQuery(RoleNameOrIdFinder RoleNameOrId, PaginationInfo Pagination)
    : IAuthorizedRequest<ListResult<User>>;

/// <summary>
/// Checks if specified role has required permissions
/// </summary>
/// <param name="RoleNameOrId">Role finder by name or ID</param>
/// <param name="Permissions">Permission names to check for</param>
public sealed record RoleHasPermissionsQuery(RoleNameOrIdFinder RoleNameOrId, IEnumerable<string> Permissions)
    : IAuthorizedRequest<bool>;

/// <summary>
/// Create role with given name
/// </summary>
/// <param name="Name">Role name</param>
[UpdatesSecurityCache([PermissionCacheTarget.RolePermission])]
public sealed record CreateRoleCommand(string Name)
    : IAuthorizedRequest<Role>, IValidatableRequest<Role>;

/// <summary>
/// Update the name of role
/// </summary>
/// <param name="OldRoleNameOrId">Role finder by old name or ID</param>
/// <param name="NewName">New name to set to role</param>
[UpdatesSecurityCache([PermissionCacheTarget.RolePermission])]
public sealed record UpdateRoleNameCommand(RoleNameOrIdFinder OldRoleNameOrId, string NewName)
    : IAuthorizedRequest<Role>, IValidatableRequest<Role>;