using System.Security.Claims;
using WebApiSkeleton.Contracts.Base.ContractFeatureInterfaces;
using WebApiSkeleton.Security.Attributes;
using WebApiSkeleton.Security.Cache.Permission;
using WebApiSkeleton.Security.Contracts.Helpers;
using WebApiSkeleton.Security.Core.Models.Output;

namespace WebApiSkeleton.Security.Contracts;

/// <summary>
/// Get user by ID
/// </summary>
/// <param name="UserLoginOrId">User finder by login or ID</param>
public sealed record GetUserQuery(UserLoginOrIdFinder UserLoginOrId) : IAuthorizedRequest<User?>;

/// <summary>
/// List users using given arguments
/// </summary>
/// <param name="ListParam">Search parameters</param>
public sealed record ListUsersQuery(ListParam<User> ListParam)
    : IAuthorizedRequest<ListResult<User>>;

/// <summary>
/// Get user claims
/// </summary>
/// <param name="UserLoginOrId">User finder by login or ID</param>
public sealed record ListUserClaimsQuery(UserLoginOrIdFinder UserLoginOrId) : IAuthorizedRequest<IEnumerable<Claim>>;

/// <summary>
/// Get roles that user belongs to
/// </summary>
/// <param name="UserLoginOrId">User finder by login or ID</param>
public sealed record GetUserRolesQuery(UserLoginOrIdFinder UserLoginOrId) : IAuthorizedRequest<IEnumerable<Role>>;

/// <summary>
/// Check if user belongs to a given role
/// </summary>
/// <param name="UserLoginOrId">User finder by login or ID</param>
/// <param name="RoleNameOrId">Role finder by name or ID</param>
public sealed record IsUserInRoleQuery(UserLoginOrIdFinder UserLoginOrId, RoleNameOrIdFinder RoleNameOrId)
    : IAuthorizedRequest<bool>;

/// <summary>
/// Checks if specified user has required permissions
/// </summary>
/// <param name="UserLoginOrId">User finder by login or ID</param>
/// <param name="Permissions">Permission names to check for</param>
public sealed record UserHasPermissionsQuery(UserLoginOrIdFinder UserLoginOrId, IEnumerable<string> Permissions)
    : IAuthorizedRequest<bool>;

/// <summary>
/// Update a user in database
/// </summary>
/// <param name="User">User with updated properties</param>
public sealed record UpdateUserCommand(User User) : IAuthorizedRequest<User>, IValidatableRequest<User>;

/// <summary>
/// Add claims to user
/// </summary>
/// <param name="UserLoginOrId">User finder by login or ID</param>
/// <param name="Claims">Claims to add</param>
public sealed record AddUserClaimsCommand(UserLoginOrIdFinder UserLoginOrId, IEnumerable<Claim> Claims)
    : IAuthorizedRequest<None>;

/// <summary>
/// Remove claims from user
/// </summary>
/// <param name="UserLoginOrId">User finder by login or ID</param>
/// <param name="ClaimNames">Claim keys to remove</param>
public sealed record RemoveUserClaimsCommand(UserLoginOrIdFinder UserLoginOrId, IEnumerable<string> ClaimNames)
    : IAuthorizedRequest<None>;

/// <summary>
/// Set a password for user
/// </summary>
/// <param name="UserLoginOrId">User finder by login or ID</param>
/// <param name="Password">Password to set</param>
public sealed record SetUserPasswordCommand(UserLoginOrIdFinder UserLoginOrId, string Password)
    : IAuthorizedRequest<None>, IValidatableRequest<None>;

/// <summary>
/// Disable the user to prevent him from logging
/// </summary>
/// <param name="UserLoginOrId">User finder by login or ID</param>
public sealed record DisableUserCommand(UserLoginOrIdFinder UserLoginOrId)
    : IAuthorizedRequest<None>, IValidatableRequest<None>;

/// <summary>
/// Add user to role provided
/// </summary>
/// <param name="UserLoginOrId">User finder by login or ID</param>
/// <param name="RoleNameOrId">Role to add user to</param>
[UpdatesSecurityCache([PermissionCacheTarget.UserRole])]
public sealed record AddUserToRoleCommand(UserLoginOrIdFinder UserLoginOrId, RoleNameOrIdFinder RoleNameOrId)
    : IAuthorizedRequest<None>, IValidatableRequest<None>;

/// <summary>
/// Remove user from role provided
/// </summary>
/// <param name="UserLoginOrId">User finder by login or ID</param>
/// <param name="RoleNameOrId">Role to add user from</param>
[UpdatesSecurityCache([PermissionCacheTarget.UserRole])]
public sealed record RemoveUserFromRoleCommand(UserLoginOrIdFinder UserLoginOrId, RoleNameOrIdFinder RoleNameOrId)
    : IAuthorizedRequest<None>, IValidatableRequest<None>;