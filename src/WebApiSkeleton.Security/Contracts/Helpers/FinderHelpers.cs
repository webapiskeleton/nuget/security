using System.Text.Json;
using System.Text.Json.Serialization;

namespace WebApiSkeleton.Security.Contracts.Helpers;

[GenerateOneOf]
[JsonConverter(typeof(NameOrIdRoleFinderConverter))]
public partial class RoleNameOrIdFinder : OneOfBase<string, int>;

[GenerateOneOf]
[JsonConverter(typeof(LoginOrIdUserFinderConverter))]
public partial class UserLoginOrIdFinder : OneOfBase<string, int>;

[GenerateOneOf]
[JsonConverter(typeof(UserOrRoleFinderConverter))]
public partial class UserOrRoleFinder : OneOfBase<UserLoginOrIdFinder, RoleNameOrIdFinder>;

file class NameOrIdRoleFinderConverter : JsonConverter<RoleNameOrIdFinder>
{
    public override RoleNameOrIdFinder? Read(ref Utf8JsonReader reader, Type typeToConvert,
        JsonSerializerOptions options)
    {
        throw new NotImplementedException();
    }

    public override void Write(Utf8JsonWriter writer, RoleNameOrIdFinder value, JsonSerializerOptions options)
    {
        value.Switch(writer.WriteStringValue, writer.WriteNumberValue);
    }
}

file class LoginOrIdUserFinderConverter : JsonConverter<UserLoginOrIdFinder>
{
    public override UserLoginOrIdFinder? Read(ref Utf8JsonReader reader, Type typeToConvert,
        JsonSerializerOptions options)
    {
        throw new NotImplementedException();
    }

    public override void Write(Utf8JsonWriter writer, UserLoginOrIdFinder value, JsonSerializerOptions options)
    {
        value.Switch(writer.WriteStringValue, writer.WriteNumberValue);
    }
}

file class UserOrRoleFinderConverter : JsonConverter<UserOrRoleFinder>
{
    public override UserOrRoleFinder? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        throw new NotImplementedException();
    }

    public override void Write(Utf8JsonWriter writer, UserOrRoleFinder value, JsonSerializerOptions options)
    {
        value.Switch(user => user.Switch(writer.WriteStringValue, writer.WriteNumberValue),
            role => role.Switch(writer.WriteStringValue, writer.WriteNumberValue));
    }
}