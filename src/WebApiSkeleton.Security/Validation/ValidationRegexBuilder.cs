using System.Text;
using System.Text.RegularExpressions;
using WebApiSkeleton.Security.Core.Settings;

namespace WebApiSkeleton.Security.Validation;

internal static class ValidationRegexBuilder
{
    public static Regex BuildUsernameRegex(UsernameValidationOptions options)
    {
        // Restrict using @ and newline by default, allow latin characters
        var builder = new StringBuilder("^[^@\n][A-z");
        if (options.AllowDigits)
        {
            builder.Append("0-9");
        }
        
        foreach (var characterRange in options.AllowedAdditionalCharacterRanges)
        {
            builder.Append($"{(char)characterRange.Start.Value}-{(char)characterRange.End.Value}");
        }

        foreach (var character in options.AllowedAdditionalCharacters)
        {
            builder.Append(character);
        }

        if (options.AllowUnderscore)
        {
            builder.Append('_');
        }

        builder.Append(']');
        var lengthLimiter = $"{{{options.MinimumLength},{options.MaximumLength}}}";
        builder.Append(lengthLimiter);
        builder.Append('$');
        return new Regex(builder.ToString());
    }

    public static Regex BuildPasswordRegex(PasswordValidationOptions options)
    {
        var builder = new StringBuilder(@"\b");
        if (options.RequireUppercase)
        {
            builder.Append("(?=.*[A-Z])");
        }
        
        if (options.RequireLowercase)
        {
            builder.Append("(?=.*[a-z])");
        }

        if (options.RequireDigit)
        {
            builder.Append("(?=.*[0-9])");
        }

        if (options.RequireNonAlphanumeric)
        {
            builder.Append(@"(?=.*[@$!%*#?&])");
        }

        builder.Append($"(?=.{{{options.MinimumLength},}})");
        builder.Append(@"\b");
        return new Regex(builder.ToString());
    }
}