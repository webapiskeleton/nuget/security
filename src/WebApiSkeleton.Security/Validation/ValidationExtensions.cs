using FluentValidation;
using WebApiSkeleton.Security.Contracts.Helpers;

namespace WebApiSkeleton.Security.Validation;

internal static class ValidationExtensions
{
    public static IRuleBuilderOptions<T, UserLoginOrIdFinder> UserMustExist<T>(
        this IRuleBuilder<T, UserLoginOrIdFinder> options, IContractDataGetter readService)
    {
        return options.MustAsync(async (loginOrId, _) =>
            await loginOrId.Match(
                login => readService.GetUserAsync(login),
                id => readService.GetUserAsync(id)) is not null);
    }

    public static IRuleBuilderOptions<T, RoleNameOrIdFinder> RoleMustExist<T>(
        this IRuleBuilder<T, RoleNameOrIdFinder> options, IContractDataGetter roleService)
    {
        return options.MustAsync(async (nameOrId, _) =>
            await nameOrId.Match(
                name => roleService.GetRoleAsync(name), 
                id => roleService.GetRoleAsync(id)) is not null);
    }
}