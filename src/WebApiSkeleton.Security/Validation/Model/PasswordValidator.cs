﻿using FluentValidation;
using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Settings;

namespace WebApiSkeleton.Security.Validation.Model;

internal sealed class PasswordValidator : AbstractValidator<Password>
{
    public PasswordValidator(ValidationSettings validationSettings)
    {
        RuleFor(x => x.Value)
            .MinimumLength(validationSettings.Password.MinimumLength)
            .WithMessage($"Password requirements not met: {validationSettings.Password}")
            .Matches(ValidationRegexBuilder.BuildPasswordRegex(validationSettings.Password))
            .WithMessage($"Password requirements not met: {validationSettings.Password}");
    }
}