﻿using System.Text.RegularExpressions;
using FluentValidation;
using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Services.Output;
using WebApiSkeleton.Security.Core.Settings;

namespace WebApiSkeleton.Security.Validation.Model;

internal sealed partial class UserValidator : AbstractValidator<User>
{
    private readonly Regex _emailRegex = EmailRegex();

    public UserValidator(ValidationSettings validationSettings,
        IUserReadService userReadService)
    {
        RuleFor(x => x.Username)
            .Cascade(CascadeMode.Continue)
            .MinimumLength(validationSettings.Username.MinimumLength)
            .WithMessage(
                $"Username is too short. Minimum length is {validationSettings.Username.MinimumLength} characters")
            .MaximumLength(validationSettings.Username.MaximumLength)
            .WithMessage(
                $"Username is too long. Maximum length is {validationSettings.Username.MaximumLength} characters")
            .Matches(ValidationRegexBuilder.BuildUsernameRegex(validationSettings.Username))
            .WithMessage("Username contains unknown characters");
        RuleFor(x => new { x.Username, x.Id })
            .MustAsync(async (userInfo, _) =>
            {
                var existingUser = await userReadService.GetUserAsync(userInfo.Username);
                return existingUser is null || existingUser.Id == userInfo.Id;
            })
            .WithMessage("User with this username already exists");

        RuleFor(x => x.Email)
            .Matches(_emailRegex)
            .WithMessage("Email has incorrect format");
        RuleFor(x => new { x.Email, x.Id })
            .MustAsync(async (userInfo, _) =>
            {
                var existingUser = await userReadService.GetUserAsync(userInfo.Email);
                return existingUser is null || existingUser.Id == userInfo.Id;
            })
            .WithMessage("User with this email already exists");

        RuleFor(x => new { x.PhoneNumber, x.Id })
            .MustAsync(async (userInfo, _) =>
            {
                var users = await userReadService.ListUsersAsync(new ListParam<User>
                {
                    Filters = new[]
                    {
                        new FilterDefinition<User>(x => x.PhoneNumber!, FilterOperand.Equals, userInfo.PhoneNumber)
                    }
                });
                return !users.Items.Any() || users.Items.Any(x => x.Id == userInfo.Id);
            })
            .When(x => x.PhoneNumber is not null);
    }

    [GeneratedRegex(@".+\@.+\..+")]
    private static partial Regex EmailRegex();
}