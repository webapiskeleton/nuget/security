using FluentValidation;
using FluentValidation.Results;
using Microsoft.Extensions.DependencyInjection;
using WebApiSkeleton.Security.Core.Models.Output;

namespace WebApiSkeleton.Security.Validation.Contract;

internal sealed class RegistrationCommandValidator : AbstractValidator<RegistrationCommand>
{
    private readonly IServiceProvider _serviceProvider;

    public RegistrationCommandValidator(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public override ValidationResult Validate(ValidationContext<RegistrationCommand> context)
    {
        var userValidationResult = _serviceProvider
            .GetRequiredService<IValidator<User>>()
            .Validate(new User
            {
                Username = context.InstanceToValidate.Credentials.Username,
                Email = context.InstanceToValidate.Credentials.Email,
                PhoneNumber = context.InstanceToValidate.Credentials.PhoneNumber
            });
        var passwordValidationResult = _serviceProvider
            .GetRequiredService<IValidator<Password>>()
            .Validate(new Password
            {
                Value = context.InstanceToValidate.Credentials.Password
            });

        return new ValidationResult(userValidationResult.Errors.Concat(passwordValidationResult.Errors));
    }

    public override async Task<ValidationResult> ValidateAsync(ValidationContext<RegistrationCommand> context,
        CancellationToken cancellationToken = default)
    {
        var userValidationResult = await _serviceProvider
            .GetRequiredService<IValidator<User>>()
            .ValidateAsync(new User
            {
                Username = context.InstanceToValidate.Credentials.Username,
                Email = context.InstanceToValidate.Credentials.Email,
                PhoneNumber = context.InstanceToValidate.Credentials.PhoneNumber
            }, cancellationToken);
        var passwordValidationResult = await _serviceProvider
            .GetRequiredService<IValidator<Password>>()
            .ValidateAsync(new Password
            {
                Value = context.InstanceToValidate.Credentials.Password
            }, cancellationToken);

        return new ValidationResult(userValidationResult.Errors.Concat(passwordValidationResult.Errors));
    }
}