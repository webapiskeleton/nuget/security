using FluentValidation;
using WebApiSkeleton.Security.Core.Repository.Query;

namespace WebApiSkeleton.Security.Validation.Contract.Roles;

internal sealed class CreateRoleCommandValidator : AbstractValidator<CreateRoleCommand>
{
    public CreateRoleCommandValidator(IRoleQueryRepository repository)
    {
        RuleFor(x => x.Name)
            .MustAsync(async (name, _) => await repository.GetRoleAsync(name) is null)
            .WithMessage("Role with such name already exists");
    }
}