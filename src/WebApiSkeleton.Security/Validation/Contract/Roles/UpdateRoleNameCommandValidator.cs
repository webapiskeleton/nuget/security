using FluentValidation;
using WebApiSkeleton.Security.Core.Repository.Query;

namespace WebApiSkeleton.Security.Validation.Contract.Roles;

internal sealed class UpdateRoleNameCommandValidator : AbstractValidator<UpdateRoleNameCommand>
{
    public UpdateRoleNameCommandValidator(IRoleQueryRepository repository)
    {
        RuleFor(x => x.OldRoleNameOrId)
            .MustAsync(async (oldNameOrId, _) =>
                await oldNameOrId.Match(repository.GetRoleAsync, repository.GetRoleAsync) is not null
            )
            .WithMessage("There is no role with this name or id found");

        RuleFor(x => x.NewName)
            .MustAsync(async (name, _) => await repository.GetRoleAsync(name) is null)
            .WithMessage("Role with such name already exists");
    }
}