using FluentValidation;
using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.IdentityModel.Tokens;
using WebApiSkeleton.Security.Core.Repository.Query;
using WebApiSkeleton.Security.Core.Services.Impl;

namespace WebApiSkeleton.Security.Validation.Contract;

internal sealed class RefreshCommandValidator : AbstractValidator<RefreshCommand>
{
    public RefreshCommandValidator(IRefreshTokenQueryRepository repository,
        TokenValidationParameters tokenValidationParameters)
    {
        RuleFor(x => x)
            .CustomAsync(async (command, context, _) =>
            {
                var validatedToken = JwtUtils.GetPrincipalFromToken(command.AccessToken, tokenValidationParameters);

                if (validatedToken is null)
                {
                    context.AddFailure("Passed token is invalid");
                    return;
                }

                var refreshToken = await repository.GetTokenAsync(command.RefreshToken);
                var jti = validatedToken.Claims.Single(x => x.Type == JwtRegisteredClaimNames.Jti).Value;

                if (refreshToken is null || refreshToken.JwtId != jti)
                {
                    context.AddFailure("Passed token does not match with stored");
                    return;
                }

                if (refreshToken.ExpiryDate < DateTime.UtcNow)
                {
                    context.AddFailure("Token is expired");
                }
            });
    }
}