using FluentValidation;
using WebApiSkeleton.Security.Contracts.Helpers;

namespace WebApiSkeleton.Security.Validation.Contract.Permissions;

internal class RemovePermissionsCommandsValidator : AbstractValidator<RemovePermissionsCommand>
{
    public RemovePermissionsCommandsValidator(IContractDataGetter dataGetter)
    {
        RuleFor(x => (x.UserOrRoleFinder.Value as UserLoginOrIdFinder)!)
            .UserMustExist(dataGetter)
            .When(x => x.UserOrRoleFinder.Value is UserLoginOrIdFinder);
        RuleFor(x => (x.UserOrRoleFinder.Value as RoleNameOrIdFinder)!)
            .RoleMustExist(dataGetter)
            .When(x => x.UserOrRoleFinder.Value is RoleNameOrIdFinder);
    }
}