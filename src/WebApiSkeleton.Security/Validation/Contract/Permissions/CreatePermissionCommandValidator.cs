using FluentValidation;
using WebApiSkeleton.Security.Core.Repository.Query;

namespace WebApiSkeleton.Security.Validation.Contract.Permissions;

internal sealed class CreatePermissionCommandValidator : AbstractValidator<CreatePermissionCommand>
{
    public CreatePermissionCommandValidator(IPermissionQueryRepository repository)
    {
        RuleFor(x => x.Name)
            .MustAsync(async (name, _) => await repository.GetPermissionAsync(name) is null)
            .WithMessage("Permission with this name already exists");
    }
}