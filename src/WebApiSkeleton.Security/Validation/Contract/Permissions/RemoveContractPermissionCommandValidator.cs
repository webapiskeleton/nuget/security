using FluentValidation;
using WebApiSkeleton.Security.Settings;

namespace WebApiSkeleton.Security.Validation.Contract.Permissions;

internal sealed class RemoveContractPermissionCommandValidator : AbstractValidator<RemoveContractPermissionsCommand>
{
    public RemoveContractPermissionCommandValidator(ContractPermissionStorageSettings contractPermissionSettings)
    {
        RuleFor(x => contractPermissionSettings.StoreContractPermissions)
            .Must(x => x)
            .WithMessage($"{nameof(ContractPermissionStorageSettings.StoreContractPermissions)} is set to false");
    }
}