using FluentValidation;

namespace WebApiSkeleton.Security.Validation.Contract;

internal sealed class AuthorizationCommandValidator : AbstractValidator<AuthorizationCommand>
{
    public AuthorizationCommandValidator(IContractDataGetter userReadService)
    {
        RuleFor(x => x.Login)
            .Must(x => !string.IsNullOrEmpty(x))
            .WithMessage("Login cannot be empty or null")
            .MustAsync(async (login, _) => await userReadService.GetUserAsync(login) is not null)
            .WithMessage("Login or password are incorrect");
    }
}