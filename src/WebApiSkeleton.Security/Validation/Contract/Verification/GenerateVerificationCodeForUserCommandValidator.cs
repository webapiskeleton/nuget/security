using FluentValidation;
using WebApiSkeleton.Security.Core.Models.Output;

namespace WebApiSkeleton.Security.Validation.Contract.Verification;

internal sealed class GenerateVerificationCodeForUserCommandValidator
    : AbstractValidator<GenerateVerificationCodeForUserCommand>
{
    public GenerateVerificationCodeForUserCommandValidator(IContractDataGetter dataGetter)
    {
        User? user = null;
        RuleFor(x => x.Finder)
            .Cascade(CascadeMode.Stop)
            .MustAsync(async (loginOrId, _) =>
                (user = await dataGetter.GetUserAsync(loginOrId)) is not null)
            .WithMessage("User does not exist")
            .Must(_ => !user!.EmailVerified)
            .WithMessage("User email is already verified");
    }
}