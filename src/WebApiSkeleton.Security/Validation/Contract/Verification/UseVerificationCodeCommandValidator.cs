using FluentValidation;
using FluentValidation.Results;
using WebApiSkeleton.Security.Core.Repository.Query;
using WebApiSkeleton.Security.Core.Settings;

namespace WebApiSkeleton.Security.Validation.Contract.Verification;

internal sealed class UseVerificationCodeCommandValidator : AbstractValidator<UseVerificationCodeCommand>
{
    public UseVerificationCodeCommandValidator(IContractDataGetter dataGetter,
        IIdentityUserQueryRepository queryRepository,
        VerificationSettings verificationSettings)
    {
        RuleFor(x => x)
            .CustomAsync(async (command, context, _) =>
            {
                var user = await dataGetter.GetUserAsync(command.Finder);
                if (user is null)
                {
                    context.AddFailure(new ValidationFailure(nameof(UseVerificationCodeCommand.Finder),
                        "User does not exist"));
                    return;
                }

                var existingCode =
                    await queryRepository.GetVerificationCodeInformationAsync(user.Id,
                        command.VerificationCodeInformation.SessionId);

                if (existingCode is null || DateTime.UtcNow - existingCode.CreatedAt >
                    verificationSettings.VerificationCodeTimeToLive)
                {
                    context.AddFailure(new ValidationFailure(
                        nameof(UseVerificationCodeCommand.VerificationCodeInformation),
                        "Verification code expired"));
                    return;
                }

                var success = existingCode.VerificationCode == command.VerificationCodeInformation.VerificationCode;
                if (!success)
                {
                    context.AddFailure(nameof(UseVerificationCodeCommand.VerificationCodeInformation),
                        "Wrong verification code");
                }
            });
    }
}