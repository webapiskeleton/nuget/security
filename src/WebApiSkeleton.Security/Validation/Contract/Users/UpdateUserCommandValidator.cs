using FluentValidation;
using FluentValidation.Results;
using Microsoft.Extensions.DependencyInjection;
using WebApiSkeleton.Security.Core.Models.Output;

namespace WebApiSkeleton.Security.Validation.Contract.Users;

internal sealed class UpdateUserCommandValidator : AbstractValidator<UpdateUserCommand>
{
    private readonly IServiceProvider _serviceProvider;
    public UpdateUserCommandValidator(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public override ValidationResult Validate(ValidationContext<UpdateUserCommand> context)
    {
        return _serviceProvider
            .GetRequiredService<IValidator<User>>()
            .Validate(new User
            {
                Username = context.InstanceToValidate.User.Username,
                Email = context.InstanceToValidate.User.Email,
                PhoneNumber = context.InstanceToValidate.User.PhoneNumber,
                Id = context.InstanceToValidate.User.Id,
                EmailVerified = context.InstanceToValidate.User.EmailVerified,
                IsEnabled = context.InstanceToValidate.User.IsEnabled
            });
    }

    public override async Task<ValidationResult> ValidateAsync(ValidationContext<UpdateUserCommand> context, CancellationToken cancellation = new CancellationToken())
    {
        return await _serviceProvider
            .GetRequiredService<IValidator<User>>()
            .ValidateAsync(new User
            {
                Username = context.InstanceToValidate.User.Username,
                Email = context.InstanceToValidate.User.Email,
                PhoneNumber = context.InstanceToValidate.User.PhoneNumber,
                Id = context.InstanceToValidate.User.Id,
                EmailVerified = context.InstanceToValidate.User.EmailVerified,
                IsEnabled = context.InstanceToValidate.User.IsEnabled
            }, cancellation);
    }
}