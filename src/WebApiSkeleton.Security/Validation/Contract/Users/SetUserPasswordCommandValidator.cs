using FluentValidation;
using FluentValidation.Results;
using Microsoft.Extensions.DependencyInjection;
using WebApiSkeleton.Security.Core.Models.Output;

namespace WebApiSkeleton.Security.Validation.Contract.Users;

internal sealed class SetUserPasswordCommandValidator : AbstractValidator<SetUserPasswordCommand>
{
    private readonly IServiceProvider _serviceProvider;

    public SetUserPasswordCommandValidator(IServiceProvider serviceProvider, IContractDataGetter dataGetter)
    {
        _serviceProvider = serviceProvider;
        RuleFor(x => x.UserLoginOrId)
            .UserMustExist(dataGetter)
            .WithMessage("User does not exist");
    }

    public override ValidationResult Validate(ValidationContext<SetUserPasswordCommand> context)
    {
        var passwordValidationResult = _serviceProvider
            .GetRequiredService<IValidator<Password>>()
            .Validate(new Password { Value = context.InstanceToValidate.Password });
        var res = base.Validate(context);
        return new ValidationResult(passwordValidationResult.Errors.Concat(res.Errors));
    }

    public override async Task<ValidationResult> ValidateAsync(ValidationContext<SetUserPasswordCommand> context,
        CancellationToken cancellation = default)
    {
        var passwordValidationResult = await _serviceProvider
            .GetRequiredService<IValidator<Password>>()
            .ValidateAsync(new Password { Value = context.InstanceToValidate.Password }, cancellation);
        var res = await base.ValidateAsync(context, cancellation);
        return new ValidationResult(passwordValidationResult.Errors.Concat(res.Errors));
    }
}