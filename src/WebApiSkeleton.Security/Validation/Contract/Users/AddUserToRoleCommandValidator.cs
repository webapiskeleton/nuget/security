using FluentValidation;
using WebApiSkeleton.Security.Cache.Permission;

namespace WebApiSkeleton.Security.Validation.Contract.Users;

internal sealed class AddUserToRoleCommandValidator : AbstractValidator<AddUserToRoleCommand>
{
    public AddUserToRoleCommandValidator(IContractDataGetter dataGetter, IPermissionCacheAccessor cacheAccessor)
    {
        ClassLevelCascadeMode = CascadeMode.Stop;
        RuleFor(x => x.UserLoginOrId)
            .UserMustExist(dataGetter)
            .WithMessage("User not found");

        RuleFor(x => x.RoleNameOrId)
            .RoleMustExist(dataGetter)
            .WithMessage("Role not found");


        RuleFor(x => new { LoginOrId = x.UserLoginOrId, NameOrId = x.RoleNameOrId })
            .MustAsync(async (userAndRoleInfo, _) =>
            {
                var userId = (await dataGetter.GetUserAsync(userAndRoleInfo.LoginOrId))!.Id;
                var role = await dataGetter.GetRoleAsync(userAndRoleInfo.NameOrId);


                return !(await cacheAccessor.CheckUserHasRolesAsync([role!.Name], userId)).IsEmpty;
            })
            .WithMessage("User already belongs to this role");
    }
}