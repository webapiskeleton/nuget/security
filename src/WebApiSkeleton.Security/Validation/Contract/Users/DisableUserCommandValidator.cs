using FluentValidation;

namespace WebApiSkeleton.Security.Validation.Contract.Users;

internal sealed class DisableUserCommandValidator : AbstractValidator<DisableUserCommand>
{
    public DisableUserCommandValidator(IServiceProvider serviceProvider, IContractDataGetter dataGetter)
    {
        RuleFor(x => x.UserLoginOrId)
            .UserMustExist(dataGetter)
            .WithMessage("User does not exist");
    }
}