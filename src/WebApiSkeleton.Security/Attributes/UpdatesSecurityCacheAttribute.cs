using WebApiSkeleton.Security.Cache.Permission;

namespace WebApiSkeleton.Security.Attributes;

/// <summary>
/// Marks that the command can update security cache after being executed
/// </summary>
/// <param name="targets"></param>
internal class UpdatesSecurityCacheAttribute(PermissionCacheTarget[] targets) : Attribute
{
    public PermissionCacheTarget[] Targets { get; } = targets;
}