namespace WebApiSkeleton.Security.Attributes;

/// <summary>
/// Contains names or roles and permissions required to execute request
/// </summary>
[AttributeUsage(AttributeTargets.Class)]
public class SecurityRequirementsAttribute : Attribute
{
    public string[] RequiredRoles { get; init; } = Array.Empty<string>();
    public string[] RequiredPermissions { get; init; } = Array.Empty<string>();
}