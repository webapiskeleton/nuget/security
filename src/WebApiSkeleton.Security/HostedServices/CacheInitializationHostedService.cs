using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using WebApiSkeleton.Security.Cache.Permission;
using WebApiSkeleton.Security.Settings;

namespace WebApiSkeleton.Security.HostedServices;

/// <summary>
/// Hosted service that initializes role and permission cache
/// </summary>
internal sealed class CacheInitializationHostedService : IHostedService
{
    private readonly IServiceProvider _serviceProvider;
    private readonly ILogger<CacheInitializationHostedService> _logger;

    public CacheInitializationHostedService(IServiceProvider serviceProvider,
        ILogger<CacheInitializationHostedService> logger)
    {
        _serviceProvider = serviceProvider;
        _logger = logger;
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("Initializing cache on startup");
        using var scope = _serviceProvider.CreateScope();
        var cacheRefresher = scope.ServiceProvider.GetRequiredService<ICacheRefresher>();
        await InitializeCache(cacheRefresher, cancellationToken);
    }

    private async Task InitializeCache(ICacheRefresher cacheRefresher, CancellationToken cancellationToken = default)
    {
        _logger.LogInformation("Initializing user permission cache");
        await cacheRefresher.RefreshUsersCacheAsync(cancellationToken);
        
        _logger.LogInformation("Initializing role permission cache");
        await cacheRefresher.RefreshRolesCacheAsync(cancellationToken);

        _logger.LogInformation("Initializing user role cache");
        await cacheRefresher.RefreshUsersRolesAsync(cancellationToken);

        var contractPermissionSettings = _serviceProvider.GetRequiredService<ContractPermissionStorageSettings>();
        if (contractPermissionSettings.StoreContractPermissions)
        {
            _logger.LogInformation("Initializing contract permission cache");
            await cacheRefresher.RefreshContractsPermissionsAsync(cancellationToken);
        }
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }
}