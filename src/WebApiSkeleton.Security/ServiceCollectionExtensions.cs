﻿using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using WebApiSkeleton.Contracts.Base.ContractFeatureInterfaces;
using WebApiSkeleton.Contracts.Validation;
using WebApiSkeleton.Security.AuthorizationRules;
using WebApiSkeleton.Security.Cache.Permission;
using WebApiSkeleton.Security.Core.Models.Output.Response;
using WebApiSkeleton.Security.HostedServices;
using WebApiSkeleton.Security.PipelineBehaviors;
using WebApiSkeleton.Security.Settings;

namespace WebApiSkeleton.Security;

public static class ServiceCollectionExtensions
{
    /// <summary>
    /// Add security MediatR dependencies
    /// </summary>
    public static IServiceCollection AddSecurityMediatR(this IServiceCollection services, Action<ContractPermissionStorageSettings>? configurationAction = null)
    {
        services.TryAddScoped<ICacheRefresher, CacheRefresher>();
        services.TryAddScoped<IPermissionCacheAccessor, PermissionCacheAccessor>();
        services.AddHostedService<CacheInitializationHostedService>();
        services.TryAddScoped<IContractDataGetter, ContractDataGetter>();
        services.AddContractValidatorsFromAssembly(Assembly.GetExecutingAssembly(), includeInternalTypes: true);

        services.AddTransient(typeof(IPipelineBehavior<,>), typeof(SecurityTransactionPipelineBehavior<,>));
        services
            .AddTransient<IPipelineBehavior<AuthorizationCommand, Result<AuthorizationResponse>>,
                BruteforceDetectionPipelineBehavior>()
            .AddContractPermissionStorage(configurationAction);
        return services.AddMediatR(options =>
        {
            options.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly());
        });
    }

    /// <summary>
    /// Add authorization rule for request
    /// </summary>
    /// <typeparam name="TRequest">Request type that implements <see cref="IAuthorizedRequest{TResponse}"/></typeparam>
    /// <typeparam name="TAuthorizationRule">Authorization rule</typeparam>
    public static IServiceCollection AddPermissionAuthorizationRuleToRequest<TRequest, TAuthorizationRule>(this IServiceCollection services)
        where TRequest : IAuthorizedRequest where TAuthorizationRule : class, IAuthorizationRule<TRequest>
    {
        services.AddTransient<IAuthorizationRule<TRequest>, TAuthorizationRule>();
        return services;
    }


    private static IServiceCollection AddContractPermissionStorage(this IServiceCollection services,
        Action<ContractPermissionStorageSettings>? configurationAction = null)
    {
        var configuration = new ContractPermissionStorageSettings();
        configurationAction?.Invoke(configuration);

        services.AddSingleton(configuration);
        return services;
    }

    /// <summary>
    /// Add <see cref="PermissionBehavior{TRequest,TResponse}"/> for request in specified assemblies
    /// </summary>
    public static IServiceCollection AddPermissionPipelineBehaviorsForRequestsInAssemblies(
        this IServiceCollection services, params Assembly[] assemblies)
    {
        var securedRequests = assemblies
            .SelectMany(x => x.DefinedTypes)
            .Where(x => x.ImplementedInterfaces.Contains(typeof(IAuthorizedRequest)));
        foreach (var securedRequest in securedRequests)
        {
            var resultTypeResponseInterface = securedRequest.ImplementedInterfaces.FirstOrDefault(x =>
                x.IsGenericType && x.GetGenericTypeDefinition() == typeof(IResultTypeResponseRequest<>));
            if (resultTypeResponseInterface is null)
                throw new InvalidOperationException(
                    $"Request that implements {nameof(IAuthorizedRequest)} must have a Result<T> return type: {securedRequest.Name}");

            var responseType = resultTypeResponseInterface.GetGenericArguments().First();
            var resultResponseType = typeof(Result<>).MakeGenericType(responseType);

            services.AddTransient(typeof(IPipelineBehavior<,>).MakeGenericType(securedRequest, resultResponseType),
                typeof(PermissionBehavior<,>).MakeGenericType(securedRequest, responseType));
        }

        return services;
    }
}