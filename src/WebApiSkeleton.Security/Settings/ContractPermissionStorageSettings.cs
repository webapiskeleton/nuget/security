namespace WebApiSkeleton.Security.Settings;

public sealed class ContractPermissionStorageSettings
{
    public bool StoreContractPermissions = false;
    public string CustomPermissionTypeName = "ContractPermission";
}