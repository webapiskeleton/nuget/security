namespace WebApiSkeleton.Security.AuthorizationDescription;

public class UnauthorizedAccessDescriptionException : Exception
{
    public required SecurityAccessDescriptor Descriptor { get; init; }
}