using WebApiSkeleton.Security.AuthorizationRules;

namespace WebApiSkeleton.Security.AuthorizationDescription;

/// <summary>
/// Builder for <see cref="SecurityAccessDescriptor"/>
/// </summary>
public class SecurityAccessDescriptorBuilder
{
    private readonly HashSet<string> _insufficientRoles = [];
    private readonly HashSet<string> _insufficientPermissions = [];
    private readonly HashSet<string> _restrictedPermissions = [];
    private readonly HashSet<AppliedAuthorizationRuleDescription> _appliedRules = [];

    public SecurityAccessDescriptorBuilder WithRules(IEnumerable<AppliedAuthorizationRuleDescription> ruleDescriptions)
    {
        foreach (var ruleDescription in ruleDescriptions)
        {
            _appliedRules.Add(ruleDescription);
        }

        return this;
    }
    
    public SecurityAccessDescriptorBuilder WithDescriptor(SecurityAccessDescriptor descriptor)
    {
        foreach (var role in descriptor.InsufficientRoles)
        {
            _insufficientRoles.Add(role);
        }

        foreach (var permission in descriptor.InsufficientPermissions)
        {
            _insufficientPermissions.Add(permission);
        }

        foreach (var permission in descriptor.RestrictedPermissions)
        {
            _restrictedPermissions.Add(permission);
        }

        return this;
    }

    public SecurityAccessDescriptorBuilder WithRoles(IEnumerable<string> roles)
    {
        foreach (var role in roles)
        {
            _insufficientRoles.Add(role);
        }

        return this;
    }

    public SecurityAccessDescriptorBuilder WithInsufficientPermissions(IEnumerable<string> permissions)
    {
        foreach (var permission in permissions)
        {
            _insufficientPermissions.Add(permission);
        }

        return this;
    }

    public SecurityAccessDescriptorBuilder WithRestrictedPermissions(IEnumerable<string> permissions)
    {
        foreach (var permission in permissions)
        {
            _restrictedPermissions.Add(permission);
        }

        return this;
    }

    public SecurityAccessDescriptor Build()
    {
        return new SecurityAccessDescriptor
        {
            InsufficientPermissions = _insufficientPermissions,
            InsufficientRoles = _insufficientRoles,
            RestrictedPermissions = _restrictedPermissions,
            Rules = _appliedRules
        };
    }
}