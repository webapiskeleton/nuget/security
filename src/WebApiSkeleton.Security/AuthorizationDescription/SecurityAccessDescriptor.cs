using WebApiSkeleton.Security.AuthorizationRules;

namespace WebApiSkeleton.Security.AuthorizationDescription;

/// <summary>
/// Authorization descriptor that points to authorization failures during the request execution
/// </summary>
public record SecurityAccessDescriptor
{
    public IReadOnlyCollection<string> InsufficientRoles { get; init; } = [];
    public IReadOnlyCollection<string> InsufficientPermissions { get; init; } = [];
    public IReadOnlyCollection<string> RestrictedPermissions { get; init; } = [];

    /// <summary>
    /// Authorization rules applied in authorization process
    /// </summary>
    public IReadOnlyCollection<AppliedAuthorizationRuleDescription> Rules { get; init; } = [];

    public bool IsEmpty => InsufficientRoles.Count == 0 && InsufficientPermissions.Count == 0 &&
                           RestrictedPermissions.Count == 0 && Rules.All(x => x.AllowExecution);
}