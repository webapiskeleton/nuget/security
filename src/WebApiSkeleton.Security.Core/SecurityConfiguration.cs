﻿using System.Configuration;
using WebApiSkeleton.DistributeLockUtilities.Settings;
using WebApiSkeleton.Security.Core.Settings;

namespace WebApiSkeleton.Security.Core;

public sealed class SecurityConfiguration
{
    /// <summary>
    /// Add user claims to JWT
    /// </summary>
    public bool IncludeUserClaimsInJwt { get; set; } = false;

    /// <summary>
    /// Encrypt generated JWT using <see cref="SecurityAlgorithms.Aes256CbcHmacSha512"/>
    /// </summary>
    public bool UseJwtEncryption { get; set; } = true;
    
    public string? PasswordEncryptionKey { get; set; }

    /// <summary>
    /// Sets the <see cref="StackExchange.Redis.IConnectionMultiplexer"/> and database number to use redis locks
    /// </summary>
    /// <remarks>This is the required property. Validation will fail if this is null</remarks>
    public RedisSettings? RedisConnectionSettings { get; set; }

    public JwtSettings? JwtSettings { get; set; }

    /// <summary>
    /// Values used while validating creation/updating of models
    /// </summary>
    public ValidationSettings ValidationSettings { get; set; } = new();

    /// <summary>
    /// Email verification settings
    /// </summary>
    public VerificationSettings VerificationSettings { get; set; } = new();
    
    public IdentityDatabaseOptions? DatabaseOptions { get; set; }

    internal void ValidateConfiguration()
    {
        if (string.IsNullOrEmpty(PasswordEncryptionKey))
            throw new ConfigurationErrorsException("No password encryption key");

        if (JwtSettings is null)
            throw new ConfigurationErrorsException("Jwt settings not set");
        
        if (RedisConnectionSettings is null)
            throw new ConfigurationErrorsException("Redis connection settings not set");

        if (DatabaseOptions is null)
            throw new ConfigurationErrorsException("No database configuration provided");
        
        if (UseJwtEncryption && JwtSettings?.EncryptionKey is null)
        {
            throw new ConfigurationErrorsException(
                $"{nameof(JwtSettings.EncryptionKey)} must be set if {nameof(SecuritySettings.UseJwtEncryption)} is true");
        }

        if (ValidationSettings.Username.MinimumLength > ValidationSettings.Username.MaximumLength)
        {
            throw new ConfigurationErrorsException(
                "Minimum length of username must be less than its maximum length");
        }

        if (ValidationSettings.Username.MinimumLength <= 0)
        {
            throw new ConfigurationErrorsException("Minimum length of username must be greater than 0");
        }     
        
        if (ValidationSettings.Password.MinimumLength <= 0)
        {
            throw new ConfigurationErrorsException("Minimum length of username must be greater than 0");
        }
    }
}