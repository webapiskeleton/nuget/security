﻿using Riok.Mapperly.Abstractions;
using WebApiSkeleton.Security.Core.Models.Database;
using WebApiSkeleton.Security.Core.Models.DTO;
using RefreshToken = WebApiSkeleton.Security.Core.Models.Database.RefreshToken;
using User = WebApiSkeleton.Security.Core.Models.Database.User;

namespace WebApiSkeleton.Security.Core.Map;

[Mapper]
internal partial class SecurityMapper
{
    internal partial User CredentialsToIdentityUser(SecurityRegistrationCredentials credentials);
    internal partial Models.Output.User CredentialsToUser(SecurityRegistrationCredentials credentials);
    internal partial RefreshToken Map(Models.Output.RefreshToken command);
    internal partial Models.Output.RefreshToken Map(RefreshToken command);
    internal partial Models.Output.User Map(User user);
    internal partial User Map(Models.Output.User user);
    internal partial Role Map(Models.Output.Role role);
    internal partial Models.Output.Role Map(Role role);
    internal partial Models.Output.Permission Map(Permission permission);
    internal partial Permission Map(Models.Output.Permission permission);

    internal Claim Map(UserClaim claim)
    {
        return new Claim(claim.Key, claim.Value);
    }
}