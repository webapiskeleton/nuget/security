using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using WebApiSkeleton.Security.Core.Extensions;

namespace WebApiSkeleton.Security.Core.DatabaseMapping;

/// <summary>
/// Automatic database mapping that extends <see cref="BaseDatabaseMapper{T}"/>
/// </summary>
/// <typeparam name="T">Database entity to map</typeparam>
internal abstract class BaseDatabaseMapping<T> : BaseDatabaseMapper<T>
{
    protected BaseDatabaseMapping(IdentityContext context) : base(context.Model.FindEntityType(typeof(T))
        ?.GetSchemaQualifiedTableName(context.Database.ProviderName !=
                                      "Microsoft.EntityFrameworkCore.Sqlite") ?? "")
    {
        var databaseModel = context.Model.FindEntityType(typeof(T));
        if (databaseModel is null)
            throw new InvalidOperationException(
                $"No entity for type {typeof(T)} registered in {typeof(IdentityContext)}");
        MapProperties(databaseModel.GetProperties());
    }

    /// <summary>
    /// Automatically process database column and match them with properties
    /// </summary>
    private void MapProperties(IEnumerable<IProperty> properties)
    {
        foreach (var pr in properties)
        {
            var typeParam = Expression.Parameter(typeof(T), "s");
            var property = Expression.Property(typeParam, pr.PropertyInfo!.Name);
            var propertyAsObject = Expression.Convert(property, typeof(object));
            var lambda = Expression.Lambda<Func<T, object>>(propertyAsObject, typeParam);
            MapProperty(lambda, pr.GetColumnName());
        }
    }
}