namespace WebApiSkeleton.Security.Core.Enums;

public enum AuthenticationResult
{
    Success = 1,

    /// <summary>
    /// Wrong login/password
    /// </summary>
    WrongCredentials = 2,

    UserDisabled = 3,

    /// <summary>
    /// Too many attempts to log in
    /// </summary>
    UserBruteForced = 4,

    /// <summary>
    /// Error while validating data
    /// </summary>
    AuthorizationFailed = 5,

    /// <summary>
    /// Too many simultaneous attempts to authorize or refresh a token
    /// </summary>
    TooManyAttempts = 6,

    /// <summary>
    /// User potentially lost the refresh token or it was stolen by hackers
    /// </summary>
    PotentialHack = 7,
}