﻿namespace WebApiSkeleton.Security.Core.Enums;

public enum PermissionMode
{
    /// <summary>
    /// Allow using permission
    /// </summary>
    Allow = 1,
    
    /// <summary>
    /// Forbid access to permission
    /// </summary>
    Restrict = 2,
}