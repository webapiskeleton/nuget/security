namespace WebApiSkeleton.Security.Core.Settings;

public sealed class VerificationSettings
{
    public IVerificationCodeType VerificationCodeTypeType { get; init; } = new IntegerVerificationCodeType();
    public TimeSpan VerificationCodeTimeToLive { get; init; } = TimeSpan.FromMinutes(2);
}

public interface IVerificationCodeType;

/// <summary>
/// Verification code represented by a random-generated integer of set <see cref="Length"/>
/// </summary>
public sealed class IntegerVerificationCodeType : IVerificationCodeType
{
    public int Length { get; init; } = 6;
}

/// <summary>
/// Verification code represented by a Guid
/// </summary>
public sealed class GuidVerificationCodeType : IVerificationCodeType;

/// <summary>
/// Verification code represented by random string containing latin characters and digits of set <see cref="Length"/>
/// </summary>
public sealed class RandomStringVerificationCodeType : IVerificationCodeType
{
    public int Length { get; init; } = 10;
    public char[] AllowedCharacters { get; }

    public RandomStringVerificationCodeType()
    {
        var letters = Enumerable.Range('A', 'z' - 'A').Select(x => (char)x);
        var digits = Enumerable.Range('0', '9' - '0').Select(x => (char)x);
        AllowedCharacters = letters.Concat(digits).ToArray();
    }
}