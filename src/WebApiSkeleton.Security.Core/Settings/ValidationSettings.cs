namespace WebApiSkeleton.Security.Core.Settings;

public sealed class ValidationSettings
{
    /// <summary>
    /// Options to validate username when creating/updating a user
    /// <remarks>By default @ and newline symbols are restricted, this cannot be undone</remarks>
    /// </summary>
    public UsernameValidationOptions Username { get; } = new();

    /// <summary>
    /// Options to validate password when setting it to a user
    /// </summary>
    public PasswordValidationOptions Password { get; } = new();
}

public sealed class PasswordValidationOptions
{
    /// <summary>
    /// Require any uppercase symbol A-Z
    /// </summary>
    public bool RequireUppercase { get; set; } = true;

    /// <summary>
    /// Require any digit 0-9
    /// </summary>
    public bool RequireDigit { get; set; } = true;

    /// <summary>
    /// Require any lowercase symbol a-z
    /// </summary>
    public bool RequireLowercase { get; set; } = true;

    /// <summary>
    /// Require one of symbols: @$!%*#?&amp;
    /// </summary>
    public bool RequireNonAlphanumeric { get; set; } = true;

    public int MinimumLength { get; set; } = 10;

    public override string ToString()
    {
        return $"""
                Minimum length: {MinimumLength} symbols
                {(RequireUppercase ? "At least one uppercase letter" : "")}
                {(RequireLowercase ? "At least one lowercase letter" : "")}
                {(RequireDigit ? "At least one digit" : "")}
                {(RequireNonAlphanumeric ? "At least one symbol from following: @$!%*#?&" : "")}
                """;
    }
}

public sealed class UsernameValidationOptions
{
    public IReadOnlyCollection<Range> AllowedAdditionalCharacterRanges => _additionalCharactersRanges.AsReadOnly();
    public IReadOnlyCollection<char> AllowedAdditionalCharacters => _additionalCharacters.AsReadOnly();

    private readonly List<Range> _additionalCharactersRanges = new();
    private readonly List<char> _additionalCharacters = new();

    /// <summary>
    /// Allow digits 0-9
    /// </summary>
    public bool AllowDigits { get; set; } = true;

    /// <summary>
    /// Allow "_" symbol
    /// </summary>
    public bool AllowUnderscore { get; set; } = true;

    /// <summary>
    /// Minimum length of a username
    /// <remarks>Must be greater than 0</remarks>
    /// </summary>
    public int MinimumLength { get; set; } = 5;

    /// <summary>
    /// Maximum length of a username
    /// <remarks>Must be greater than <see cref="MinimumLength"/></remarks>
    /// </summary>
    public int MaximumLength { get; set; } = 15;

    public UsernameValidationOptions AddCharacterRange(Range characterRange)
    {
        if (characterRange.Start.Value >= characterRange.End.Value)
        {
            throw new InvalidOperationException("Cannot add character range where end symbol is less than start");
        }

        _additionalCharactersRanges.Add(characterRange);
        return this;
    }

    public UsernameValidationOptions AddCharacterRange(IEnumerable<Range> characterRanges)
    {
        foreach (var characterRange in characterRanges)
        {
            if (characterRange.Start.Value >= characterRange.End.Value)
            {
                throw new InvalidOperationException("Cannot add character range where end symbol is less than start");
            }

            _additionalCharactersRanges.Add(characterRange);
        }

        return this;
    }

    public UsernameValidationOptions AddCharacter(char character)
    {
        _additionalCharacters.Add(character);
        return this;
    }

    public UsernameValidationOptions AddCharacter(IEnumerable<char> characters)
    {
        _additionalCharacters.AddRange(characters);
        return this;
    }
}