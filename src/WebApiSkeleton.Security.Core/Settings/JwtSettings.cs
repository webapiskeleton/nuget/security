﻿namespace WebApiSkeleton.Security.Core.Settings;

public sealed class JwtSettings
{
    public required string SigningKey { get; init; } = null!;

    /// <summary>
    /// Set if <see cref="SecurityConfiguration.UseJwtEncryption"/> is set to true
    /// </summary>
    public string? EncryptionKey { get; init; }

    public string? Audience { get; init; }
    public string? Issuer { get; init; }
    public required TimeSpan AccessTokenLifetime { get; init; }
    public required int RefreshTokenMonthLifetime { get; init; }
    
    /// <summary>
    /// Token validation parameters
    /// </summary>
    /// <remarks>
    /// NOTE: Following properties will be set to service-needed values:
    /// <see cref="Microsoft.IdentityModel.Tokens.TokenValidationParameters.IssuerSigningKey"/> to the <see cref="SymmetricSecurityKey"/> using provided <see cref="SigningKey"/>,
    /// <see cref="Microsoft.IdentityModel.Tokens.TokenValidationParameters.ValidateIssuerSigningKey"/> to true,
    /// <see cref="Microsoft.IdentityModel.Tokens.TokenValidationParameters.ValidateLifetime"/> to true,
    /// <see cref="Microsoft.IdentityModel.Tokens.TokenValidationParameters.ClockSkew"/> to <see cref="TimeSpan.Zero"/>,
    /// if <see cref="SecurityConfiguration.UseJwtEncryption"/> set to true then
    /// <see cref="Microsoft.IdentityModel.Tokens.TokenValidationParameters.TokenDecryptionKey"/>
    /// will be set to <see cref="SymmetricSecurityKey"/> using <see cref="EncryptionKey"/> passed
    /// </remarks>
    public required TokenValidationParameters TokenValidationParameters { get; init; }
}