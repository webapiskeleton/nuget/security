﻿namespace WebApiSkeleton.Security.Core.Settings;

public sealed class PasswordEncryptionSettings
{
    /// <summary>
    /// Key to use it for hashing the password
    /// </summary>
    public string PasswordEncryptionKey { get; set; } = null!;
}