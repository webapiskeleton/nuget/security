﻿namespace WebApiSkeleton.Security.Core.Settings;

internal sealed class SecuritySettings
{
    /// <summary>
    /// Attempts of user logging in before it can be handled as bruteforce
    /// </summary>
    public int BruteforceAfter { get; init; } = 5;
    
    /// <summary>
    /// Time to wait before bruteforce blocking is cancelled
    /// </summary>
    public TimeSpan BruteforceCooldown { get; init; } = TimeSpan.FromMinutes(3);
    
    /// <summary>
    /// Add <see cref="Claim"/>s that user has to JWT
    /// </summary>
    public bool IncludeUserClaimsInJwt { get; init; }
    
    /// <summary>
    /// Additionally encrypt JWT
    /// </summary>
    public bool UseJwtEncryption { get; init; }
}