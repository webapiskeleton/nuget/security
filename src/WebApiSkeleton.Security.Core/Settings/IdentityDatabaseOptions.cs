using Microsoft.EntityFrameworkCore;

namespace WebApiSkeleton.Security.Core.Settings;

public sealed class IdentityDatabaseOptions
{
    /// <summary>
    /// Options to configure DbContext, including the provider setting and some behavior
    /// </summary>
    /// <remarks>
    /// <see cref="QueryTrackingBehavior"/> will be always set to <see cref="QueryTrackingBehavior.NoTracking"/>
    /// for correct work of service
    /// </remarks>
    public required Action<DbContextOptionsBuilder> OptionsAction { get; set; }

    /// <summary>
    /// True if you need to use one transaction across whole MediatR requests
    /// </summary>
    /// <remarks>
    /// Only works for requests that implement ITransactRequest interface in WebApiSkeleton.Contracts.Base assembly.
    /// Highly recommended to use as it can produce data issues when using some commands in this service
    /// </remarks>
    public required bool UseRequestTransactions { get; set; } = true;
}