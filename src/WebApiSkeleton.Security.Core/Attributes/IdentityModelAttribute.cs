﻿namespace WebApiSkeleton.Security.Core.Attributes;

/// <summary>
/// Attribute that identifies models stored in database
/// </summary>
internal class IdentityModelAttribute : Attribute
{
    public IdentityModelAttribute()
    {
    }
}