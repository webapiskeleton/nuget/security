using Microsoft.Extensions.DependencyInjection;
using WebApiSkeleton.Security.Core.Repository.Command;
using WebApiSkeleton.Security.Core.Repository.Query;
using WebApiSkeleton.Security.Core.Settings;

namespace WebApiSkeleton.Security.Core.Verification;

public class EmailVerifier : IEmailVerifier
{
    private readonly IUserCommandRepository _userCommandRepository;
    private readonly IIdentityUserQueryRepository _userQueryRepository;
    private readonly VerificationSettings _verificationSettings;

    public EmailVerifier(VerificationSettings verificationSettings,
        IServiceProvider serviceProvider)
    {
        _verificationSettings = verificationSettings;
        _userCommandRepository = serviceProvider.GetRequiredService<IUserCommandRepository>();
        _userQueryRepository = serviceProvider.GetRequiredService<IIdentityUserQueryRepository>();
    }

    public async Task<VerificationCodeInformation> GenerateVerificationCodeForUserAsync(int userId,
        CancellationToken cancellationToken = default)
    {
        var code = VerificationCodeGenerator.GenerateVerificationCode(_verificationSettings.VerificationCodeTypeType);
        var sessionId = Guid.NewGuid();
        var info = new VerificationCodeInformation { VerificationCode = code, SessionId = sessionId };

        await _userCommandRepository.AddEmailVerificationCodeAsync(userId, info, cancellationToken);

        return info;
    }

    public async Task UseVerificationCodeAsync(int userId, CancellationToken cancellationToken = default)
    {
        await _userCommandRepository.VerifyUserEmail(userId, cancellationToken);
    }
}