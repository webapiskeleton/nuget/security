using WebApiSkeleton.Security.Core.Settings;

namespace WebApiSkeleton.Security.Core.Verification;

/// <summary>
/// Service used to verify user emails
/// </summary>
public interface IEmailVerifier
{
    /// <summary>
    /// Generates unique verification code and sessionId for given user, based on <see cref="VerificationSettings"/>
    /// </summary>
    /// <param name="userId">User to generate code for</param>
    /// <param name="cancellationToken">Cancellation token</param>
    /// <returns>Generated verification code information</returns>
    public Task<VerificationCodeInformation> GenerateVerificationCodeForUserAsync(int userId,
        CancellationToken cancellationToken = default);

    /// <summary>
    /// Makes user's email verified
    /// </summary>
    /// <param name="userId">User to verify</param>
    /// <param name="cancellationToken">Cancellation token</param>
    /// <remarks>All verification code checks should be done before execution</remarks>
    public Task UseVerificationCodeAsync(int userId, CancellationToken cancellationToken = default);
}