namespace WebApiSkeleton.Security.Core.Verification;

public class VerificationCodeInformation
{
    /// <summary>
    /// Verification code string representation
    /// </summary>
    public required string VerificationCode { get; init; }

    /// <summary>
    /// Guid generated for every verification request.
    /// Used to ensure that the same device is trying to access verification process
    /// </summary>
    public required Guid SessionId { get; init; }
}

/// <summary>
/// <see cref="VerificationCodeInformation"/> with creation date to validate expiration
/// </summary>
public class StoredVerificationCodeInformation : VerificationCodeInformation
{
    public DateTime CreatedAt { get; set; }
}