using System.Diagnostics;
using WebApiSkeleton.Security.Core.Settings;

namespace WebApiSkeleton.Security.Core.Verification;

/// <summary>
/// Entity that generates verification codes based on <see cref="IVerificationCodeType"/> provided
/// </summary>
internal static class VerificationCodeGenerator
{
    public static string GenerateVerificationCode(IVerificationCodeType type)
    {
        switch (type)
        {
            case IntegerVerificationCodeType integer:
                var minInteger = (int)Math.Pow(10, integer.Length);
                var maxInteger = (int)Math.Pow(10, integer.Length + 1);
                return Random.Shared.Next(minInteger, maxInteger).ToString();
            case RandomStringVerificationCodeType randomString:
                var code = string.Join("", Enumerable.Range(0, randomString.Length)
                    .Select(_ =>
                        randomString.AllowedCharacters[Random.Shared.Next(randomString.AllowedCharacters.Length)]));
                return code;
            case GuidVerificationCodeType:
                return Guid.NewGuid().ToString();
            default:
                throw new UnreachableException();
        }
    }
}