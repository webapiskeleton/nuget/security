﻿using WebApiSkeleton.SearchSqlGeneration;
using WebApiSkeleton.Security.Core.Map;
using WebApiSkeleton.Security.Core.Models.Database;
using WebApiSkeleton.Security.Core.Repository.Base;
using WebApiSkeleton.Security.Core.Verification;
using DomainUser = WebApiSkeleton.Security.Core.Models.Output.User;
using DomainRole = WebApiSkeleton.Security.Core.Models.Output.Role;

namespace WebApiSkeleton.Security.Core.Repository.Query.Impl;

internal sealed class IdentityUserQueryRepository : SecurityQueryRepositoryBase, IIdentityUserQueryRepository
{
    private readonly IDatabaseMapping<User> _userMapping;
    private readonly IDatabaseMapping<UserPassword> _userPasswordMapping;
    private readonly IDatabaseMapping<UserClaim> _userClaimMapping;
    private readonly IDatabaseMapping<UserRole> _userRoleMapping;
    private readonly IDatabaseMapping<Role> _roleMapping;
    private readonly IDatabaseMapping<UserVerificationCode> _verificationCodeMapping;
    private readonly SecurityMapper _mapper;

    public IdentityUserQueryRepository(SecurityNpgsqlConnectionProvider connectionProvider,
        IDatabaseMapping<User> userMapping, IDatabaseMapping<UserPassword> userPasswordMapping,
        IDatabaseMapping<UserClaim> userClaimMapping, IDatabaseMapping<Role> roleMapping,
        IDatabaseMapping<UserRole> userRoleMapping,
        IDatabaseMapping<UserVerificationCode> verificationCodeMapping,
        ISearchSqlConverter searchSqlConverter) : base(connectionProvider, searchSqlConverter)
    {
        _userMapping = userMapping;
        _userPasswordMapping = userPasswordMapping;
        _userClaimMapping = userClaimMapping;
        _roleMapping = roleMapping;
        _userRoleMapping = userRoleMapping;
        _verificationCodeMapping = verificationCodeMapping;
        _mapper = new SecurityMapper();
    }

    public async Task<DomainUser?> GetUserByIdAsync(int userId)
    {
        var (connection, transaction) = ConnectionProvider.GetConnection();
        var sql = GetOrAddCachedSql($"""
                                     SELECT *
                                     FROM {_userMapping.SchemaQualifiedTableName}
                                     WHERE {_userMapping[nameof(User.Id)]} = @{nameof(userId)}
                                     """);

        var result = await connection.QuerySingleOrDefaultAsync<User>(sql, new { userId }, transaction);
        return result is null ? null : _mapper.Map(result);
    }

    public async Task<DomainUser?> GetUserByEmailAsync(string email)
    {
        var (connection, transaction) = ConnectionProvider.GetConnection();
        var sql = GetOrAddCachedSql($"""
                                     SELECT *
                                     FROM {_userMapping.SchemaQualifiedTableName}
                                     WHERE {_userMapping[nameof(User.Email)]} = @{nameof(email)}
                                     """);

        var result = await connection.QuerySingleOrDefaultAsync<User>(sql, new { email }, transaction);
        return result is null ? null : _mapper.Map(result);
    }

    public async Task<DomainUser?> GetUserByUsernameAsync(string username)
    {
        var (connection, transaction) = ConnectionProvider.GetConnection();
        var sql = GetOrAddCachedSql($"""
                                     SELECT *
                                     FROM {_userMapping.SchemaQualifiedTableName}
                                     WHERE {_userMapping[nameof(User.Username)]} = @{nameof(username)}
                                     """);

        var result = await connection.QuerySingleOrDefaultAsync<User>(sql, new { username }, transaction);
        return result is null ? null : _mapper.Map(result);
    }

    public async Task<DomainUser?> GetUserByLoginAsync(string login)
    {
        var (connection, transaction) = ConnectionProvider.GetConnection();
        var sql = GetOrAddCachedSql($"""
                                     SELECT *
                                     FROM {_userMapping.SchemaQualifiedTableName}
                                     WHERE {_userMapping[nameof(User.Username)]} = @{nameof(login)}
                                        OR {_userMapping[nameof(User.Email)]} = @{nameof(login)}
                                     """);

        var result = await connection.QuerySingleOrDefaultAsync<User>(sql, new { login }, transaction);
        return result is null ? null : _mapper.Map(result);
    }

    public async Task<ListResult<DomainUser>> ListUsersAsync(ListParam<DomainUser> listParam)
    {
        var parameters = new DynamicParameters();
        var filterString = GetFiltersSql<DomainUser, User>(listParam.Filters, parameters);
        var orderByString = GetOrderBySql<DomainUser, User>(listParam.SortingDefinitions, _userMapping[nameof(User.Id)]!);

        var resultSql = $@"SELECT * 
                     FROM {_userMapping.SchemaQualifiedTableName}
                     WHERE TRUE {(!string.IsNullOrEmpty(filterString) ? $"AND {filterString}" : "")}
                     ORDER BY {orderByString}
                     {SqlConverter.ToSql(listParam.PaginationInfo)}";
        var countSql = $@"SELECT COUNT(*) 
                          FROM {_userMapping.SchemaQualifiedTableName}
                          WHERE TRUE {(!string.IsNullOrEmpty(filterString) ? $"AND {filterString}" : "")}";
        var (connection, transaction) = ConnectionProvider.GetConnection();
        var users = await connection.QueryAsync<User>(resultSql, parameters, transaction);
        var count = await connection.ExecuteScalarAsync<int>(countSql, parameters, transaction);
        return new ListResult<DomainUser>(users.Select(x => _mapper.Map(x)), count);
    }

    public async Task<bool> IsUserPasswordCorrectAsync(int userId, byte[] passwordHash)
    {
        var (connection, transaction) = ConnectionProvider.GetConnection();
        var sql = GetOrAddCachedSql($"""
                                     SELECT {_userPasswordMapping[nameof(UserPassword.PasswordHash)]}
                                     FROM {_userPasswordMapping.SchemaQualifiedTableName}
                                     WHERE {_userPasswordMapping[nameof(UserPassword.IsActual)]} = '1'
                                        AND {_userPasswordMapping[nameof(UserPassword.UserId)]} = @{nameof(userId)}
                                     """);
        var existingHash = await connection.ExecuteScalarAsync<byte[]>(sql, new { userId }, transaction);

        if (existingHash is null)
            return false;
        return existingHash.SequenceEqual(passwordHash);
    }

    public async Task<IEnumerable<Claim>> GetClaimsForUserAsync(int userId)
    {
        var (connection, transaction) = ConnectionProvider.GetConnection();
        var sql = GetOrAddCachedSql($"""
                                     SELECT *
                                     FROM {_userClaimMapping.SchemaQualifiedTableName}
                                     WHERE {_userClaimMapping[nameof(UserClaim.UserId)]} = @{nameof(userId)}
                                     """);
        return (await connection.QueryAsync<UserClaim>(sql, new { userId }, transaction))
            .Select(x => _mapper.Map(x));
    }

    public async Task<IEnumerable<DomainRole>> GetRolesForUserAsync(int userId)
    {
        var (connection, transaction) = ConnectionProvider.GetConnection();
        var sql = GetOrAddCachedSql($"""
                                     SELECT r.{_roleMapping[nameof(Role.Id)]},
                                            r.{_roleMapping[nameof(Role.Name)]}
                                     FROM {_roleMapping.SchemaQualifiedTableName} r
                                        JOIN {_userRoleMapping.SchemaQualifiedTableName} ur
                                            ON ur.{_userRoleMapping[nameof(UserRole.RoleId)]} = r.{_roleMapping[nameof(Role.Id)]}
                                                AND ur.{_userRoleMapping[nameof(UserRole.UserId)]} = @{nameof(userId)}
                                     """);
        return (await connection.QueryAsync<Role>(sql, new { userId }, transaction))
            .Select(x => _mapper.Map(x));
    }

    public async Task<bool> UserInRoleAsync(int userId, int roleId)
    {
        var (connection, transaction) = ConnectionProvider.GetConnection();
        var sql = GetOrAddCachedSql($"""
                                     SELECT EXISTS(SELECT
                                                   FROM {_userRoleMapping.SchemaQualifiedTableName}
                                                   WHERE {_userRoleMapping[nameof(UserRole.UserId)]} = @{nameof(userId)}
                                                       AND {_userRoleMapping[nameof(UserRole.RoleId)]} = @{nameof(roleId)})
                                     """);
        return await connection.ExecuteScalarAsync<bool>(sql, new { userId, roleId }, transaction);
    }

    public async Task<StoredVerificationCodeInformation?> GetVerificationCodeInformationAsync(int userId,
        Guid sessionId)
    {
        var (connection, transaction) = ConnectionProvider.GetConnection();
        var sql = GetOrAddCachedSql($"""
                                     SELECT {_verificationCodeMapping[nameof(UserVerificationCode.VerificationCode)]},
                                            {_verificationCodeMapping[nameof(UserVerificationCode.SessionId)]},
                                            {_verificationCodeMapping[nameof(UserVerificationCode.CreatedAt)]}
                                     FROM {_verificationCodeMapping.SchemaQualifiedTableName}
                                     WHERE {_verificationCodeMapping[nameof(UserVerificationCode.UserId)]} = @{nameof(userId)}
                                        AND {_verificationCodeMapping[nameof(UserVerificationCode.SessionId)]} = @{nameof(sessionId)}
                                     """);
        return await connection.QueryFirstOrDefaultAsync<StoredVerificationCodeInformation>(sql,
            new { userId, sessionId }, transaction);
    }
}