﻿using WebApiSkeleton.SearchSqlGeneration;
using WebApiSkeleton.Security.Core.Map;
using WebApiSkeleton.Security.Core.Repository.Base;
using RefreshToken = WebApiSkeleton.Security.Core.Models.Database.RefreshToken;

namespace WebApiSkeleton.Security.Core.Repository.Query.Impl;

internal sealed class RefreshTokenQueryRepository : SecurityQueryRepositoryBase, IRefreshTokenQueryRepository
{
    private readonly IDatabaseMapping<RefreshToken> _refreshTokenMapping;
    private readonly SecurityMapper _mapper;

    public RefreshTokenQueryRepository(SecurityNpgsqlConnectionProvider connectionProvider,
        IDatabaseMapping<RefreshToken> refreshTokenMapping,
        ISearchSqlConverter searchSqlConverter) : base(connectionProvider, searchSqlConverter)
    {
        _mapper = new SecurityMapper();
        _refreshTokenMapping = refreshTokenMapping;
    }

    public async Task<Models.Output.RefreshToken?> GetTokenAsync(string token)
    {
        var (connection, transaction) = ConnectionProvider.GetConnection();
        var sql = GetOrAddCachedSql($"""
                                     SELECT *
                                     FROM {_refreshTokenMapping.SchemaQualifiedTableName}
                                        WHERE {_refreshTokenMapping[nameof(RefreshToken.Token)]} = @{nameof(token)}
                                     """);

        var result = await connection.QuerySingleOrDefaultAsync<RefreshToken>(sql, new { token }, transaction);
        return result is null ? null : _mapper.Map(result);
    }
}