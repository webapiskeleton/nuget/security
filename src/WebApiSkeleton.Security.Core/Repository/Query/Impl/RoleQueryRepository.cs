﻿using WebApiSkeleton.SearchSqlGeneration;
using WebApiSkeleton.Security.Core.Map;
using WebApiSkeleton.Security.Core.Models.Database;
using WebApiSkeleton.Security.Core.Repository.Base;
using Role = WebApiSkeleton.Security.Core.Models.Output.Role;
using DomainUser = WebApiSkeleton.Security.Core.Models.Output.User;

namespace WebApiSkeleton.Security.Core.Repository.Query.Impl;

internal sealed class RoleQueryRepository : SecurityQueryRepositoryBase, IRoleQueryRepository
{
    private readonly IDatabaseMapping<Models.Database.Role> _roleMapping;
    private readonly IDatabaseMapping<UserRole> _userRoleMapping;
    private readonly IDatabaseMapping<User> _userMapping;
    private readonly SecurityMapper _mapper = new();

    public RoleQueryRepository(SecurityNpgsqlConnectionProvider connectionProvider,
        IDatabaseMapping<Models.Database.Role> roleMapping,
        IDatabaseMapping<UserRole> userRoleMapping,
        IDatabaseMapping<User> userMapping,
        ISearchSqlConverter searchSqlConverter) : base(connectionProvider, searchSqlConverter)
    {
        _roleMapping = roleMapping;
        _userRoleMapping = userRoleMapping;
        _userMapping = userMapping;
    }

    public async Task<ListResult<Role>> ListRolesAsync(PaginationInfo pagination)
    {
        var (connection, transaction) = ConnectionProvider.GetConnection();
        var countSql = GetOrAddCachedSql($"""
                                          SELECT COUNT(*)
                                          FROM {_roleMapping.SchemaQualifiedTableName}
                                          """);
        var sql = GetOrAddCachedSql($"""
                                     SELECT *
                                     FROM {_roleMapping.SchemaQualifiedTableName}
                                     {SqlConverter.ToSql(pagination)}
                                     """);
        var result = await connection.QueryAsync<Models.Database.Role>(sql, transaction);
        var count = await connection.ExecuteScalarAsync<int>(countSql, transaction);
        return new ListResult<Role>(result.Select(x => _mapper.Map(x)), count);
    }

    public async Task<Role?> GetRoleAsync(string name)
    {
        var (connection, transaction) = ConnectionProvider.GetConnection();
        var sql = GetOrAddCachedSql($"""
                                     SELECT *
                                     FROM {_roleMapping.SchemaQualifiedTableName}
                                     WHERE {_roleMapping[nameof(Models.Database.Role.Name)]} = @{nameof(name)}
                                     """);
        var result = await connection.QueryFirstOrDefaultAsync<Models.Database.Role>(sql, new { name }, transaction);
        return result is null ? null : _mapper.Map(result);
    }

    public async Task<Role?> GetRoleAsync(int id)
    {
        var (connection, transaction) = ConnectionProvider.GetConnection();
        var sql = GetOrAddCachedSql($"""
                                     SELECT *
                                     FROM {_roleMapping.SchemaQualifiedTableName}
                                     WHERE {_roleMapping[nameof(Models.Database.Role.Id)]} = @{nameof(id)}
                                     """);
        var result = await connection.QueryFirstOrDefaultAsync<Models.Database.Role>(sql, new { id }, transaction);
        return result is null ? null : _mapper.Map(result);
    }

    public async Task<ListResult<DomainUser>> GetRoleUsersAsync(int roleId, PaginationInfo pagination)
    {
        var (connection, transaction) = ConnectionProvider.GetConnection();
        var sql = GetOrAddCachedSql($"""
                                     SELECT u.{_userMapping[nameof(User.Id)]},
                                            u.{_userMapping[nameof(User.Username)]},
                                            u.{_userMapping[nameof(User.PhoneNumber)]},
                                            u.{_userMapping[nameof(User.CreatedOnTimeStamp)]},
                                            u.{_userMapping[nameof(User.Email)]},
                                            u.{_userMapping[nameof(User.EmailVerified)]},
                                            u.{_userMapping[nameof(User.IsEnabled)]}
                                     FROM {_userMapping.SchemaQualifiedTableName} u
                                        JOIN {_userRoleMapping.SchemaQualifiedTableName} ur
                                            ON ur.{_userRoleMapping[nameof(UserRole.UserId)]} = u.{_userMapping[nameof(User.Id)]}
                                                AND ur.{_userRoleMapping[nameof(UserRole.RoleId)]} = @{nameof(roleId)}
                                     ORDER BY u.{_userMapping[nameof(User.Id)]}
                                     {SqlConverter.ToSql(pagination)}
                                     """);
        var countSql = GetOrAddCachedSql($"""
                                          SELECT COUNT(*)
                                          FROM {_userRoleMapping.SchemaQualifiedTableName}
                                          WHERE {_userRoleMapping[nameof(UserRole.RoleId)]} = @{nameof(roleId)}
                                          """);
        var users = await connection.QueryAsync<User>(sql, new { roleId }, transaction);
        var totalCount = await connection.ExecuteScalarAsync<int>(countSql, new { roleId });
        return new ListResult<DomainUser>(users.Select(x => _mapper.Map(x)), totalCount);
    }
}