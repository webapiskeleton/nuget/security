using System.Text;
using WebApiSkeleton.SearchSqlGeneration;
using WebApiSkeleton.Security.Core.Map;
using WebApiSkeleton.Security.Core.Models.Database;
using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Repository.Base;
using CustomPermission = WebApiSkeleton.Security.Core.Models.Database.CustomPermission;
using Permission = WebApiSkeleton.Security.Core.Models.Database.Permission;

namespace WebApiSkeleton.Security.Core.Repository.Query.Impl;

internal sealed class PermissionQueryRepository : SecurityQueryRepositoryBase, IPermissionQueryRepository
{
    private readonly IDatabaseMapping<Permission> _permissionMapping;
    private readonly IDatabaseMapping<UserPermission> _userPermissionMapping;
    private readonly IDatabaseMapping<RolePermission> _rolePermissionMapping;
    private readonly IDatabaseMapping<UserRole> _userRoleMapping;
    private readonly IDatabaseMapping<CustomPermission> _customPermissionMapping;
    private readonly IDatabaseMapping<CustomPermissionType> _customPermissionTypeMapping;
    private readonly SecurityMapper _mapper;

    public PermissionQueryRepository(SecurityNpgsqlConnectionProvider connectionProvider,
        IDatabaseMapping<Permission> permissionMapping,
        IDatabaseMapping<UserPermission> userPermissionMapping,
        IDatabaseMapping<RolePermission> rolePermissionMapping,
        IDatabaseMapping<UserRole> userRoleMapping,
        ISearchSqlConverter searchSqlConverter,
        IDatabaseMapping<CustomPermission> customPermissionMapping,
        IDatabaseMapping<CustomPermissionType> customPermissionTypeMapping) : base(connectionProvider,
        searchSqlConverter)
    {
        _mapper = new SecurityMapper();
        _permissionMapping = permissionMapping;
        _userPermissionMapping = userPermissionMapping;
        _rolePermissionMapping = rolePermissionMapping;
        _userRoleMapping = userRoleMapping;
        _customPermissionMapping = customPermissionMapping;
        _customPermissionTypeMapping = customPermissionTypeMapping;
    }

    public async Task<Models.Output.Permission?> GetPermissionAsync(string name)
    {
        var (connection, transaction) = ConnectionProvider.GetConnection();
        var sql = GetOrAddCachedSql($"""
                                     SELECT *
                                     FROM {_permissionMapping.SchemaQualifiedTableName}
                                        WHERE {_permissionMapping[nameof(Permission.Name)]} = @{nameof(name)}
                                     """);
        var result = await connection.QuerySingleOrDefaultAsync<Permission>(sql, new { name }, transaction);
        return result is null ? null : _mapper.Map(result);
    }

    public Task<IEnumerable<GrantedPermission>> ListUserPermissions(int userId, bool includeRolePermissions)
    {
        var (connection, transaction) = ConnectionProvider.GetConnection();
        string sql;
        switch (includeRolePermissions)
        {
            case true:
                sql = GetOrAddCachedSql($"""
                                         SELECT p.{_permissionMapping[nameof(Permission.Id)]},
                                                p.{_permissionMapping[nameof(Permission.Name)]},
                                                up.{_userPermissionMapping[nameof(UserPermission.Mode)]} "Mode"
                                         FROM {_permissionMapping.SchemaQualifiedTableName} p
                                            JOIN {_userPermissionMapping.SchemaQualifiedTableName} up
                                                ON up.{_userPermissionMapping[nameof(UserPermission.PermissionId)]} = p.{_permissionMapping[nameof(Permission.Id)]}
                                         WHERE {_userPermissionMapping[nameof(UserPermission.UserId)]} = @{nameof(userId)}
                                         UNION
                                         SELECT p.{_permissionMapping[nameof(Permission.Id)]},
                                                p.{_permissionMapping[nameof(Permission.Name)]},
                                                rp.{_rolePermissionMapping[nameof(UserPermission.Mode)]} "Mode"
                                         FROM {_permissionMapping.SchemaQualifiedTableName} p
                                            JOIN {_rolePermissionMapping.SchemaQualifiedTableName} rp
                                                ON rp.{_rolePermissionMapping[nameof(RolePermission.PermissionId)]} = p.{_permissionMapping[nameof(Permission.Id)]}
                                            JOIN {_userRoleMapping.SchemaQualifiedTableName} ur
                                                ON ur.{_userRoleMapping[nameof(UserRole.RoleId)]} = rp.{_rolePermissionMapping[nameof(RolePermission.RoleId)]}
                                                    AND ur.{_userRoleMapping[nameof(UserRole.UserId)]} = @{nameof(userId)}
                                         """, includeRolePermissions.ToString());
                break;
            case false:
                sql = GetOrAddCachedSql($"""
                                         SELECT p.{_permissionMapping[nameof(Permission.Id)]},
                                                p.{_permissionMapping[nameof(Permission.Name)]},
                                                up.{_userPermissionMapping[nameof(UserPermission.Mode)]} "Mode"
                                         FROM {_permissionMapping.SchemaQualifiedTableName} p
                                            JOIN {_userPermissionMapping.SchemaQualifiedTableName} up
                                                ON up.{_userPermissionMapping[nameof(UserPermission.PermissionId)]} = p.{_permissionMapping[nameof(Permission.Id)]}
                                         WHERE {_userPermissionMapping[nameof(UserPermission.UserId)]} = @{nameof(userId)}
                                         """, includeRolePermissions.ToString());
                break;
        }


        return connection.QueryAsync<GrantedPermission>(sql, new { userId }, transaction);
    }

    public Task<IEnumerable<GrantedPermission>> ListRolePermissions(int roleId)
    {
        var (connection, transaction) = ConnectionProvider.GetConnection();
        var sql = GetOrAddCachedSql($"""
                                     SELECT p.{_permissionMapping[nameof(Permission.Id)]},
                                            p.{_permissionMapping[nameof(Permission.Name)]},
                                            up.{_rolePermissionMapping[nameof(RolePermission.Mode)]} "Mode"
                                     FROM {_permissionMapping.SchemaQualifiedTableName} p
                                        JOIN {_rolePermissionMapping.SchemaQualifiedTableName} up
                                            ON up.{_rolePermissionMapping[nameof(RolePermission.PermissionId)]} = p.{_permissionMapping[nameof(Permission.Id)]}
                                     WHERE {_rolePermissionMapping[nameof(RolePermission.RoleId)]} = @{nameof(roleId)}
                                     """);
        return connection.QueryAsync<GrantedPermission>(sql, new { roleId }, transaction);
    }

    public async Task<IEnumerable<CustomGrantedPermission>> ListCustomPermissionsAsync(
        ListParam<CustomGrantedPermission> listParam)
    {
        var whereBuilder = new StringBuilder(" WHERE TRUE ");
        var parameters = new DynamicParameters();
        var filters = listParam.Filters.ToList();
        var typeNameFilter = filters.FirstOrDefault(x => x.PropertyName == nameof(CustomGrantedPermission.TypeName));
        if (typeNameFilter is not null)
        {
            var filterSql = GetFiltersSql<CustomGrantedPermission, CustomPermissionType>([typeNameFilter], parameters,
                "cpt",
                new Dictionary<string, string>
                    { { nameof(CustomGrantedPermission.TypeName), nameof(CustomPermissionType.TypeName) } });
            whereBuilder.Append($" AND {filterSql} ");
        }

        var permissionFilters = filters.Where(x =>
                new[] { nameof(CustomGrantedPermission.Name), nameof(CustomGrantedPermission.Id) }
                    .Contains(x.PropertyName))
            .ToList();
        if (permissionFilters.Count != 0)
        {
            var permissionFiltersSql =
                GetFiltersSql<CustomGrantedPermission, Permission>(permissionFilters, parameters, "p");
            whereBuilder.Append($" AND {permissionFiltersSql} ");
        }

        var customPermissionFilters = filters.Where(x =>
                new[] { nameof(CustomGrantedPermission.Key), nameof(CustomGrantedPermission.Mode) }
                    .Contains(x.PropertyName))
            .ToList();
        if (customPermissionFilters.Count != 0)
        {
            var customPermissionFiltersSql =
                GetFiltersSql<CustomGrantedPermission, CustomPermission>(customPermissionFilters, parameters, "cp");
            whereBuilder.Append($" AND {customPermissionFiltersSql} ");
        }

        var where = whereBuilder.ToString();

        var sql = $"""
                   SELECT p.{_permissionMapping[nameof(Permission.Id)]},
                          p.{_permissionMapping[nameof(Permission.Name)]},
                          cp.{_customPermissionMapping[nameof(CustomPermission.Mode)]} "Mode",
                          cp.{_customPermissionMapping[nameof(CustomPermission.Key)]} "Key",
                          cpt.{_customPermissionTypeMapping[nameof(CustomPermissionType.TypeName)]} "TypeName"
                   FROM {_permissionMapping.SchemaQualifiedTableName} p
                   JOIN {_customPermissionMapping.SchemaQualifiedTableName} cp
                       ON cp.{_customPermissionMapping[nameof(CustomPermission.PermissionId)]} = p.{_permissionMapping[nameof(Permission.Id)]}
                   JOIN {_customPermissionTypeMapping.SchemaQualifiedTableName} cpt
                       ON cpt.{_customPermissionTypeMapping[nameof(CustomPermissionType.Id)]} = cp.{_customPermissionMapping[nameof(CustomPermission.TypeId)]}
                   {where}
                   """;

        var (connection, transaction) = ConnectionProvider.GetConnection();
        return await connection.QueryAsync<CustomGrantedPermission>(sql, parameters, transaction);
    }

    public async Task<ListResult<Models.Output.Permission>> ListPermissionsAsync(
        ListParam<Models.Output.Permission> listParam)
    {
        var parameters = new DynamicParameters();
        var whereString = GetFiltersSql<Models.Output.Permission, Permission>(listParam.Filters, parameters);
        var orderByString = GetOrderBySql<Models.Output.Permission, Permission>(listParam.SortingDefinitions,
            _permissionMapping[nameof(Permission.Id)]!);
        var sql = $@"SELECT {_permissionMapping[nameof(Permission.Id)]} ""Id"",
                            {_permissionMapping[nameof(Permission.Name)]} ""Name""
                     FROM {_permissionMapping.SchemaQualifiedTableName}
                     {(!string.IsNullOrEmpty(whereString) ? $"WHERE {whereString}" : "")}
                     ORDER BY {orderByString}
                     {SqlConverter.ToSql(listParam.PaginationInfo)}";
        var countSql = $@"SELECT COUNT(*)
                          FROM {_permissionMapping.SchemaQualifiedTableName}
                          {(!string.IsNullOrEmpty(whereString) ? $"WHERE {whereString}" : "")}";
        var (connection, transaction) = ConnectionProvider.GetConnection();
        var res = await connection.QueryAsync<Permission>(sql, parameters, transaction);
        var count = await connection.ExecuteScalarAsync<int>(countSql, parameters, transaction);
        return new ListResult<Models.Output.Permission>(res.Select(x => _mapper.Map(x)), count);
    }
}