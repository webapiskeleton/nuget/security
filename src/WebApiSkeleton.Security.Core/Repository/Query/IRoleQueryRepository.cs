﻿using System.Diagnostics.Contracts;
using WebApiSkeleton.Security.Core.Models.Output;

namespace WebApiSkeleton.Security.Core.Repository.Query;

internal interface IRoleQueryRepository
{
    [Pure]
    public Task<ListResult<Role>> ListRolesAsync(PaginationInfo pagination);

    /// <summary>
    /// Get role information
    /// </summary>
    /// <param name="name">Role name</param>
    /// <returns>Role if found, otherwise null</returns>
    [Pure]
    public Task<Role?> GetRoleAsync(string name);

    /// <summary>
    /// Get role information
    /// </summary>
    /// <param name="id">Role id</param>
    /// <returns>Role if found, otherwise null</returns>
    [Pure]
    public Task<Role?> GetRoleAsync(int id);

    /// <summary>
    /// Get users that belong to the given role
    /// </summary>
    /// <param name="roleId">Role ID</param>
    /// <param name="pagination">Pagination parameters</param>
    /// <returns>Users that belong to the given role</returns>
    [Pure]
    public Task<ListResult<User>> GetRoleUsersAsync(int roleId, PaginationInfo pagination);
}