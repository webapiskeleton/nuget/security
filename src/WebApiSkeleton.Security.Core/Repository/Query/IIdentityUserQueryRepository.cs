﻿using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Verification;

namespace WebApiSkeleton.Security.Core.Repository.Query;

internal interface IIdentityUserQueryRepository
{
    public Task<User?> GetUserByIdAsync(int userId);
    public Task<User?> GetUserByEmailAsync(string email);
    public Task<User?> GetUserByUsernameAsync(string username);
    public Task<User?> GetUserByLoginAsync(string login);
    public Task<ListResult<User>> ListUsersAsync(ListParam<User> listParam);
    public Task<bool> IsUserPasswordCorrectAsync(int userId, byte[] passwordHash);
    public Task<IEnumerable<Claim>> GetClaimsForUserAsync(int userId);
    public Task<IEnumerable<Role>> GetRolesForUserAsync(int userId);
    public Task<bool> UserInRoleAsync(int userId, int roleId);
    public Task<StoredVerificationCodeInformation?> GetVerificationCodeInformationAsync(int userId, Guid sessionId);
}