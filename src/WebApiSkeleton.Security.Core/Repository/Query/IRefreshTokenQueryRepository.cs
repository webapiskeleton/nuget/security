﻿using WebApiSkeleton.Security.Core.Models.Output;

namespace WebApiSkeleton.Security.Core.Repository.Query;

internal interface IRefreshTokenQueryRepository
{
    public Task<RefreshToken?> GetTokenAsync(string token);
}