using System.Diagnostics.Contracts;
using WebApiSkeleton.Security.Core.Models.Output;

namespace WebApiSkeleton.Security.Core.Repository.Query;

internal interface IPermissionQueryRepository
{
    [Pure]
    public Task<Permission?> GetPermissionAsync(string name);

    [Pure]
    public Task<IEnumerable<GrantedPermission>> ListUserPermissions(int userId, bool includeRolePermissions);

    [Pure]
    public Task<IEnumerable<GrantedPermission>> ListRolePermissions(int roleId);

    public Task<IEnumerable<CustomGrantedPermission>> ListCustomPermissionsAsync(ListParam<CustomGrantedPermission> listParam);
    public Task<ListResult<Permission>> ListPermissionsAsync(ListParam<Permission> listParam);
}