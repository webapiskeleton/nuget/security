﻿using Microsoft.EntityFrameworkCore;
using WebApiSkeleton.Security.Core.Map;
using WebApiSkeleton.Security.Core.Models.Database;
using WebApiSkeleton.Security.Core.Repository.Base;
using WebApiSkeleton.Security.Core.Verification;

namespace WebApiSkeleton.Security.Core.Repository.Command.Impl;

internal sealed class UserCommandRepository : IdentityCommandRepositoryBase, IUserCommandRepository
{
    private readonly SecurityMapper _mapper;

    public UserCommandRepository(IdentityContext context) : base(context)
    {
        _mapper = new SecurityMapper();
    }

    public async Task<Models.Output.User> CreateUserAsync(Models.Output.User user,
        CancellationToken cancellationToken = default)
    {
        var databaseUser = _mapper.Map(user);
        databaseUser.CreatedOnTimeStamp = DateTime.Now.Ticks;
        await Context.AddAsync(databaseUser, cancellationToken);
        await Context.SaveChangesAsync(cancellationToken);
        return _mapper.Map(databaseUser);
    }

    public async Task SetUserPasswordAsync(int userId, byte[] passwordHash, int? changedBy = null,
        CancellationToken cancellationToken = default)
    {
        var model = new UserPassword
        {
            UserId = userId, PasswordHash = passwordHash,
            ChangedBy = changedBy ?? userId, IsActual = true,
            ChangedOn = DateTime.UtcNow
        };
        var set = Context.Set<UserPassword>();
        await set
            .Where(x => x.UserId == userId)
            .ExecuteUpdateAsync(setters => setters
                    .SetProperty(x => x.IsActual, false)
                    .SetProperty(x => x.ChangedOn, DateTime.UtcNow)
                    .SetProperty(x => x.ChangedBy, changedBy ?? userId),
                cancellationToken);
        await set.AddAsync(model, cancellationToken);
        await Context.SaveChangesAsync(cancellationToken);
    }

    public async Task<Models.Output.User> UpdateUserAsync(Models.Output.User user,
        CancellationToken cancellationToken = default)
    {
        var databaseUser = _mapper.Map(user);
        var res = Context
            .ChangeTracker
            .Entries<User>()
            .FirstOrDefault(x => x.State != EntityState.Detached && x.Entity.Id == user.Id);
        if (res is not null)
            res.State = EntityState.Detached;

        Context.Update(databaseUser);
        await Context.SaveChangesAsync(cancellationToken);
        return _mapper.Map(databaseUser);
    }

    public async Task DisableUserLoginAsync(int userId, CancellationToken cancellationToken = default)
    {
        var user = await Context.Users.FindAsync([userId], cancellationToken);
        user!.IsEnabled = false;
        Context.Users.Update(user);
        await Context.SaveChangesAsync(cancellationToken);
    }

    public async Task AddUserToRoleAsync(int userId, int roleId,
        CancellationToken cancellationToken = default)
    {
        var userRole = new UserRole { UserId = userId, RoleId = roleId };
        await Context.AddAsync(userRole, cancellationToken);
        await Context.SaveChangesAsync(cancellationToken);
    }

    public async Task RemoveUserFromRoleAsync(int userId, int roleId,
        CancellationToken cancellationToken = default)
    {
        var existingEntity = await Context
            .Set<UserRole>()
            .SingleAsync(x => x.UserId == userId && x.RoleId == roleId, cancellationToken);
        Context.Remove(existingEntity);
        await Context.SaveChangesAsync(cancellationToken);
    }

    public async Task AddEmailVerificationCodeAsync(int userId, VerificationCodeInformation codeInformation,
        CancellationToken cancellationToken = default)
    {
        var userVerificationCode = new UserVerificationCode
        {
            UserId = userId, VerificationCode = codeInformation.VerificationCode, SessionId = codeInformation.SessionId
        };
        await Context.AddAsync(userVerificationCode, cancellationToken);
        await Context.SaveChangesAsync(cancellationToken);
    }

    public async Task VerifyUserEmail(int userId, CancellationToken cancellationToken = default)
    {
        var user = await Context.Users.FindAsync([userId], cancellationToken);
        user!.EmailVerified = true;
        Context.Users.Update(user);
        await Context.SaveChangesAsync(cancellationToken);
    }

    public async Task AddUserClaimAsync(int userId, IEnumerable<Claim> claims,
        CancellationToken cancellationToken = default)
    {
        var claimsDictionary = claims.ToDictionary(x => x.Type, x => x.Value);
        var existingClaims = await Context
            .Set<UserClaim>()
            .Where(x => x.UserId == userId && claimsDictionary.Select(c => c.Key).Contains(x.Key))
            .ToListAsync(cancellationToken);
        foreach (var existingClaim in existingClaims)
        {
            existingClaim.Value = claimsDictionary[existingClaim.Key];
            claimsDictionary.Remove(existingClaim.Key);
            Context.Update(existingClaim);
        }

        foreach (var newClaim in claimsDictionary)
        {
            var userClaim = new UserClaim { UserId = userId, Key = newClaim.Key, Value = newClaim.Value };
            Context.Add(userClaim);
        }

        await Context.SaveChangesAsync(cancellationToken);
    }

    public async Task RemoveClaimAsync(int userId, IEnumerable<string> claimKeys,
        CancellationToken cancellationToken = default)
    {
        await Context.Set<UserClaim>()
            .Where(x => x.UserId == userId && claimKeys.Contains(x.Key))
            .ExecuteDeleteAsync(cancellationToken);
        await Context.SaveChangesAsync(cancellationToken);
    }
}