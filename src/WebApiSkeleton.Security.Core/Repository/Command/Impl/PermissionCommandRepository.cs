using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WebApiSkeleton.Security.Core.Models.Database;
using WebApiSkeleton.Security.Core.Models.DTO;
using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Repository.Base;
using Role = WebApiSkeleton.Security.Core.Models.Output.Role;
using User = WebApiSkeleton.Security.Core.Models.Output.User;
using CustomPermission = WebApiSkeleton.Security.Core.Models.Output.CustomPermission;

namespace WebApiSkeleton.Security.Core.Repository.Command.Impl;

internal sealed class PermissionCommandRepository : IdentityCommandRepositoryBase, IPermissionCommandRepository
{
    private readonly ILogger<PermissionCommandRepository> _logger;

    public PermissionCommandRepository(IdentityContext context, ILogger<PermissionCommandRepository> logger) :
        base(context)
    {
        _logger = logger;
    }

    public async Task<int> CreatePermissionAsync(string name, CancellationToken cancellationToken = default)
    {
        var permission = new Models.Database.Permission { Name = name };
        await Context.AddAsync(permission, cancellationToken);
        await Context.SaveChangesAsync(cancellationToken);
        return permission.Id;
    }

    public async Task AddPermissionAsync<T>(T entity, IEnumerable<PermissionDto> permissions,
        CancellationToken cancellationToken = default) where T : IGrantable
    {
        var permissionsHashSet = permissions.ToHashSet();
        var permissionNames = permissionsHashSet
            .Select(x => x.Name)
            .ToHashSet();

        var permissionIdsByName = Context
            .Set<Models.Database.Permission>()
            .Where(x => permissionNames.Contains(x.Name))
            .Select(x => new { x.Id, x.Name })
            .ToDictionary(x => x.Name, x => x.Id);
        permissionsHashSet.RemoveWhere(x => !permissionIdsByName.ContainsKey(x.Name));
        var permissionIds = permissionIdsByName.Values;

        HashSet<PermissionDto>? existingPermissions;
        switch (entity)
        {
            case Role role:
                existingPermissions = Context
                    .Set<RolePermission>()
                    .Include(x => x.Permission)
                    .Where(x => x.RoleId == role.Id
                                && permissionIds.Contains(x.PermissionId))
                    .Select(x => new PermissionDto(x.Permission!.Name, x.Mode))
                    .ToHashSet();
                permissionsHashSet.ExceptWith(existingPermissions);
                var rolePermissionsToAdd = permissionsHashSet
                    .Select(x => new RolePermission
                    {
                        RoleId = role.Id, PermissionId = permissionIdsByName[x.Name], Mode = x.Mode
                    });
                await Context.AddRangeAsync(rolePermissionsToAdd, cancellationToken);
                break;
            case User user:
                existingPermissions = Context
                    .Set<UserPermission>()
                    .Include(x => x.Permission)
                    .Where(x => x.UserId == user.Id
                                && permissionIds.Contains(x.PermissionId))
                    .Select(x => new PermissionDto(x.Permission!.Name, x.Mode))
                    .ToHashSet();
                permissionsHashSet.ExceptWith(existingPermissions);
                var userPermissionsToAdd = permissionsHashSet
                    .Select(x => new UserPermission
                    {
                        UserId = user.Id, PermissionId = permissionIdsByName[x.Name], Mode = x.Mode
                    })
                    .ToList();
                await Context.AddRangeAsync(userPermissionsToAdd, cancellationToken);
                break;
            case CustomPermission customPermission:
                var customPermissionType = await Context
                    .Set<CustomPermissionType>()
                    .FirstOrDefaultAsync(x => x.TypeName == customPermission.CustomPermissionTypeName,
                        cancellationToken: cancellationToken);

                customPermissionType ??= new CustomPermissionType
                    { TypeName = customPermission.CustomPermissionTypeName };
                Context.Entry(customPermissionType).State = EntityState.Unchanged;
                if (customPermissionType.Id == default)
                {
                    await Context.AddAsync(customPermissionType, cancellationToken);
                }

                existingPermissions = Context
                    .Set<Models.Database.CustomPermission>()
                    .Include(x => x.Permission)
                    .Where(x => x.Key == customPermission.Key
                                && permissionIds.Contains(x.PermissionId))
                    .Select(x => new PermissionDto(x.Permission!.Name, x.Mode))
                    .ToHashSet();
                permissionsHashSet.ExceptWith(existingPermissions);
                var customPermissions = permissionsHashSet
                    .Select(x => new Models.Database.CustomPermission
                    {
                        Key = customPermission.Key, PermissionId = permissionIdsByName[x.Name], Mode = x.Mode,
                        PermissionType = customPermissionType,
                    })
                    .ToList();
                await Context.AddRangeAsync(customPermissions, cancellationToken);
                break;
            default:
                throw new ArgumentException();
        }

        await Context.SaveChangesAsync(cancellationToken);
    }

    public async Task RemovePermissionAsync<T>(T entity, IEnumerable<PermissionDto> permissions,
        CancellationToken cancellationToken = default) where T : IGrantable
    {
        var permissionsHashSet = permissions.ToHashSet();
        var permissionNames = permissionsHashSet
            .Select(x => x.Name)
            .ToHashSet();

        var permissionIdsByName = Context
            .Set<Models.Database.Permission>()
            .Where(x => permissionNames.Contains(x.Name))
            .Select(x => new { x.Id, x.Name })
            .ToDictionary(x => x.Name, x => x.Id);
        permissionsHashSet.RemoveWhere(x => !permissionIdsByName.ContainsKey(x.Name));
        var permissionIds = permissionIdsByName.Values;

        switch (entity)
        {
            case Role:
                var existingRolePermissions = Context
                    .Set<RolePermission>()
                    .Include(x => x.Permission)
                    .Where(x => permissionIds.Contains(x.PermissionId))
                    .ToHashSet();
                var rolePermissionIdsToDelete = new List<int>();
                foreach (var permission in existingRolePermissions)
                {
                    if (permissionsHashSet.FirstOrDefault(x =>
                            permissionIdsByName[x.Name] == permission.PermissionId &&
                            x.Mode == permission.Mode) is not null)
                    {
                        rolePermissionIdsToDelete.Add(permission.Id);
                    }
                }

                await Context
                    .Set<RolePermission>()
                    .Where(x => rolePermissionIdsToDelete.Contains(x.Id))
                    .ExecuteDeleteAsync(cancellationToken: cancellationToken);
                break;
            case User:
                var existingUserPermissions = Context
                    .Set<UserPermission>()
                    .Include(x => x.Permission)
                    .Where(x => permissionIds.Contains(x.PermissionId))
                    .ToHashSet();
                var userPermissionIdsToDelete = new List<int>();
                foreach (var permission in existingUserPermissions)
                {
                    if (permissionsHashSet.FirstOrDefault(x =>
                            permissionIdsByName[x.Name] == permission.PermissionId &&
                            x.Mode == permission.Mode) is not null)
                    {
                        userPermissionIdsToDelete.Add(permission.Id);
                    }
                }

                await Context
                    .Set<UserPermission>()
                    .Where(x => userPermissionIdsToDelete.Contains(x.Id))
                    .ExecuteDeleteAsync(cancellationToken: cancellationToken);
                break;
            case CustomPermission customPermission:
                var existingCustomPermissions = Context
                    .Set<Models.Database.CustomPermission>()
                    .Include(x => x.Permission)
                    .Where(x => permissionIds.Contains(x.PermissionId))
                    .ToHashSet();
                var customPermissionIdsToDelete = new List<int>();
                foreach (var permission in existingCustomPermissions)
                {
                    if (permissionsHashSet.FirstOrDefault(x =>
                            permissionIdsByName[x.Name] == permission.PermissionId) is not null)
                    {
                        customPermissionIdsToDelete.Add(permission.Id);
                    }
                }

                await Context
                    .Set<Models.Database.CustomPermission>()
                    .Where(x => customPermissionIdsToDelete.Contains(x.Id))
                    .ExecuteDeleteAsync(cancellationToken: cancellationToken);
                break;
        }

        await Context.SaveChangesAsync(cancellationToken);
    }
}