﻿using Microsoft.EntityFrameworkCore;
using WebApiSkeleton.Security.Core.Models.Database;

namespace WebApiSkeleton.Security.Core.Repository.Command.Impl;

internal sealed class RefreshCommandRepository : IRefreshCommandRepository
{
    private readonly IdentityContext _context;

    public RefreshCommandRepository(IdentityContext context)
    {
        _context = context;
    }

    public async Task CreateTokenAsync(RefreshToken token, CancellationToken cancellationToken = default)
    {
        _context.RefreshTokens.Add(token);
        await _context.SaveChangesAsync(cancellationToken);
    }

    public async Task UseTokenAsync(string token, CancellationToken cancellationToken = default)
    {
        await _context.RefreshTokens
            .Where(x => x.Token == token)
            .ExecuteUpdateAsync(x => x.SetProperty(
                e => e.IsUsed,
                _ => true), cancellationToken: cancellationToken);
    }

    public async Task<int> RevokeTokenIfExistsAsync(int userId, string deviceId, CancellationToken cancellationToken = default)
    {
       return await _context.RefreshTokens
            .Where(x => x.UserId == userId && x.DeviceId == deviceId && !x.IsUsed && !x.IsRevoked)
            .ExecuteUpdateAsync(x => x.SetProperty(
                e => e.IsRevoked,
                _ => true), cancellationToken: cancellationToken);
    }
}