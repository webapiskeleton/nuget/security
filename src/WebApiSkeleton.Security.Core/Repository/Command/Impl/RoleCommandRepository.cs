﻿using WebApiSkeleton.Security.Core.Models.Database;
using WebApiSkeleton.Security.Core.Repository.Base;

namespace WebApiSkeleton.Security.Core.Repository.Command.Impl;

internal sealed class RoleCommandRepository : IdentityCommandRepositoryBase, IRoleCommandRepository
{
    public RoleCommandRepository(IdentityContext context) : base(context)
    {
    }

    public async Task<int> CreateRoleAsync(string name, CancellationToken cancellationToken = default)
    {
        var role = new Role { Name = name };
        await Context.AddAsync(role, cancellationToken);
        await Context.SaveChangesAsync(cancellationToken);
        return role.Id;
    }

    public async Task UpdateRoleNameAsync(int roleId, string newName, CancellationToken cancellationToken = default)
    {
        var role = await Context.FindAsync<Role>(roleId);
        role!.Name = newName;
        Context.Update(role);
        await Context.SaveChangesAsync(cancellationToken);
    }
}