﻿using WebApiSkeleton.Security.Core.Models.Database;

namespace WebApiSkeleton.Security.Core.Repository.Command;

internal interface IRefreshCommandRepository
{
    public Task CreateTokenAsync(RefreshToken token, CancellationToken cancellationToken = default);
    public Task UseTokenAsync(string token, CancellationToken cancellationToken = default);
    public Task<int> RevokeTokenIfExistsAsync(int userId, string deviceId, CancellationToken cancellationToken = default);
}