﻿namespace WebApiSkeleton.Security.Core.Repository.Command;

internal interface IRoleCommandRepository
{
    /// <summary>
    /// Create role with given name
    /// </summary>
    /// <param name="name">Role name</param>
    /// <param name="cancellationToken">Cancellation token</param>
    /// <returns>ID of created role</returns>
    public Task<int> CreateRoleAsync(string name, CancellationToken cancellationToken = default);

    /// <summary>
    /// Update the name of role
    /// </summary>
    /// <param name="roleId">Role ID</param>
    /// <param name="newName">New name to set to role</param>
    /// <param name="cancellationToken">Cancellation token</param>
    /// <returns>Result response</returns>
    public Task UpdateRoleNameAsync(int roleId, string newName,
        CancellationToken cancellationToken = default);    
}