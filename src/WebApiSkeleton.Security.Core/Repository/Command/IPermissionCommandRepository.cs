using WebApiSkeleton.Security.Core.Models.DTO;
using WebApiSkeleton.Security.Core.Models.Output;

namespace WebApiSkeleton.Security.Core.Repository.Command;

internal interface IPermissionCommandRepository
{
    public Task<int> CreatePermissionAsync(string name, CancellationToken cancellationToken = default);

    public Task AddPermissionAsync<T>(T entity, IEnumerable<PermissionDto> permissions,
        CancellationToken cancellationToken = default) where T : IGrantable;

    public Task RemovePermissionAsync<T>(T entity, IEnumerable<PermissionDto> permissions,
        CancellationToken cancellationToken = default) where T : IGrantable;
}