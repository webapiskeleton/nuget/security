﻿using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Verification;

namespace WebApiSkeleton.Security.Core.Repository.Command;

internal interface IUserCommandRepository
{
    public Task<User> CreateUserAsync(Models.Output.User user, CancellationToken cancellationToken = default);

    public Task SetUserPasswordAsync(int userId, byte[] passwordHash, int? changedBy = null,
        CancellationToken cancellationToken = default);

    public Task<User> UpdateUserAsync(Models.Output.User user, CancellationToken cancellationToken = default);

    public Task DisableUserLoginAsync(int userId, CancellationToken cancellationToken = default);

    public Task AddUserToRoleAsync(int userId, int roleId,
        CancellationToken cancellationToken = default);

    public Task RemoveUserFromRoleAsync(int userId, int roleId,
        CancellationToken cancellationToken = default);

    public Task AddEmailVerificationCodeAsync(int userId, VerificationCodeInformation codeInformation,
        CancellationToken cancellationToken = default);

    public Task VerifyUserEmail(int userId, CancellationToken cancellationToken = default);
    public Task AddUserClaimAsync(int userId, IEnumerable<Claim> claims, CancellationToken cancellationToken = default);

    public Task RemoveClaimAsync(int userId, IEnumerable<string> claimKeys,
        CancellationToken cancellationToken = default);
}