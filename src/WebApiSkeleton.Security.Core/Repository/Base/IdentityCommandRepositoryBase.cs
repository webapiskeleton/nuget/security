﻿namespace WebApiSkeleton.Security.Core.Repository.Base;

internal abstract class IdentityCommandRepositoryBase
{
    protected readonly IdentityContext Context;

    protected IdentityCommandRepositoryBase(IdentityContext context)
    {
        Context = context;
    }
}