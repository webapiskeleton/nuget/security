﻿using System.Collections.Concurrent;
using System.Runtime.CompilerServices;
using WebApiSkeleton.SearchSqlGeneration;

namespace WebApiSkeleton.Security.Core.Repository.Base;

internal class SecurityQueryRepositoryBase
{
    private readonly ConcurrentDictionary<string, string> _methodSqlCache = new();

    protected readonly SecurityNpgsqlConnectionProvider ConnectionProvider;
    protected readonly ISearchSqlConverter SqlConverter;

    protected SecurityQueryRepositoryBase(SecurityNpgsqlConnectionProvider connectionProvider,
        ISearchSqlConverter sqlConverter)
    {
        ConnectionProvider = connectionProvider;
        SqlConverter = sqlConverter;
    }

    protected string GetOrAddCachedSql(string sql,
        string keyParameter = "",
        [CallerMemberName] string callerName = "",
        [CallerLineNumber] int callerLineNumber = 0)
    {
        return _methodSqlCache.GetOrAdd($"{callerName}{callerLineNumber}{keyParameter}", sql);
    }

    protected string GetFiltersSql<TSource, TTarget>(IEnumerable<FilterDefinition<TSource>> definitions,
        DynamicParameters parameters, string? tableAlias = null, Dictionary<string, string>? propertyMappings = null)
        where TTarget : class where TSource : class
    {
        var whereConditions = new List<string>();
        var filters = definitions.Select(x =>
            x.ToFilterDefinition<TSource, TTarget>(
                propertyMappings?.TryGetValue(x.PropertyName, out var targetPropertyName) ?? false
                    ? targetPropertyName
                    : null));
        foreach (var filter in filters)
        {
            var sqlFilter = SqlConverter.ToSqlFilter(filter, tableAlias);
            if (sqlFilter is null)
                continue;
            parameters.Add(sqlFilter.ParameterName, sqlFilter.Value);
            whereConditions.Add(sqlFilter.Sql);
        }

        return whereConditions.Count == 0 ? "" : $" {string.Join(" AND ", whereConditions)} ";
    }

    protected string GetOrderBySql<TSource, TTarget>(IEnumerable<SortingDefinition<TSource>> definitions,
        string defaultSortColumn) where TTarget : class where TSource : class
    {
        var sortingDefinitions = definitions
            .Select(x => x.ToSortingDefinition<TSource, TTarget>());
        var sortConditions = sortingDefinitions
            .Select(sortingDefinition => SqlConverter.ToSql(sortingDefinition))
            .ToList();

        return $" {(sortConditions.Count != 0 ? string.Join(", ", sortConditions) : defaultSortColumn)} ";
    }
}