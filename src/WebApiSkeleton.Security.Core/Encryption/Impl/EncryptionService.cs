﻿using System.Security.Cryptography;
using System.Text;
using WebApiSkeleton.Security.Core.Extensions;
using WebApiSkeleton.Security.Core.Settings;

namespace WebApiSkeleton.Security.Core.Encryption.Impl;

/// <inheritdoc />
public sealed class EncryptionService : IEncryptionService
{
    private readonly HMACSHA256 _encryptor;

    public EncryptionService(PasswordEncryptionSettings passwordEncryptionSettings)
    {
        _encryptor = new HMACSHA256(Encoding.ASCII.GetBytes(passwordEncryptionSettings.PasswordEncryptionKey));
    }

    /// <inheritdoc />
    public async Task<byte[]> EncryptPasswordAsync(string password)
    {
        var stream = await password.GenerateStreamAsync();

        return await _encryptor.ComputeHashAsync(stream);
    }
}