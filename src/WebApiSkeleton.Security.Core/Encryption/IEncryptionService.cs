﻿namespace WebApiSkeleton.Security.Core.Encryption;

/// <summary>
/// Service that uses hash encryption for passwords
/// </summary>
public interface IEncryptionService
{
    /// <summary>
    /// Encrypt password to hash by using <see cref="WebApiSkeleton.Security.Core.Settings.PasswordEncryptionSettings"/>
    /// </summary>
    /// <param name="password">Password to encrypt</param>
    /// <returns>Hash byte sequence</returns>
    public Task<byte[]> EncryptPasswordAsync(string password);
}