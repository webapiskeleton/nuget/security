﻿using Microsoft.IdentityModel.JsonWebTokens;

namespace WebApiSkeleton.Security.Core.Encryption;

internal static class EncryptionConstants
{
    public const string ValidEncryptionAlgorithm = JwtConstants.DirectKeyUseAlg;
    public const string ValidAlgorithm = "HS256";
}