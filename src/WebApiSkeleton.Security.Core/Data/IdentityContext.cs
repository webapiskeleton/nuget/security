﻿using Microsoft.EntityFrameworkCore;
using WebApiSkeleton.Security.Core.Enums;
using WebApiSkeleton.Security.Core.Extensions.DatabaseContextExtensions;
using WebApiSkeleton.Security.Core.Models.Database;

namespace WebApiSkeleton.Security.Core.Data;

public sealed class IdentityContext : DbContext
{
    private readonly string _defaultSchema;

    public IdentityContext(DbContextOptions<IdentityContext> options)
        : base(options)
    {
        var tableNamingExtension =
            options.Extensions.FirstOrDefault(x => x is WebApiSkeletonTableNamingExtension) as
                WebApiSkeletonTableNamingExtension ?? new WebApiSkeletonTableNamingExtension();
        _defaultSchema = tableNamingExtension.DefaultSchema;
    }

    internal DbSet<User> Users { get; set; } = null!;
    internal DbSet<RefreshToken> RefreshTokens { get; set; } = null!;

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.HasDefaultSchema(_defaultSchema);

        #region Identity role

        modelBuilder.Entity<Role>()
            .HasIndex(x => x.Name)
            .IsUnique();

        #endregion

        #region Identity user

        modelBuilder.Entity<User>()
            .HasIndex(x => x.Email)
            .IsUnique();

        modelBuilder.Entity<User>()
            .HasIndex(x => x.PhoneNumber)
            .IsUnique();

        modelBuilder.Entity<User>()
            .HasIndex(x => x.Username)
            .IsUnique();

        modelBuilder.Entity<User>()
            .HasMany(x => x.Roles)
            .WithMany(x => x.Users)
            .UsingEntity<UserRole>(
                x => x.HasOne<Role>(z => z.Role).WithMany().HasForeignKey(z => z.RoleId),
                x => x.HasOne<User>(z => z.User).WithMany().HasForeignKey(z => z.UserId)
            );

        modelBuilder.Entity<User>()
            .HasMany<UserPassword>()
            .WithOne()
            .HasForeignKey(x => x.UserId)
            .OnDelete(DeleteBehavior.NoAction);


        modelBuilder.Entity<User>()
            .HasMany<UserClaim>(x => x.Claims)
            .WithOne(x => x.User)
            .HasForeignKey(x => x.UserId)
            .OnDelete(DeleteBehavior.NoAction);

        modelBuilder.Entity<User>()
            .HasMany<UserVerificationCode>()
            .WithOne(x => x.User)
            .HasForeignKey(x => x.UserId)
            .OnDelete(DeleteBehavior.NoAction);

        #region Change logs

        modelBuilder.Entity<User>()
            .HasMany<UserPassword>()
            .WithOne()
            .HasForeignKey(x => x.ChangedBy)
            .OnDelete(DeleteBehavior.NoAction);

        #endregion

        #endregion

        #region Verification code

        modelBuilder.Entity<UserVerificationCode>()
            .Property(x => x.CreatedAt);

        #endregion

        #region UserRole

        modelBuilder.Entity<UserRole>()
            .HasIndex(x => new { x.UserId, x.RoleId })
            .IsUnique();

        #endregion

        #region User password

        var entityProperty = modelBuilder.Model.FindEntityType(typeof(UserPassword))!
            .GetProperty(nameof(UserPassword.IsActual));
        modelBuilder.Entity<UserPassword>()
            .HasIndex(x => new { x.UserId, x.IsActual })
            .IsUnique()
            .HasFilter(@$"""{entityProperty.GetColumnName()}""='1'");

        #endregion

        #region Role permissions

        modelBuilder.Entity<Permission>()
            .HasMany(x => x.Roles)
            .WithMany(x => x.Permissions)
            .UsingEntity<RolePermission>(
                x => x.HasOne<Role>(z => z.Role).WithMany().HasForeignKey(z => z.RoleId),
                x => x.HasOne<Permission>(z => z.Permission).WithMany().HasForeignKey(z => z.PermissionId)
            );

        entityProperty = modelBuilder.Model.FindEntityType(typeof(RolePermission))!
            .GetProperty(nameof(RolePermission.Mode));
        modelBuilder.Entity<RolePermission>()
            .ToTable(t => t.HasCheckConstraint("ValidValues",
                $"{entityProperty.GetColumnName()} IN ({string.Join(", ", Enum.GetValues<PermissionMode>().Select(x => (int)x))})"));

        modelBuilder.Entity<RolePermission>()
            .HasIndex(x => new { x.RoleId, x.PermissionId, x.Mode })
            .IsUnique();

        #endregion

        #region User permissions

        modelBuilder.Entity<Permission>()
            .HasMany(x => x.Users)
            .WithMany(x => x.Permissions)
            .UsingEntity<UserPermission>(
                x => x.HasOne<User>(z => z.User).WithMany().HasForeignKey(z => z.UserId),
                x => x.HasOne<Permission>(z => z.Permission).WithMany().HasForeignKey(z => z.PermissionId)
            );

        entityProperty = modelBuilder.Model.FindEntityType(typeof(UserPermission))!
            .GetProperty(nameof(UserPermission.Mode));
        modelBuilder.Entity<UserPermission>()
            .ToTable(t => t.HasCheckConstraint("ValidValues",
                $"{entityProperty.GetColumnName()} IN ({string.Join(", ", Enum.GetValues<PermissionMode>().Select(x => (int)x))})"));

        modelBuilder.Entity<UserPermission>()
            .HasIndex(x => new { x.PermissionId, x.UserId, x.Mode })
            .IsUnique();

        #endregion

        #region Custom permissions

        modelBuilder.Entity<CustomPermission>()
            .HasOne(x => x.PermissionType)
            .WithMany(x => x.Permissions)
            .HasForeignKey(x => x.TypeId);

        modelBuilder.Entity<CustomPermission>()
            .HasOne(x => x.Permission)
            .WithMany(x => x.CustomPermissions)
            .HasForeignKey(x => x.PermissionId);

        modelBuilder.Entity<CustomPermission>()
            .HasIndex(x => new { x.Key, x.PermissionId, x.TypeId })
            .IsUnique();

        #endregion

        #region Refresh token

        modelBuilder.Entity<RefreshToken>()
            .HasOne<User>()
            .WithMany()
            .HasForeignKey(x => x.UserId);

        var entityModel = modelBuilder.Model.FindEntityType(typeof(RefreshToken))!;
        modelBuilder.Entity<RefreshToken>()
            .HasIndex(x => new { x.IsUsed, x.IsRevoked, x.UserId, x.DeviceId })
            .IsUnique()
            .HasFilter(
                @$"""{entityModel.GetProperty(nameof(RefreshToken.IsUsed)).GetColumnName()}""='0' AND ""{entityModel.GetProperty(nameof(RefreshToken.IsRevoked)).GetColumnName()}""='0'");

        #endregion
    }
}