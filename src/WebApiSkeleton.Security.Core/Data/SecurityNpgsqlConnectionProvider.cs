﻿using System.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace WebApiSkeleton.Security.Core.Data;

internal sealed class SecurityNpgsqlConnectionProvider
{
    private readonly IdentityContext _context;
    
    public SecurityNpgsqlConnectionProvider(IdentityContext context)
    {
        _context = context;
    }

    public (IDbConnection connection, IDbTransaction? transaction) GetConnection()
    {
        var connection = _context.Database.GetDbConnection();
        return (connection, _context.Database.CurrentTransaction?.GetDbTransaction());
    }
}