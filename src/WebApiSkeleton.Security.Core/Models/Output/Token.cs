﻿namespace WebApiSkeleton.Security.Core.Models.Output;

public sealed class Token
{
    public required string AccessToken { get; init; }
    public required string RefreshToken { get; init; }
}