namespace WebApiSkeleton.Security.Core.Models.Output;

public sealed class User : IGrantable
{
    public int Id { get; init; }
    public required string Username { get; init; }
    public required string Email { get; init; }
    public bool EmailVerified { get; init; }
    public string? PhoneNumber { get; init; }
    
    public bool IsEnabled { get; init; }
}