namespace WebApiSkeleton.Security.Core.Models.Output;

public sealed class Role : IGrantable
{
    public int Id { get; set; }
    public required string Name { get; set; }
}