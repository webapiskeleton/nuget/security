﻿namespace WebApiSkeleton.Security.Core.Models.Output;

public sealed class Password
{
    public int UserId { get; init; }
    public required string Value { get; init; }
}