﻿using WebApiSkeleton.Security.Core.Enums;

namespace WebApiSkeleton.Security.Core.Models.Output.Response;

public sealed class AuthorizationResponse
{
    public required AuthenticationResult Result { get; init; }
    public required string Message { get; init; }
    public string? AccessToken { get; init; }
    public string? RefreshToken { get; init; }

    public static AuthorizationResponse Success(Token token) => new()
    {
        Message = string.Empty, AccessToken = token.AccessToken, RefreshToken = token.RefreshToken,
        Result = AuthenticationResult.Success
    };

    public static AuthorizationResponse AuthorizationFailed(string message) => new()
    {
        Result = AuthenticationResult.AuthorizationFailed, Message = message
    };

    public static AuthorizationResponse WrongCredentials() => new()
    {
        Result = AuthenticationResult.WrongCredentials, Message = "Wrong login or password"
    };

    public static AuthorizationResponse UserDisabled() => new()
    {
        Result = AuthenticationResult.UserDisabled, Message = "This user is disabled and cannot log in"
    };

    public static AuthorizationResponse TooManyAttempts() => new()
    {
        Result = AuthenticationResult.TooManyAttempts, Message = "Too many simultaneous authorization attempts"
    };

    public static AuthorizationResponse RefreshIsNotAvailable() => new()
    {
        Result = AuthenticationResult.PotentialHack, Message = "User needs to authorize with login and password"
    };
    
    public static AuthorizationResponse BruteforceDetected() => new()
    {
        Result = AuthenticationResult.UserBruteForced,
        Message = "Too many authorization requests. Try again later"
    };
}