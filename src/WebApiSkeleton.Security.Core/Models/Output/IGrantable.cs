namespace WebApiSkeleton.Security.Core.Models.Output;

/// <summary>
/// Entity that can be granted a permission
/// </summary>
public interface IGrantable
{
}