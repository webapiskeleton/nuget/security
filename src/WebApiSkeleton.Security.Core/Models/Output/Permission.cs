﻿namespace WebApiSkeleton.Security.Core.Models.Output;

public class Permission
{
    public int Id { get; init; }

    public required string Name { get; init; }
}