using WebApiSkeleton.Security.Core.Enums;

namespace WebApiSkeleton.Security.Core.Models.Output;

public class GrantedPermission : Permission
{
    public required PermissionMode Mode { get; init; }
}

/// <summary>
/// Custom permission that was granted to some entity
/// </summary>
public sealed class CustomGrantedPermission : GrantedPermission
{
    /// <summary>
    /// Key of custom permission
    /// </summary>
    public required string Key { get; set; }
    
    /// <summary>
    /// Name of the type of custom permission
    /// </summary>
    public required string TypeName { get; set; }
}