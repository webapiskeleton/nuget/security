﻿namespace WebApiSkeleton.Security.Core.Models.Output;

/// <summary>
/// Additional permission for some entity (not user or role)
/// </summary>
public sealed class CustomPermission : IGrantable
{
    /// <summary>
    /// Key of custom permission
    /// </summary>
    public required string Key { get; init; }

    /// <summary>
    /// Name of the type of custom permission
    /// </summary>
    public required string CustomPermissionTypeName { get; init; }
}