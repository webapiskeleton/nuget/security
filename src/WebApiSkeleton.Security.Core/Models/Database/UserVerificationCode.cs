namespace WebApiSkeleton.Security.Core.Models.Database;

[IdentityModel]
internal sealed class UserVerificationCode
{
    public int Id { get; set; }

    public int UserId { get; set; }
    public User? User { get; set; }

    public string VerificationCode { get; set; } = null!;
    public Guid SessionId { get; set; }

    public DateTime CreatedAt { get; } = DateTime.UtcNow;
}