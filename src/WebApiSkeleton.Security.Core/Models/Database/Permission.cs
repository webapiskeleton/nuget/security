namespace WebApiSkeleton.Security.Core.Models.Database;

[IdentityModel]
internal sealed class Permission
{
    public int Id { get; set; }

    [MaxLength(200)]
    public required string Name { get; set; }

    public List<Role> Roles { get; set; } = new();
    public List<User> Users { get; set; } = new();
    public List<CustomPermission> CustomPermissions { get; set; } = new();
}