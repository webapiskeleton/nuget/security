﻿using WebApiSkeleton.Security.Core.Enums;

namespace WebApiSkeleton.Security.Core.Models.Database;

[IdentityModel]
internal sealed class CustomPermission : IGrantPermission
{
    public int Id { get; set; }

    [MaxLength(200)] public required string Key { get; set; }

    public int TypeId { get; set; }
    public CustomPermissionType? PermissionType { get; set; }

    public PermissionMode Mode { get; set; }

    public int PermissionId { get; set; }
    public Permission? Permission { get; set; }
}