using WebApiSkeleton.Security.Core.Enums;

namespace WebApiSkeleton.Security.Core.Models.Database;

public interface IGrantPermission
{
    public int PermissionId { get; }
    public PermissionMode Mode { get; }
}