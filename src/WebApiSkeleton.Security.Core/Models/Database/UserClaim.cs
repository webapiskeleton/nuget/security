﻿namespace WebApiSkeleton.Security.Core.Models.Database;

[IdentityModel]
internal sealed class UserClaim
{
    public int Id { get; set; }

    public int UserId { get; set; }
    public User? User { get; set; }

    [MaxLength(200)]
    public string Key { get; set; } = null!;
    
    [MaxLength(200)]
    public string Value { get; set; } = null!;
}