﻿namespace WebApiSkeleton.Security.Core.Models.Database;

[IdentityModel]
internal sealed class UserPassword
{
    public int Id { get; set; }
    public int UserId { get; set; }
    
    [MaxLength(50)]
    public byte[] PasswordHash { get; set; } = null!;
    public bool IsActual { get; set; }
    public DateTime ChangedOn { get; set; }
    public int ChangedBy { get; set; }
}