﻿namespace WebApiSkeleton.Security.Core.Models.Database;

[IdentityModel]
internal sealed class RefreshToken
{
    public int Id { get; set; }

    /// <summary>
    /// Server-generated token Id used on client
    /// </summary>
    [MaxLength(250)]
    public string Token { get; set; } = null!;

    /// <summary>
    /// Access token Id that is related to this refresh token
    /// </summary>
    [MaxLength(250)]
    public string JwtId { get; set; } = null!;
    public bool IsUsed { get; set; }
    public bool IsRevoked { get; set; }
    public DateTime CreationDate { get; set; }
    public DateTime ExpiryDate { get; set; }
    public int UserId { get; set; }

    [MaxLength(250)]
    public string DeviceId { get; set; } = null!;
}