﻿namespace WebApiSkeleton.Security.Core.Models.Database;

[IdentityModel]
internal class CustomPermissionType
{
    public int Id { get; set; }
    
    [MaxLength(200)]
    public required string TypeName { get; set; }

    public List<CustomPermission> Permissions { get; set; } = new();
}