﻿namespace WebApiSkeleton.Security.Core.Models.Database;

[IdentityModel]
internal sealed class Role
{
    public int Id { get; set; }

    [MaxLength(200)]
    public required string Name { get; set; }

    public List<User> Users { get; set; } = null!;
    public List<Permission> Permissions { get; set; } = new();
}