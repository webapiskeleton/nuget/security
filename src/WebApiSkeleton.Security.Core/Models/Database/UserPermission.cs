using WebApiSkeleton.Security.Core.Enums;

namespace WebApiSkeleton.Security.Core.Models.Database;

[IdentityModel]
internal sealed class UserPermission : IGrantPermission
{
    public int Id { get; set; }

    public int UserId { get; set; }
    public User? User { get; set; }
    
    public PermissionMode Mode { get; set; }
    
    public int PermissionId { get; set; }
    public Permission? Permission { get; set; }
}