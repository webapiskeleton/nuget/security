using WebApiSkeleton.Security.Core.Enums;

namespace WebApiSkeleton.Security.Core.Models.Database;

[IdentityModel]
internal sealed class RolePermission : IGrantPermission
{
    public int Id { get; set; }
    public int RoleId { get; set; }
    public Role? Role { get; set; }
    
    public PermissionMode Mode { get; set; }
    
    public int PermissionId { get; set; }
    public Permission? Permission { get; set; }
}