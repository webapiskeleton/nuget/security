﻿namespace WebApiSkeleton.Security.Core.Models.Database;

[IdentityModel]
internal sealed class User
{
    public int Id { get; set; }
    
    [MaxLength(50)]
    public required string Username { get; set; }
    
    [MaxLength(200)]
    public required string Email { get; set; }
    public bool EmailVerified { get; set; }
    
    [MaxLength(200)]
    public string? PhoneNumber { get; set; }
    
    public bool IsEnabled { get; set; }
    public long CreatedOnTimeStamp { get; set; }

    public List<UserClaim> Claims { get; set; } = new();
    public List<Permission> Permissions { get; set; } = new();
    public List<Role> Roles { get; set; } = new();
}