﻿namespace WebApiSkeleton.Security.Core.Models.DTO;

public sealed class SecurityRegistrationCredentials
{
    public required string Username { get; init; }
    public required string Email { get; init; }
    public required string Password { get; init; }
    public required bool IsEnabled { get; init; }
    public string? PhoneNumber { get; init; }
    public string[] RoleNames { get; init; } = Array.Empty<string>();
}