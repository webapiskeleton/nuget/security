﻿using WebApiSkeleton.Security.Core.Enums;

namespace WebApiSkeleton.Security.Core.Models.DTO;

public record PermissionDto(string Name, PermissionMode Mode = PermissionMode.Allow);