﻿namespace WebApiSkeleton.Security.Core.Models.DTO;

public record RoleDto(int? Id, string? Name);