﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApiSkeleton.Security.Core.Extensions;

public static class EntityTypeExtensions
{
    /// <summary>
    /// Get full table name attached to entity, including schema and table name
    /// </summary>
    public static string? GetSchemaQualifiedTableName(this IEntityType entityType, bool addSchema = true)
    {
        var tableName = entityType.GetTableName();
        if (tableName == null)
        {
            return null;
        }

        var schema = entityType.GetSchema();
        return (!addSchema || string.IsNullOrEmpty(schema) ? "" : $@"""{schema}""" + ".") + $@"""{tableName}""";
    }
}