using Microsoft.EntityFrameworkCore.Infrastructure;

namespace WebApiSkeleton.Security.Core.Extensions.DatabaseContextExtensions;

internal sealed class WebApiSkeletonTableNamingExtensionInfo : DbContextOptionsExtensionInfo
{
    public WebApiSkeletonTableNamingExtensionInfo(IDbContextOptionsExtension extension) : base(extension)
    {
    }

    public override int GetServiceProviderHashCode() => 0;

    public override bool ShouldUseSameServiceProvider(DbContextOptionsExtensionInfo other) => true;

    public override void PopulateDebugInfo(IDictionary<string, string> debugInfo)
    {
    }

    public override bool IsDatabaseProvider => false;
    public override string LogFragment => "WebApiSkeleton table naming extension";
}