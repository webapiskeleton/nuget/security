using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace WebApiSkeleton.Security.Core.Extensions.DatabaseContextExtensions;

public static class OptionsExtensions
{
    /// <summary>
    /// Add <see cref="WebApiSkeletonTableNamingExtension"/> to DbContext to use default schema for tables
    /// </summary>
    /// <param name="builder">Options builder</param>
    /// <param name="schema">Default schema to use for all identity tables</param>
    public static void UseDefaultSchema(this DbContextOptionsBuilder builder, string schema)
    {
        var extension = new WebApiSkeletonTableNamingExtension { DefaultSchema = schema };
        ((IDbContextOptionsBuilderInfrastructure)builder).AddOrUpdateExtension(extension);
    }
}