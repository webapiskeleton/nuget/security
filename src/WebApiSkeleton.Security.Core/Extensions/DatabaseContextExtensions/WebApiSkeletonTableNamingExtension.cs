using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.DependencyInjection;

namespace WebApiSkeleton.Security.Core.Extensions.DatabaseContextExtensions;

/// <summary>
/// DbContext extension that contains information on default table schema
/// </summary>
internal sealed class WebApiSkeletonTableNamingExtension : IDbContextOptionsExtension
{
    public string DefaultSchema { get; init; } = "identity";

    public void ApplyServices(IServiceCollection services)
    {
    }

    public void Validate(IDbContextOptions options)
    {
    }

    public DbContextOptionsExtensionInfo Info => new WebApiSkeletonTableNamingExtensionInfo(this);
}