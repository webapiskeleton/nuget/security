﻿using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using WebApiSkeleton.DistributeLockUtilities;
using WebApiSkeleton.Security.Core.DatabaseMapping;
using WebApiSkeleton.Security.Core.Encryption.Impl;
using WebApiSkeleton.Security.Core.HostedServices;
using WebApiSkeleton.Security.Core.Repository.Command;
using WebApiSkeleton.Security.Core.Repository.Command.Impl;
using WebApiSkeleton.Security.Core.Repository.Query;
using WebApiSkeleton.Security.Core.Repository.Query.Impl;
using WebApiSkeleton.Security.Core.Services;
using WebApiSkeleton.Security.Core.Services.Impl;
using WebApiSkeleton.Security.Core.Services.Output;
using WebApiSkeleton.Security.Core.Services.Output.Impl;
using WebApiSkeleton.Security.Core.Settings;
using WebApiSkeleton.Security.Core.UserIdentity;
using WebApiSkeleton.Security.Core.UserIdentity.Impl;
using WebApiSkeleton.Security.Core.Verification;

namespace WebApiSkeleton.Security.Core;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddSecurityCore(this IServiceCollection services,
        Action<SecurityConfiguration> securityConfigurationCallback)
    {
        DefaultTypeMap.MatchNamesWithUnderscores = true;
        var configuration = new SecurityConfiguration();
        securityConfigurationCallback.Invoke(configuration);
        configuration.ValidateConfiguration();

        var securitySettings = new SecuritySettings
        {
            UseJwtEncryption = configuration.UseJwtEncryption,
            IncludeUserClaimsInJwt = configuration.IncludeUserClaimsInJwt
        };

        var passwordEncryptionSettings = new PasswordEncryptionSettings
            { PasswordEncryptionKey = configuration.PasswordEncryptionKey! };

        services.TryAddRedisDistributedLockService();
        services.AddHostedService<DatabaseProviderConfigurator>();
        services.AddHostedService<DatabaseMigrationHostedService>();
        services.TryAddSingleton(configuration.RedisConnectionSettings!);
        services.TryAddSingleton(configuration.JwtSettings!);
        services.TryAddSingleton(securitySettings);
        services.TryAddSingleton(passwordEncryptionSettings);
        services.TryAddSingleton(configuration.ValidationSettings);
        services.TryAddSingleton(configuration.VerificationSettings);
        services.TryAddSingleton(configuration.DatabaseOptions!);
        return services
            .AddCommandRepositories(configuration.DatabaseOptions!)
            .AddQueryRepositories()
            .ConfigureAuthorization(configuration.JwtSettings!, configuration.UseJwtEncryption)
            .AddScoped<IUserIdentity>(sp =>
            {
                var provider = sp.GetService<IUserIdentityProvider>();
                return provider is null ? AnonymousUserIdentity.Identity : provider.GetCurrentUserIdentity();
            });
    }

    private static IServiceCollection ConfigureAuthorization(this IServiceCollection services,
        JwtSettings jwtSettings, bool useEncryption)
    {
        var userDefinedTokenValidationParameters = jwtSettings.TokenValidationParameters;
        userDefinedTokenValidationParameters.IssuerSigningKey =
            new SymmetricSecurityKey(Encoding.ASCII.GetBytes(jwtSettings.SigningKey));
        userDefinedTokenValidationParameters.ValidateIssuerSigningKey = true;
        userDefinedTokenValidationParameters.ValidateLifetime = true;
        userDefinedTokenValidationParameters.ClockSkew = TimeSpan.Zero;

        if (useEncryption)
        {
            userDefinedTokenValidationParameters.TokenDecryptionKey =
                new SymmetricSecurityKey(Encoding.ASCII.GetBytes(jwtSettings.EncryptionKey!));
        }

        services.TryAddScoped<IJwtService, JwtService>();
        services.TryAddScoped<ISecurityService, SecurityService>();
        services.TryAddScoped<IUserReadService, UserReadService>();
        services.TryAddScoped<IUserCommandService, UserCommandService>();
        services.TryAddScoped<IRoleService, RoleService>();
        services.TryAddScoped<IPermissionService, PermissionService>();
        services.TryAddScoped<IEmailVerifier, EmailVerifier>();
        services.TryAddSingleton<IEncryptionService, EncryptionService>();

        services.TryAddSingleton(userDefinedTokenValidationParameters);

        return services;
    }

    private static IServiceCollection AddCommandRepositories(this IServiceCollection services,
        IdentityDatabaseOptions opt)
    {
        opt.OptionsAction += options => { options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking); };
        services.AddDbContext<IdentityContext>(opt.OptionsAction);

        services.TryAddScoped<IUserCommandRepository, UserCommandRepository>();
        services.TryAddScoped<IRefreshCommandRepository, RefreshCommandRepository>();
        services.TryAddScoped<IRoleCommandRepository, RoleCommandRepository>();
        services.TryAddScoped<IPermissionCommandRepository, PermissionCommandRepository>();
        return services;
    }

    private static IServiceCollection AddQueryRepositories(this IServiceCollection services)
    {
        var databaseModels = Assembly
            .GetExecutingAssembly()
            .GetTypes()
            .Where(x => x.GetCustomAttribute<IdentityModelAttribute>() is not null);
        var assemblyName = new AssemblyName("DatabaseEntityMapping");
        var assemblyBuilder = AssemblyBuilder.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
        var moduleBuilder = assemblyBuilder.DefineDynamicModule("MappingModule");
        foreach (var databaseModel in databaseModels)
        {
            var typeName = $"{databaseModel.Name}DatabaseMapping";
            var baseType = typeof(BaseDatabaseMapping<>).MakeGenericType(databaseModel);
            var builder = moduleBuilder.DefineType(typeName, TypeAttributes.NotPublic | TypeAttributes.Class, baseType);
            var ctor = builder.DefineConstructor(MethodAttributes.Public, CallingConventions.Standard,
                [typeof(IdentityContext)]);
            var ctorIl = ctor.GetILGenerator();
            var baseTypeCtor = baseType.GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance,
                [typeof(IdentityContext)]);
            ctorIl.Emit(OpCodes.Ldarg_0);
            ctorIl.Emit(OpCodes.Ldarg_1);
            ctorIl.Emit(OpCodes.Call, baseTypeCtor!);
            ctorIl.Emit(OpCodes.Ret);
            var type = builder.CreateType();
            services.TryAddScoped(typeof(IDatabaseMapping<>).MakeGenericType(databaseModel), type);
        }

        services.TryAddScoped<SecurityNpgsqlConnectionProvider>();
        services.TryAddScoped<IIdentityUserQueryRepository, IdentityUserQueryRepository>();
        services.TryAddScoped<IRefreshTokenQueryRepository, RefreshTokenQueryRepository>();
        services.TryAddScoped<IRoleQueryRepository, RoleQueryRepository>();
        services.TryAddScoped<IPermissionQueryRepository, PermissionQueryRepository>();

        return services;
    }
}