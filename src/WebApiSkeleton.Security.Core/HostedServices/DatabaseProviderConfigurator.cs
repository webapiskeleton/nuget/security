using System.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using WebApiSkeleton.Security.Core.Settings;

namespace WebApiSkeleton.Security.Core.HostedServices;

/// <summary>
/// Hosted service that applies provider-specific database settings
/// </summary>
internal sealed class DatabaseProviderConfigurator : IHostedService
{
    private readonly IServiceProvider _serviceProvider;
    private readonly IdentityDatabaseOptions _options;
    private readonly ILogger<DatabaseProviderConfigurator> _logger;

    public DatabaseProviderConfigurator(IServiceProvider serviceProvider,
        ILogger<DatabaseProviderConfigurator> logger,
        IdentityDatabaseOptions options)
    {
        _serviceProvider = serviceProvider;
        _logger = logger;
        _options = options;
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        using var scope = _serviceProvider.CreateScope();
        var context = scope.ServiceProvider.GetRequiredService<IdentityContext>();
        switch (context.Database.ProviderName)
        {
            case ProviderNames.Sqlite:
                await context.Database.ExecuteSqlAsync($"pragma defer_foreign_keys = true", cancellationToken);
                SqlMapper.AddTypeHandler(new SqliteGuidFromStringMapper());
                SqlMapper.RemoveTypeMap(typeof(Guid));
                SqlMapper.RemoveTypeMap(typeof(Guid?));
                break;
        }
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }
}

file static class ProviderNames
{
    public const string Sqlite = "Microsoft.EntityFrameworkCore.Sqlite";
}

file class SqliteGuidFromStringMapper : SqlMapper.TypeHandler<Guid>
{
    public override void SetValue(IDbDataParameter parameter, Guid value)
    {
        parameter.Value = value;
    }

    public override Guid Parse(object value)
    {
        return Guid.Parse(value.ToString()!);
    }
}