﻿// Global using directives

global using System.ComponentModel.DataAnnotations;
global using System.Security.Claims;
global using Dapper;
global using Microsoft.IdentityModel.Tokens;
global using WebApiSkeleton.DatabaseMapping;
global using WebApiSkeleton.SearchUtilities;
global using WebApiSkeleton.Security.Core.Attributes;
global using WebApiSkeleton.Security.Core.Data;
global using WebApiSkeleton.Security.Core.Encryption;
global using WebApiSkeleton.Security.Core.Models;