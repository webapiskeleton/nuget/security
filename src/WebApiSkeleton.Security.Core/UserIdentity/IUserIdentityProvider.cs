﻿namespace WebApiSkeleton.Security.Core.UserIdentity;

public interface IUserIdentityProvider
{
    public IUserIdentity GetCurrentUserIdentity();
}