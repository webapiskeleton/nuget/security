﻿using System.Security.Principal;

namespace WebApiSkeleton.Security.Core.UserIdentity;

/// <summary>
/// User identity containing base information about them
/// </summary>
public interface IUserIdentity : IIdentity
{
    /// <summary>
    /// User ID used in this Security Service
    /// </summary>
    public int UserId { get; }
}