﻿namespace WebApiSkeleton.Security.Core.UserIdentity.Impl;

/// <summary>
/// Identity that is returned if user is not authenticated
/// </summary>
public sealed class AnonymousUserIdentity : IUserIdentity
{
    public static readonly AnonymousUserIdentity Identity = new();

    private AnonymousUserIdentity()
    {
    }
    
    public string AuthenticationType => "Unauthenticated";
    public bool IsAuthenticated => false;
    public string Name => "Anonymous";
    public int UserId => 0;
}