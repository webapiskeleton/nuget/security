﻿using System.Security.Principal;

namespace WebApiSkeleton.Security.Core.UserIdentity.Impl;

public sealed class UserIdentity : IUserIdentity
{
    public string? AuthenticationType { get; }
    public bool IsAuthenticated { get; }
    public string? Name { get; }
    public int UserId { get; }

    public UserIdentity(IIdentity identity, int userId)
    {
        AuthenticationType = identity.AuthenticationType;
        IsAuthenticated = identity.IsAuthenticated;
        Name = identity.Name;
        UserId = userId;
    }
}