using System.Diagnostics.Contracts;
using WebApiSkeleton.Security.Core.Models.Output;

namespace WebApiSkeleton.Security.Core.Services.Output;

public interface IUserReadService
{
    /// <summary>
    /// Get user by ID
    /// </summary>
    /// <param name="userId">User ID</param>
    /// <returns>User data if found, otherwise null</returns>
    [Pure]
    public Task<User?> GetUserAsync(int userId);

    /// <summary>
    /// Get user by login (username/email)
    /// </summary>
    /// <param name="login">Username or email</param>
    /// <returns>User data if found, otherwise null</returns>
    [Pure]
    public Task<User?> GetUserAsync(string login);
    
    /// <summary>
    /// List users using given arguments
    /// </summary>
    /// <param name="listParam">Search parameters</param>
    /// <returns>User data if found, otherwise null</returns>
    [Pure]
    public Task<ListResult<User>> ListUsersAsync(ListParam<User> listParam);

    /// <summary>
    /// Get user claims
    /// </summary>
    /// <param name="userId">User ID</param>
    /// <returns>Claims for a given user if found, otherwise empty collection</returns>
    [Pure]
    public Task<IEnumerable<Claim>> ListUserClaimsAsync(int userId);

    /// <summary>
    /// Checks if provided password is correct for given user
    /// </summary>
    /// <param name="userId">User ID</param>
    /// <param name="password">Password used to access user</param>
    /// <returns>True if password is correct, otherwise false</returns>
    [Pure]
    public Task<bool> IsUserPasswordCorrect(int userId, string password);

    /// <summary>
    /// Get roles that user belongs to
    /// </summary>
    /// <returns>Collection of roles that user belongs to</returns>
    [Pure]
    public Task<IEnumerable<Role>> GetUserRolesAsync(int userId);

    /// <summary>
    /// Check if user belongs to a given role
    /// </summary>
    /// <param name="userId">User ID</param>
    /// <param name="role">Role to check belonging to</param>
    /// <returns>True if user belongs to a role, otherwise false</returns>
    [Pure]
    public Task<bool> UserInRoleAsync(int userId, Role role);
}