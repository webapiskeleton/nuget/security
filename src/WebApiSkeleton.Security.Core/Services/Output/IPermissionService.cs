using System.Diagnostics.Contracts;
using WebApiSkeleton.Security.Core.Models.DTO;
using WebApiSkeleton.Security.Core.Models.Output;

namespace WebApiSkeleton.Security.Core.Services.Output;

public interface IPermissionService
{
    /// <summary>
    /// Create permission with given name
    /// </summary>
    /// <param name="name">Permission name</param>
    /// <param name="cancellationToken">Cancellation token</param>
    /// <returns>ID of created permission</returns>
    public Task CreatePermissionAsync(string name, CancellationToken cancellationToken = default);

    /// <summary>
    /// List all permission types stored based on param
    /// </summary>
    /// <param name="listParam">ListParam with filters, sort orders and pagination</param>
    /// <returns>ListResult with permission types based on <see cref="listParam"/></returns>
    [Pure]
    public Task<ListResult<Permission>> ListPermissionsAsync(ListParam<Permission> listParam);

    /// <summary>
    /// List permissions of a given user
    /// </summary>
    /// <param name="userId">User ID that has permissions</param>
    /// <param name="includeRolePermissions">Include user's role permissions in result</param>
    /// <returns>List of permissions granted to a given user</returns>
    [Pure]
    public Task<IEnumerable<GrantedPermission>> ListUserPermissionsAsync(int userId, bool includeRolePermissions = false);

    /// <summary>
    /// List permissions of a given role
    /// </summary>
    /// <param name="roleId">Role ID that has permissions</param>
    /// <returns>List of permission granted to a given role</returns>
    [Pure]
    public Task<IEnumerable<GrantedPermission>> ListRolePermissionsAsync(int roleId);

    /// <summary>
    /// List custom permissions based on a listParam given
    /// </summary>
    /// <param name="listParam">ListParam with filters, sort orders and pagination</param>
    /// <returns>List of custom permissions granted on the filtered entity</returns>
    [Pure]
    public Task<IEnumerable<CustomGrantedPermission>> ListCustomPermissionsAsync(ListParam<CustomGrantedPermission> listParam);

    /// <summary>
    /// Add permissions to some entity
    /// </summary>
    /// <param name="entity">Entity object that extends IGrantable</param>
    /// <param name="permissions">Permissions to add</param>
    /// <param name="cancellationToken">Cancellation token</param>
    /// <typeparam name="T">Entity type to add permissions for that implements <see cref="IGrantable"/></typeparam>
    public Task AddPermissionAsync<T>(T entity, IEnumerable<PermissionDto> permissions,
        CancellationToken cancellationToken = default) where T : IGrantable;

    /// <summary>
    /// Remove permissions from some entity
    /// </summary>
    /// <param name="entity">Entity object that extends IGrantable</param>
    /// <param name="permissions">Permissions to remove</param>
    /// <param name="cancellationToken">Cancellation token</param>
    /// <typeparam name="T">Entity type to remove permissions from that implements <see cref="IGrantable"/></typeparam>
    public Task RemovePermissionAsync<T>(T entity, IEnumerable<PermissionDto> permissions,
        CancellationToken cancellationToken = default) where T : IGrantable;
}