using WebApiSkeleton.Security.Core.Models.Output;

namespace WebApiSkeleton.Security.Core.Services.Output;

public interface IUserCommandService
{
    /// <summary>
    /// Create a user in database
    /// </summary>
    /// <param name="user">Model that contains all data about user</param>
    /// <param name="cancellationToken">Cancellation token</param>
    /// <returns>Created user if successful, otherwise null</returns>
    public Task<User> CreateUserAsync(User user, CancellationToken cancellationToken = default);

    /// <summary>
    /// Update a user in database
    /// </summary>
    /// <param name="user">User with updated properties</param>
    /// <param name="cancellationToken">Cancellation token</param>
    /// <returns>Result response</returns>
    public Task<User> UpdateUserAsync(User user, CancellationToken cancellationToken = default);

    /// <summary>
    /// Add claims to user
    /// </summary>
    /// <param name="userId">User ID</param>
    /// <param name="claims">Claims to add</param>
    /// <param name="cancellationToken">Cancellation token</param>
    public Task AddClaimsAsync(int userId, IEnumerable<Claim> claims, CancellationToken cancellationToken = default);

    /// <summary>
    /// Remove claims from user
    /// </summary>
    /// <param name="userId">User ID</param>
    /// <param name="claimKeys">Claim keys to remove</param>
    /// <param name="cancellationToken">Cancellation token</param>
    public Task RemoveClaimsAsync(int userId, IEnumerable<string> claimKeys,
        CancellationToken cancellationToken = default);

    /// <summary>
    /// Set a password for user
    /// </summary>
    /// <param name="password">Password to set</param>
    /// <param name="cancellationToken">Cancellation token</param>
    public Task SetUserPasswordAsync(Password password, CancellationToken cancellationToken = default);

    /// <summary>
    /// Disable the user to prevent him from logging
    /// </summary>
    /// <param name="userId">User ID</param>
    /// <param name="cancellationToken">Cancellation token</param>
    /// <returns></returns>
    public Task DisableUserLoginAsync(int userId, CancellationToken cancellationToken = default);

    /// <summary>
    /// Add user to role provided
    /// </summary>
    /// <param name="userId">User ID</param>
    /// <param name="role">Role to add user to</param>
    /// <param name="cancellationToken">Cancellation token</param>
    public Task AddUserToRoleAsync(int userId, Role role, CancellationToken cancellationToken = default);

    /// <summary>
    /// Remove user from role provided
    /// </summary>
    /// <param name="userId">User ID</param>
    /// <param name="role">Role to remove user from</param>
    /// <param name="cancellationToken">Cancellation token</param>
    public Task RemoveUserFromRoleAsync(int userId, Role role, CancellationToken cancellationToken = default);
}