using System.Diagnostics.Contracts;
using WebApiSkeleton.Security.Core.Models.Output;

namespace WebApiSkeleton.Security.Core.Services.Output;

public interface IRoleService
{
    /// <summary>
    /// Get the list of existing roles with pagination
    /// </summary>
    /// <param name="pagination">Pagination info</param>
    /// <returns>ListResult of roles based on pagination info</returns>
    [Pure]
    public Task<ListResult<Role>> ListRolesAsync(PaginationInfo pagination);
    
    /// <summary>
    /// Get role information
    /// </summary>
    /// <param name="name">Role name</param>
    /// <returns>Role if found, otherwise null</returns>
    [Pure]
    public Task<Role?> GetRoleAsync(string name);

    /// <summary>
    /// Get role information
    /// </summary>
    /// <param name="id">Role id</param>
    /// <returns>Role if found, otherwise null</returns>
    [Pure]
    public Task<Role?> GetRoleAsync(int id);

    /// <summary>
    /// Get users that belong to the given role
    /// </summary>
    /// <param name="roleId">Role ID</param>
    /// <param name="pagination">Pagination parameters</param>
    /// <returns>Users that belong to the given role</returns>
    [Pure]
    public Task<ListResult<User>> GetRoleUsersAsync(int roleId, PaginationInfo pagination);

    /// <summary>
    /// Create role with given name
    /// </summary>
    /// <param name="name">Role name</param>
    /// <param name="cancellationToken">Cancellation token</param>
    /// <returns>ID of created role</returns>
    public Task<Role> CreateRoleAsync(string name, CancellationToken cancellationToken = default);

    /// <summary>
    /// Update the name of role
    /// </summary>
    /// <param name="oldName">Current role name to find</param>
    /// <param name="newName">New name to set to role</param>
    /// <param name="cancellationToken">Cancellation token</param>
    /// <returns>Result response</returns>
    public Task<Role> UpdateRoleNameAsync(string oldName, string newName,
        CancellationToken cancellationToken = default);    
    
    /// <summary>
    /// Update the name of role
    /// </summary>
    /// <param name="roleId">Role ID</param>
    /// <param name="newName">New name to set to role</param>
    /// <param name="cancellationToken">Cancellation token</param>
    /// <returns>Result response</returns>
    public Task<Role> UpdateRoleNameAsync(int roleId, string newName,
        CancellationToken cancellationToken = default);
}