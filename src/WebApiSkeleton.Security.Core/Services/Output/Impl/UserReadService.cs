﻿using System.Diagnostics.Contracts;
using Microsoft.Extensions.DependencyInjection;
using WebApiSkeleton.Security.Core.Map;
using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Repository.Query;

namespace WebApiSkeleton.Security.Core.Services.Output.Impl;

public sealed class UserReadService : IUserReadService
{
    private readonly IEncryptionService _encryptionService;
    private readonly IIdentityUserQueryRepository _queryRepository;
    private readonly SecurityMapper _mapper;

    public UserReadService(IServiceProvider serviceProvider,
        IEncryptionService encryptionService)
    {
        _encryptionService = encryptionService;
        _queryRepository = serviceProvider.GetRequiredService<IIdentityUserQueryRepository>();
        _mapper = new SecurityMapper();
    }

    /// <inheritdoc/>
    public async Task<User?> GetUserAsync(int userId)
    {
        return await _queryRepository.GetUserByIdAsync(userId);
    }

    /// <inheritdoc/>
    public async Task<User?> GetUserAsync(string login)
    {
        return await _queryRepository.GetUserByLoginAsync(login);
    }

    /// <inheritdoc/>
    public async Task<ListResult<User>> ListUsersAsync(ListParam<User> listParam)
    {
        return await _queryRepository.ListUsersAsync(listParam);
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<Claim>> ListUserClaimsAsync(int userId)
    {
        return await _queryRepository.GetClaimsForUserAsync(userId);
    }

    /// <inheritdoc/>
    public async Task<bool> IsUserPasswordCorrect(int userId, string password)
    {
        var passwordHash = await _encryptionService.EncryptPasswordAsync(password);
        return await _queryRepository.IsUserPasswordCorrectAsync(userId, passwordHash);
    }

    /// <inheritdoc/>
    [Pure]
    public async Task<IEnumerable<Role>> GetUserRolesAsync(int userId)
    {
        return await _queryRepository.GetRolesForUserAsync(userId);
    }

    /// <inheritdoc/>
    public async Task<bool> UserInRoleAsync(int userId, Role role)
    {
        return await _queryRepository.UserInRoleAsync(userId, role.Id);
    }
}