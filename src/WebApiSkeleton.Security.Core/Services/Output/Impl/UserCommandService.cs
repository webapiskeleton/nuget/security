﻿using Microsoft.Extensions.DependencyInjection;
using WebApiSkeleton.Security.Core.Map;
using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Repository.Command;
using WebApiSkeleton.Security.Core.UserIdentity;

namespace WebApiSkeleton.Security.Core.Services.Output.Impl;

public sealed class UserCommandService : IUserCommandService
{
    private readonly IUserCommandRepository _repository;
    private readonly SecurityMapper _mapper;
    private readonly IEncryptionService _encryptionService;
    private readonly IUserReadService _readService;
    private readonly IUserIdentity _userIdentity;

    public UserCommandService(IServiceProvider serviceProvider,
        IEncryptionService encryptionService,
        IUserIdentity userIdentity,
        IUserReadService readService)
    {
        _encryptionService = encryptionService;
        _userIdentity = userIdentity;
        _readService = readService;
        _mapper = new SecurityMapper();
        _repository = serviceProvider.GetRequiredService<IUserCommandRepository>();
    }

    /// <inheritdoc/>
    public async Task<User> CreateUserAsync(User user, CancellationToken cancellationToken = default)
    {
        return await _repository.CreateUserAsync(user, cancellationToken);
    }

    /// <inheritdoc/>
    public async Task<User> UpdateUserAsync(User user, CancellationToken cancellationToken = default)
    {
        return await _repository.UpdateUserAsync(user, cancellationToken);
    }

    /// <inheritdoc/>
    public async Task AddClaimsAsync(int userId, IEnumerable<Claim> claims,
        CancellationToken cancellationToken = default)
    {
        await _repository.AddUserClaimAsync(userId, claims, cancellationToken);
    }

    /// <inheritdoc/>
    public async Task RemoveClaimsAsync(int userId, IEnumerable<string> claimKeys,
        CancellationToken cancellationToken = default)
    {
        await _repository.RemoveClaimAsync(userId, claimKeys, cancellationToken);
    }

    /// <inheritdoc/>
    public async Task SetUserPasswordAsync(Password password, CancellationToken cancellationToken = default)
    {
        var passwordHash = await _encryptionService.EncryptPasswordAsync(password.Value);
        await _repository.SetUserPasswordAsync(password.UserId, passwordHash,
            _userIdentity.UserId == 0 ? null : _userIdentity.UserId, cancellationToken);
    }

    /// <inheritdoc/>
    public async Task DisableUserLoginAsync(int userId, CancellationToken cancellationToken = default)
    {
        await _repository.DisableUserLoginAsync(userId, cancellationToken);
    }

    /// <inheritdoc/>
    public async Task AddUserToRoleAsync(int userId, Role role, CancellationToken cancellationToken = default)
    {
        await _repository.AddUserToRoleAsync(userId, role.Id, cancellationToken);
    }

    /// <inheritdoc/>
    public async Task RemoveUserFromRoleAsync(int userId, Role role, CancellationToken cancellationToken = default)
    {
        await _repository.RemoveUserFromRoleAsync(userId, role.Id, cancellationToken);
    }
}