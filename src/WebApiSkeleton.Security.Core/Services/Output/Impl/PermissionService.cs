using Microsoft.Extensions.DependencyInjection;
using WebApiSkeleton.Security.Core.Models.DTO;
using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Repository.Command;
using WebApiSkeleton.Security.Core.Repository.Query;

namespace WebApiSkeleton.Security.Core.Services.Output.Impl;

public sealed class PermissionService : IPermissionService
{
    private readonly IPermissionCommandRepository _commandRepository;
    private readonly IPermissionQueryRepository _queryRepository;

    public PermissionService(IServiceProvider serviceProvider)
    {
        _commandRepository = serviceProvider.GetRequiredService<IPermissionCommandRepository>();
        _queryRepository = serviceProvider.GetRequiredService<IPermissionQueryRepository>();
    }

    /// <inheritdoc/>
    public async Task CreatePermissionAsync(string name,
        CancellationToken cancellationToken = default)
    {
        await _commandRepository.CreatePermissionAsync(name, cancellationToken);
    }

    /// <inheritdoc/>
    public async Task<ListResult<Permission>> ListPermissionsAsync(ListParam<Permission> listParam)
    {
        return await _queryRepository.ListPermissionsAsync(listParam);
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<GrantedPermission>> ListUserPermissionsAsync(int userId,
        bool includeRolePermissions = false)
    {
        return await _queryRepository.ListUserPermissions(userId, includeRolePermissions);
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<GrantedPermission>> ListRolePermissionsAsync(int roleId)
    {
        return await _queryRepository.ListRolePermissions(roleId);
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<CustomGrantedPermission>> ListCustomPermissionsAsync(
        ListParam<CustomGrantedPermission> listParam)
    {
        return await _queryRepository.ListCustomPermissionsAsync(listParam);
    }

    /// <inheritdoc/>
    public async Task AddPermissionAsync<T>(T entity, IEnumerable<PermissionDto> permissions,
        CancellationToken cancellationToken = default) where T : IGrantable
    {
        await _commandRepository.AddPermissionAsync(entity, permissions, cancellationToken);
    }

    /// <inheritdoc/>
    public async Task RemovePermissionAsync<T>(T entity, IEnumerable<PermissionDto> permissions,
        CancellationToken cancellationToken = default) where T : IGrantable
    {
        await _commandRepository.RemovePermissionAsync(entity, permissions, cancellationToken);
    }
}