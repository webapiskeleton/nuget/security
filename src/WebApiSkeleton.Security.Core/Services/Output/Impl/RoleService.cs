﻿using System.Diagnostics.Contracts;
using Microsoft.Extensions.DependencyInjection;
using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Repository.Command;
using WebApiSkeleton.Security.Core.Repository.Query;

namespace WebApiSkeleton.Security.Core.Services.Output.Impl;

public sealed class RoleService : IRoleService
{
    private readonly IRoleCommandRepository _commandRepository;
    private readonly IRoleQueryRepository _queryRepository;

    public RoleService(IServiceProvider serviceProvider)
    {
        _commandRepository = serviceProvider.GetRequiredService<IRoleCommandRepository>();
        _queryRepository = serviceProvider.GetRequiredService<IRoleQueryRepository>();
    }

    /// <inheritdoc/>
    public async Task<ListResult<Role>> ListRolesAsync(PaginationInfo pagination)
    {
        return await _queryRepository.ListRolesAsync(pagination);
    }

    /// <inheritdoc/>
    [Pure]
    public async Task<Role?> GetRoleAsync(string name)
    {
        return await _queryRepository.GetRoleAsync(name);
    }

    /// <inheritdoc/>
    [Pure]
    public async Task<Role?> GetRoleAsync(int id)
    {
        return await _queryRepository.GetRoleAsync(id);
    }

    /// <inheritdoc/>
    [Pure]
    public async Task<ListResult<User>> GetRoleUsersAsync(int roleId, PaginationInfo pagination)
    {
        return await _queryRepository.GetRoleUsersAsync(roleId, pagination);
    }

    /// <inheritdoc/>
    public async Task<Role> CreateRoleAsync(string name, CancellationToken cancellationToken = default)
    {
        var roleId = await _commandRepository.CreateRoleAsync(name, cancellationToken);
        return new Role { Id = roleId, Name = name };
    }

    /// <inheritdoc/>
    public async Task<Role> UpdateRoleNameAsync(string oldName, string newName,
        CancellationToken cancellationToken = default)
    {
        var existingRole = await GetRoleAsync(oldName);
        await _commandRepository.UpdateRoleNameAsync(existingRole!.Id, newName, cancellationToken);
        return new Role { Id = existingRole.Id, Name = newName };
    }

    /// <inheritdoc/>
    public async Task<Role> UpdateRoleNameAsync(int roleId, string newName,
        CancellationToken cancellationToken = default)
    {
        await _commandRepository.UpdateRoleNameAsync(roleId, newName, cancellationToken);
        return new Role { Id = roleId, Name = newName };
    }
}