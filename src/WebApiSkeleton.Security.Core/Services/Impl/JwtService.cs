﻿using System.IdentityModel.Tokens.Jwt;
using System.Text;
using WebApiSkeleton.Security.Core.Map;
using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Repository.Command;
using WebApiSkeleton.Security.Core.Services.Output;
using WebApiSkeleton.Security.Core.Settings;
using RefreshToken = WebApiSkeleton.Security.Core.Models.Output.RefreshToken;

namespace WebApiSkeleton.Security.Core.Services.Impl;

internal sealed class JwtService : IJwtService
{
    private readonly IUserReadService _userReadService;
    private readonly JwtSettings _jwtSettings;
    private readonly IRefreshCommandRepository _refreshCommandRepository;
    private readonly SecurityMapper _mapper;
    private readonly SecuritySettings _settings;

    public JwtService(JwtSettings jwtSettings,
        IRefreshCommandRepository refreshCommandRepository, 
        SecuritySettings settings, 
        IUserReadService userReadService)
    {
        _jwtSettings = jwtSettings;
        _refreshCommandRepository = refreshCommandRepository;
        _settings = settings;
        _userReadService = userReadService;
        _mapper = new SecurityMapper();
    }

    /// <inheritdoc/>
    public async Task<Token> GenerateTokenForUserAsync(int userId, string deviceId, CancellationToken cancellationToken = default)
    {
        var user = await _userReadService.GetUserAsync(userId);
        return await GenerateToken(user!, deviceId, cancellationToken);
    }

    private async Task<Token> GenerateToken(User user, string deviceId, CancellationToken cancellationToken = default)
    {
        var tokenHandler = new JwtSecurityTokenHandler();
        var signingKey = Encoding.ASCII.GetBytes(_jwtSettings.SigningKey);

        var claims = new List<Claim>
        {
            new(JwtRegisteredClaimNames.Sub, user.Email),
            new(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            new(JwtRegisteredClaimNames.Name, user.Email),
            new(JwtRegisteredClaimNames.Email, user.Email),
            new("id", user.Id.ToString()),
        };

        if (_settings.IncludeUserClaimsInJwt)
        {
            var userClaims = await _userReadService.ListUserClaimsAsync(user.Id);
            claims.AddRange(userClaims);
        }

        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Audience = _jwtSettings.Audience ?? default,
            Issuer = _jwtSettings.Issuer ?? default,
            IssuedAt = DateTime.UtcNow,
            Subject = new ClaimsIdentity(claims),
            Expires = DateTime.UtcNow.Add(_jwtSettings.AccessTokenLifetime),
            SigningCredentials =
                new SigningCredentials(new SymmetricSecurityKey(signingKey), SecurityAlgorithms.HmacSha256Signature),
        };

        if (_settings.UseJwtEncryption)
        {
            var encryptingKey = Encoding.ASCII.GetBytes(_jwtSettings.EncryptionKey!);
            tokenDescriptor.EncryptingCredentials = new EncryptingCredentials(new SymmetricSecurityKey(encryptingKey),
                JwtConstants.DirectKeyUseAlg, SecurityAlgorithms.Aes256CbcHmacSha512);
        }

        var accessToken = tokenHandler.CreateToken(tokenDescriptor);
        var refreshToken = new RefreshToken
        {
            JwtId = accessToken.Id,
            UserId = user.Id,
            DeviceId = deviceId,
            CreationDate = DateTime.UtcNow,
            ExpiryDate = DateTime.UtcNow.AddMonths(_jwtSettings.RefreshTokenMonthLifetime)
        };

        await _refreshCommandRepository.CreateTokenAsync(_mapper.Map(refreshToken), cancellationToken);

        return new Token
        {
            AccessToken = tokenHandler.WriteToken(accessToken),
            RefreshToken = refreshToken.Token,
        };
    }
}