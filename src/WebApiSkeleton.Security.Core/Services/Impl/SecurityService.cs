﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using WebApiSkeleton.DistributeLockUtilities.Services;
using WebApiSkeleton.Security.Core.Map;
using WebApiSkeleton.Security.Core.Models.DTO;
using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.Models.Output.Response;
using WebApiSkeleton.Security.Core.Repository.Command;
using WebApiSkeleton.Security.Core.Repository.Query;
using WebApiSkeleton.Security.Core.Services.Output;

namespace WebApiSkeleton.Security.Core.Services.Impl;

public sealed class SecurityService : ISecurityService
{
    private readonly IUserReadService _userReadService;
    private readonly IUserCommandService _userCommandService;
    private readonly IRoleService _roleService;
    private readonly IRefreshCommandRepository _refreshCommandRepository;
    private readonly IRefreshTokenQueryRepository _refreshTokenQueryRepository;
    private readonly TokenValidationParameters _tokenValidationParameters;
    private readonly IJwtService _jwtService;
    private readonly SecurityMapper _mapper;
    private readonly ILogger<SecurityService> _logger;
    private readonly IDistributedLockService _distributedLockService;

    public SecurityService(IServiceProvider serviceProvider,
        IUserReadService userReadService,
        ILogger<SecurityService> logger,
        TokenValidationParameters tokenValidationParameters,
        IUserCommandService userCommandService,
        IDistributedLockService distributedLockService,
        IRoleService roleService)
    {
        _jwtService = serviceProvider.GetRequiredService<IJwtService>();
        _userReadService = userReadService;
        _refreshCommandRepository = serviceProvider.GetRequiredService<IRefreshCommandRepository>();
        _refreshTokenQueryRepository = serviceProvider.GetRequiredService<IRefreshTokenQueryRepository>();
        _logger = logger;
        _tokenValidationParameters = tokenValidationParameters;
        _userCommandService = userCommandService;
        _distributedLockService = distributedLockService;
        _roleService = roleService;
        _mapper = new SecurityMapper();
    }

    /// <inheritdoc/>
    public async Task<AuthorizationResponse> RegisterAsync(SecurityRegistrationCredentials credentials, string deviceId,
        CancellationToken cancellationToken = default)
    {
        var userToCreate = _mapper.CredentialsToUser(credentials);
        var createdUser = await _userCommandService.CreateUserAsync(userToCreate, cancellationToken);
        foreach (var roleName in credentials.RoleNames)
        {
            var role = await _roleService.GetRoleAsync(roleName);
            if (role is null)
                continue;
            await _userCommandService.AddUserToRoleAsync(createdUser.Id, role, cancellationToken);
        }

        await _userCommandService.SetUserPasswordAsync(
            new Password { UserId = createdUser.Id, Value = credentials.Password },
            cancellationToken);

        if (!createdUser.IsEnabled)
            return AuthorizationResponse.UserDisabled();

        var token = await _jwtService.GenerateTokenForUserAsync(createdUser.Id, deviceId, cancellationToken);
        return AuthorizationResponse.Success(token);
    }

    /// <inheritdoc/>
    public async Task<AuthorizationResponse> AuthorizeAsync(string login, string password, string deviceId,
        CancellationToken cancellationToken = default)
    {
        var user = await _userReadService.GetUserAsync(login);

        if (user is null)
            return AuthorizationResponse.WrongCredentials();

        if (!user.IsEnabled)
            return AuthorizationResponse.UserDisabled();

        if (!await _userReadService.IsUserPasswordCorrect(user.Id, password))
            return AuthorizationResponse.WrongCredentials();

        var lockKey = $"refresh-token-{user.Id}";
        await using var lockHandle = await _distributedLockService.AcquireLockAsync(lockKey);
        if (lockHandle is null)
            return AuthorizationResponse.TooManyAttempts();

        var revokedTokens =
            await _refreshCommandRepository.RevokeTokenIfExistsAsync(user.Id, deviceId, cancellationToken);
        if (revokedTokens > 0)
        {
            _logger.LogWarning("Revoked {Count} token(s) for user {UserId} with device id {DeviceId}", revokedTokens,
                user.Id, deviceId);
        }

        var token = await _jwtService.GenerateTokenForUserAsync(user.Id, deviceId, cancellationToken);
        return AuthorizationResponse.Success(token);
    }

    /// <inheritdoc/>
    public async Task<AuthorizationResponse> RefreshAsync(string accessToken, string refreshToken, string deviceId,
        CancellationToken cancellationToken = default)
    {
        var validatedToken = JwtUtils.GetPrincipalFromToken(accessToken, _tokenValidationParameters)!;
        var userId = int.Parse(validatedToken.Claims.Single(x => x.Type == "id").Value);

        if (!(await _userReadService.GetUserAsync(userId))!.IsEnabled)
        {
            return AuthorizationResponse.UserDisabled();
        }

        var lockKey = $"refresh-token-{userId}";
        await using var lockHandle = await _distributedLockService.AcquireLockAsync(lockKey);
        if (lockHandle is null)
            return AuthorizationResponse.TooManyAttempts();

        var storedRefreshToken = (await _refreshTokenQueryRepository.GetTokenAsync(refreshToken))!;
        if (storedRefreshToken.IsRevoked)
            return AuthorizationResponse.RefreshIsNotAvailable();

        if (storedRefreshToken.IsUsed)
        {
            var revokedTokens =
                await _refreshCommandRepository.RevokeTokenIfExistsAsync(userId, deviceId, cancellationToken);
            _logger.LogWarning("User {UserId} tried to refresh used token {Token}. Revoked {Count} tokens",
                userId, storedRefreshToken.Token, revokedTokens);
            return AuthorizationResponse.RefreshIsNotAvailable();
        }

        await _refreshCommandRepository.UseTokenAsync(refreshToken, cancellationToken);
        var token = await _jwtService.GenerateTokenForUserAsync(userId, deviceId, cancellationToken);
        return AuthorizationResponse.Success(token);
    }
}