﻿using WebApiSkeleton.Security.Core.Models.DTO;
using WebApiSkeleton.Security.Core.Models.Output.Response;

namespace WebApiSkeleton.Security.Core.Services;

public interface ISecurityService
{
    /// <summary>
    /// Register user based on information from <see cref="credentials"/> and generate token pair associated with <see cref="deviceId"/>
    /// </summary>
    /// <param name="credentials">User information</param>
    /// <param name="deviceId">Device ID to generate tokens for</param>
    /// <param name="cancellationToken">Cancellation token</param>
    public Task<AuthorizationResponse> RegisterAsync(SecurityRegistrationCredentials credentials, string deviceId,
        CancellationToken cancellationToken = default);

    /// <summary>
    /// Authorize user by returning Access and Refresh token pair
    /// </summary>
    /// <param name="login">User login (email or username)</param>
    /// <param name="password">User password</param>
    /// <param name="deviceId">Device ID to generate tokens for</param>
    /// <param name="cancellationToken">Cancellation token</param>
    public Task<AuthorizationResponse> AuthorizeAsync(string login, string password, string deviceId,
        CancellationToken cancellationToken = default);

    /// <summary>
    /// Refresh token pair by returning the new pair
    /// </summary>
    /// <param name="accessToken">Previous access token</param>
    /// <param name="refreshToken">Previous refresh token</param>
    /// <param name="deviceId">Device ID to generate tokens for</param>
    /// <param name="cancellationToken">Cancellation token</param>
    public Task<AuthorizationResponse> RefreshAsync(string accessToken, string refreshToken, string deviceId,
        CancellationToken cancellationToken = default);
}