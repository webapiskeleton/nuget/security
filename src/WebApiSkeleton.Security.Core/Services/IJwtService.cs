﻿using WebApiSkeleton.Security.Core.Models.Output;

namespace WebApiSkeleton.Security.Core.Services;

internal interface IJwtService
{
    /// <summary>
    /// Generates token based on user's information stored
    /// </summary>
    /// <param name="userId">User ID</param>
    /// <param name="deviceId">Device ID to generate token for</param>
    /// <param name="cancellationToken">Cancellation token</param>
    /// <returns>Generated access and refresh token pair</returns>
    public Task<Token> GenerateTokenForUserAsync(int userId, string deviceId,
        CancellationToken cancellationToken = default);
}