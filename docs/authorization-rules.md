# Authorization rules

Authorization rule is a custom logic executed in `PermissionBehavior` before request execution. It can contain
additional data checks and/or manipulations with authorization requirements.

## Definition

```csharp
public class SelfPasswordChangeAuthorizarionRule : IAuthorizationRule<SetUserPasswordCommand>
{
    private readonly IUserIdentity _userIdentity;
    private readonly IContractDataGetter _contractDataGetter;

    public SelfPasswordChangeAuthorizarionRule(IUserIdentity userIdentity,
        IContractDataGetter contractDataGetter)
    {
        _userIdentity = userIdentity;
        _contractDataGetter = contractDataGetter;
    }

    public string Name => "User allow self password change";

    public async Task<AuthorizationRuleApplyResult> HandleRule(SetUserPasswordCommand request,
        HashSet<string> requiredRoles,
        HashSet<string> requiredPermissions)
    {
        var user = await _contractDataGetter.GetUserAsync(request.UserLoginOrId);

        if (user is null)
            return new AuthorizationRuleApplyResult
            {
                AllowCommand = false, Message = "No user found to change password"
            };

        if (_userIdentity.UserId != user.Id)
        {
            // rule does not apply when password is changing for non-self user
            return new AuthorizationRuleApplyResult
            {
                AllowCommand = true, Message = "Password change requested for another user"
            };
        }

        // otherwise clear permission that requires set password command to execute
        requiredPermissions.Remove("SetUserPassword");
        return new AuthorizationRuleApplyResult { AllowCommand = true };
    }
}
```

In example above the rule checks whether user executed `SetUserPasswordCommand` on self or not. If command's user ID is
same as the current user's then `SetUserPassword` permission requirement is removed. `AllowCommand = true` means that
the rule does not forbid command execution but all other permission and role checks will be executed after rule
completion.

As an example, if user is not found, command returns `AllowCommand = false` which means that the rule does not allow
command to be executed in any other cases. Message to results are optional but recommended to describe the authorization
state that is returned as a `Result<T>` from `PermissionBehavior`.

Authorization rules share request scope and can use `IContractDataGetter` interface to receive request-scoped cached
data.

## Registration

Authorization rules can be added to DI by using `AddPermissionAuthorizationRuleToRequest` extension method
on `IServiceCollection`. Rules are registered as `Transient` by default and can have multiple implementations for same
request type.

```csharp
builder.Services.AddPermissionAuthorizationRuleToRequest<SetUserPasswordCommand, SelfPasswordChangeAuthorizarionRule>();
```

NOTE: just like with pipeline behaviors, the order of rule registration defines in which order they will be executed.