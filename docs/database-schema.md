# Configure and migrate database schema

## Supported database prodiders

Since query requests are using `WebApiSkeleton.SeachSqlGeneration` package for performance reasons, not all database
providers are supported. It is highly recommended to use one of the following database providers:

* PostgreSQL
* Sqlite

Other providers may be supported by creating a custom `ISearchSqlConverter` implementation for the syntax of required
provider, but it also may need changes in SQL generation
in [`SecurityQueryRepositoryBase`](../src/WebApiSkeleton.Security.Core/Repository/Base/SecurityQueryRepositoryBase.cs)
or some implementations of query repositories.

NOTE:

1. SQL Server was planned to be supported but was removed because of syntax modifications on pagination (`TOP` in the
   start of query instead of `LIMIT` at the end) and no support for deferred foreign key checks (required for
   cross-request
   transaction support).
2. Startup with Sqlite provider will always execute `pragma defer_foreign_keys = true` query for the correct work of
   cross-request transactions.

## Configuration

This package uses Entity Framework and supports multiple database providers to be used. Each (except
for `QueryTrackingBehavior`) can be configured the required way as all of the configurations are free to set.

`DbContextOptionsBuilder` callback function that is used in `AddDbContext` method is defined
in [`IdentityDatabaseOptions`](../src/WebApiSkeleton.Security.Core/Settings/IdentityDatabaseOptions.cs).

Example configuration for PostgreSQL:

``` csharp
builder.Services.AddSecurityCore(config => 
    {
        config.DatabaseOptions = new IdentityDatabaseOptions
        {
            OptionsAction = options => options.UseNpgsql("SomeConnectionString"),
                    npgsqlOptions => npgsqlOptions
                        .UseQuerySplittingBehavior(QuerySplittingBehavior.SingleQuery)
                        .MigrationsHistoryTable("identity_migrations", "identity")
                        .MigrationsAssembly(typeof(PostgresMigrations).Assembly.FullName))
                .UseSnakeCaseNamingConvention() // from EFCore.NamingConventions package
                .UseDefaultSchema("identity"),
            UseRequestTransactions = true,
        };
    });
builder.Services.TryAddScoped<ISearchSqlConverter, PostgresSearchSqlConverter>();
```

It is possible to define default schema for all tables by using `UseDefaultSchema` extension method defined
on `DbContextOptionsBuilder`. Calling the method adds
internal [`WebApiSkeletonTableNamingExtension`](../src/WebApiSkeleton.Security.Core/Extensions/DatabaseContextExtensions/WebApiSkeletonTableNamingExtension.cs)
to DbContext.

NOTE: As Sqlite has no schemas, this method call will not affect database schema.

It is required to also add the scoped implementation of `ISearchSqlConverter` to DI, otherwise database querying will
not work.
Default implementations
for [PostgreSQL](https://gitlab.com/webapiskeleton/nuget/search/-/tree/master/src/WebApiSkeleton.SearchSqlGeneration.Postgres?ref_type=heads)
and [Sqlite](https://gitlab.com/webapiskeleton/nuget/search/-/tree/master/src/WebApiSkeleton.SearchSqlGeneration.Sqlite?ref_type=heads)
can be used from `WebApiSkeleton.SearchSqlGeneration.xxx` NuGet packages.

## Migration

Database schema can be created by using EF core migrations or manually.

### Migrate database schema (preferred)

This migration method is preferred as it is used in testing of package and allows the developer to migrate database
schema the time package updates it.

Migration creation is done by using `dotnet ef` CLI tool and design-time EF Core context factory.
Example design-time context for PostgreSQL:

```csharp
internal class DesignTimeIdentityContext : IDesignTimeDbContextFactory<IdentityContext>
{
    public IdentityContext CreateDbContext(string[] args)
    {
        var builder = new DbContextOptionsBuilder<IdentityContext>();

        builder.UseNpgsql("fake",
                opt => opt.MigrationsAssembly(typeof(PostgresMigrations.PostgresMigrations).Assembly.FullName))
            // If using naming conventions in database options, it is necessary to use them here too
            .UseSnakeCaseNamingConvention()
            // Default schema is named identity by default, if overrided call this method too
            .UseDefaultSchema("identity");
        return new IdentityContext(builder.Options);
    }
}
```

Then use CLI tool to create migration:

```
dotnet ef migrations add MigrationName
```

Migrations are applied automatically on application startup, so there's no need to apply them from CLI.

To create migrations for multiple providers within one solution
see [Microsoft Docs](https://learn.microsoft.com/en-us/ef/core/managing-schemas/migrations/providers?tabs=dotnet-core-cli)
or [Example](../examples).

### Create database schema manually

#### Sqlite

Sqlite schema for manual creation can be found in [sqlite-schema.png](./sqlite-schema.png)

#### PostgreSQL

PostgreSQL schema for manual creation can be found in [postgres-schema.png](./postgres-schema.png)