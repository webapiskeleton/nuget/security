using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using WebApiSkeleton.SearchUtilities;
using WebApiSkeleton.Security.Contracts;
using WebApiSkeleton.Security.Contracts.Helpers;
using WebApiSkeleton.Security.Core.Models.DTO;
using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.UserIdentity;

namespace WebApiSkeleton.Security.Service.Controllers;

[Route("api/[controller]")]
public class UserController : SecurityControllerBase
{
    private readonly ISender _sender;
    private readonly IUserIdentity _userIdentity;

    public UserController(ISender sender, IUserIdentity userIdentity)
    {
        _sender = sender;
        _userIdentity = userIdentity;
    }

    [HttpGet]
    [Route("{id:int}")]
    public async Task<IActionResult> Get([FromRoute] int id)
    {
        var command = new GetUserQuery(id);
        var res = await _sender.Send(command);
        return MatchResult(res, x => x is null ? NotFound() : Ok(x));
    }

    [HttpGet]
    public async Task<IActionResult> GetAll([FromQuery] int pageNumber, [FromQuery] int pageSize)
    {
        var command = new ListUsersQuery(new ListParam<User>
        {
            PaginationInfo = new PaginationInfo(pageNumber, pageSize)
        });
        var res = await _sender.Send(command);
        return MatchResult(res);
    }

    [HttpGet]
    [Route("claim")]
    public async Task<IActionResult> GetClaims([FromQuery] int? userId)
    {
        var command = new ListUserClaimsQuery(userId ?? _userIdentity.UserId);
        var res = await _sender.Send(command);
        return MatchResult(res);
    }

    [HttpGet]
    [Route("role")]
    public async Task<IActionResult> GetRoles([FromQuery] int? userId)
    {
        var command = new GetUserRolesQuery(userId ?? _userIdentity.UserId);
        var res = await _sender.Send(command);
        return MatchResult(res);
    }

    [HttpGet]
    [Route("role-check")]
    public async Task<IActionResult> IsInRole([FromQuery] int? userId, [FromQuery] string roleName)
    {
        var command = new IsUserInRoleQuery(userId ?? _userIdentity.UserId, roleName);
        var res = await _sender.Send(command);
        return MatchResult(res);
    }

    [HttpGet]
    [Route("permissions-check")]
    public async Task<IActionResult> HasPermissions([FromQuery] int? userId,
        [FromQuery] IEnumerable<string> permissions)
    {
        var command = new UserHasPermissionsQuery(userId ?? _userIdentity.UserId, permissions);
        var res = await _sender.Send(command);
        return MatchResult(res);
    }

    [HttpPut]
    public async Task<IActionResult> UpdateUser([FromBody] User user)
    {
        var command = new UpdateUserCommand(user);
        var res = await _sender.Send(command);
        return MatchResult(res);
    }

    [HttpPost]
    [Route("password")]
    public async Task<IActionResult> SetPassword([FromQuery] int? userId, [FromBody] string password)
    {
        var command = new SetUserPasswordCommand(userId ?? _userIdentity.UserId, password);
        var res = await _sender.Send(command);
        return MatchResult(res);
    }

    [HttpPost]
    [Route("disable")]
    public async Task<IActionResult> DisableUser([FromQuery] int? userId)
    {
        var command = new DisableUserCommand(userId ?? _userIdentity.UserId);
        var res = await _sender.Send(command);
        return MatchResult(res);
    }

    [HttpPost]
    [Route("role")]
    public async Task<IActionResult> AddToRole([FromQuery] int? userId, [FromBody] string roleName)
    {
        var command = new AddUserToRoleCommand(userId ?? _userIdentity.UserId, roleName);
        var res = await _sender.Send(command);
        return MatchResult(res);
    }

    [HttpDelete]
    [Route("role")]
    public async Task<IActionResult> RemoveFromRole([FromQuery] int? userId, [FromBody] string roleName)
    {
        var command = new RemoveUserFromRoleCommand(userId ?? _userIdentity.UserId, roleName);
        var res = await _sender.Send(command);
        return MatchResult(res);
    }

    [HttpPost]
    [Route("claim")]
    public async Task<IActionResult> AddClaims([FromQuery] int? userId, [FromBody] IEnumerable<KeyValuePair<string, string>> userClaims)
    {
        var command = new AddUserClaimsCommand(userId ?? _userIdentity.UserId, userClaims.Select(x => new Claim(x.Key, x.Value)));
        var res = await _sender.Send(command);
        return MatchResult(res);
    }

    [HttpDelete]
    [Route("claim")]
    public async Task<IActionResult> RemoveClaims([FromQuery] int? userId, [FromBody] IEnumerable<string> userClaims)
    {
        var command = new RemoveUserClaimsCommand(userId ?? _userIdentity.UserId, userClaims);
        var res = await _sender.Send(command);
        return MatchResult(res);
    }

    [HttpGet]
    [Route("permission")]
    public async Task<IActionResult> ListPermissions([FromQuery] int? userId, [FromQuery] bool includeRolePermissions)
    {
        var command = new ListUserPermissionsQuery(userId ?? _userIdentity.UserId, includeRolePermissions);
        var res = await _sender.Send(command);
        return MatchResult(res);
    }

    [HttpPost]
    [Route("permission")]
    public async Task<IActionResult> AddPermission([FromQuery] int? userId,
        [FromBody] IEnumerable<PermissionDto> permissions)
    {
        var command = new AddPermissionsCommand(new UserLoginOrIdFinder(userId ?? _userIdentity.UserId), permissions);
        var res = await _sender.Send(command);
        return MatchResult(res);
    }

    [HttpDelete]
    [Route("permission")]
    public async Task<IActionResult> RemovePermission([FromQuery] int? userId,
        [FromBody] IEnumerable<PermissionDto> permissions)
    {
        var command =
            new RemovePermissionsCommand(new UserLoginOrIdFinder(userId ?? _userIdentity.UserId), permissions);
        var res = await _sender.Send(command);
        return MatchResult(res);
    }
}