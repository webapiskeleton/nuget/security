﻿using Microsoft.AspNetCore.Mvc;
using WebApiSkeleton.SearchUtilities;
using WebApiSkeleton.Security.Contracts;
using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Core.UserIdentity;

namespace WebApiSkeleton.Security.Service.Controllers;

[Route("api/[controller]")]
public class PermissionController : SecurityControllerBase
{
    private readonly ISender _sender;
    private readonly IUserIdentity _userIdentity;

    public PermissionController(ISender sender, IUserIdentity userIdentity)
    {
        _sender = sender;
        _userIdentity = userIdentity;
    }

    [HttpGet]
    public async Task<IActionResult> ListPermissions([FromQuery] int pageNumber, [FromQuery] int pageSize)
    {
        var command = new ListPermissionsQuery(new ListParam<Permission>
        {
            PaginationInfo = new PaginationInfo(pageNumber, pageSize)
        });
        var res = await _sender.Send(command);
        return MatchResult(res);
    }

    [HttpPost]
    public async Task<IActionResult> CreatePermission([FromQuery] string permissionName)
    {
        var command = new CreatePermissionCommand(permissionName);
        var res = await _sender.Send(command);
        return MatchResult(res);
    }

    [HttpPost]
    [Route("contract")]
    public async Task<IActionResult> AddContractPermission([FromQuery] string contractName,
        [FromBody] IEnumerable<string> permissions)
    {
        var command = new AddContractPermissionsCommand(contractName, permissions);
        var res = await _sender.Send(command);
        return MatchResult(res);
    }

    [HttpDelete]
    [Route("contract")]
    public async Task<IActionResult> RemoveContractPermission([FromQuery] string contractName,
        [FromBody] IEnumerable<string> permissions)
    {
        var command = new RemoveContractPermissionsCommand(contractName, permissions);
        var res = await _sender.Send(command);
        return MatchResult(res);
    }
}