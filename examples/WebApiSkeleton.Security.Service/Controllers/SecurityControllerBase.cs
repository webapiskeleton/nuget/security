using System.Security.Authentication;
using FluentValidation;
using LanguageExt.Common;
using Microsoft.AspNetCore.Mvc;
using WebApiSkeleton.Security.AuthorizationDescription;

namespace WebApiSkeleton.Security.Service.Controllers;

[ApiController]
public class SecurityControllerBase : ControllerBase
{
    protected IActionResult MatchResult<T>(Result<T> result, Func<T, IActionResult>? successResult = null)
    {
        return result.Match<IActionResult>(
            res => successResult is not null ? successResult(res) : Ok(res),
            exception => exception switch
            {
                AuthenticationException ae => Unauthorized(ae.Message),
                UnauthorizedAccessDescriptionException a => Forbid(),
                ValidationException ve => BadRequest(ve.Errors),
                _ => Problem("Unexpected error happened")
            });
    }
}