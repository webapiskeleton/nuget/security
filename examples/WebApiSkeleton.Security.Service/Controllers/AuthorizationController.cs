﻿using Microsoft.AspNetCore.Mvc;
using Riok.Mapperly.Abstractions;
using WebApiSkeleton.Security.Contracts;
using WebApiSkeleton.Security.Core.Models.DTO;
using WebApiSkeleton.Security.Core.Models.Output;
using WebApiSkeleton.Security.Service.Models;

namespace WebApiSkeleton.Security.Service.Controllers;

[Route("/api/[controller]")]
public sealed class AuthorizationController : SecurityControllerBase
{
    private readonly ISender _sender;
    private readonly ApplicationMapper _applicationMapper;

    public AuthorizationController(ISender sender)
    {
        _sender = sender;
        _applicationMapper = new ApplicationMapper();
    }

    [HttpPost]
    [Route("register")]
    public async Task<IActionResult> Register([FromBody] RegistrationCredentials credentials)
    {
        var securityRegistrationCredentials = _applicationMapper.Map(credentials);
        var command = new RegistrationCommand(securityRegistrationCredentials, Request.Headers.UserAgent!);
        var result = await _sender.Send(command);

        return MatchResult(result, x => Created("/api", x));
    }

    [HttpPost]
    [Route("login")]
    public async Task<IActionResult> Authorize([FromBody] AuthorizationCredentials credentials)
    {
        var authorizeCommand =
            new AuthorizationCommand(credentials.Login, credentials.Password, Request.Headers.UserAgent!);
        var result = await _sender.Send(authorizeCommand);

        return MatchResult(result);
    }

    [HttpPost]
    [Route("refresh")]
    public async Task<IActionResult> Refresh([FromBody] RefreshCredentials credentials)
    {
        var refreshCommand =
            new RefreshCommand(credentials.AccessToken, credentials.RefreshToken, Request.Headers.UserAgent!);
        var result = await _sender.Send(refreshCommand);

        return MatchResult(result);
    }
}

[Mapper]
public partial class ApplicationMapper
{
    public partial SecurityRegistrationCredentials Map(RegistrationCredentials credentials);
    public partial Role Map(RoleDto role);
}