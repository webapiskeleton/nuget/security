﻿using Microsoft.AspNetCore.Mvc;
using WebApiSkeleton.Security.Contracts;
using WebApiSkeleton.Security.Core.UserIdentity;
using WebApiSkeleton.Security.Core.Verification;

namespace WebApiSkeleton.Security.Service.Controllers;

[Route("api/[controller]")]
public class VerificationController : SecurityControllerBase
{
    private readonly ISender _sender;
    private readonly IUserIdentity _userIdentity;

    public VerificationController(ISender sender, IUserIdentity userIdentity)
    {
        _sender = sender;
        _userIdentity = userIdentity;
    }

    [HttpPost]
    [Route("generate-code")]
    public async Task<IActionResult> GenerateCode([FromQuery] int? userId)
    {
        var command = new GenerateVerificationCodeForUserCommand(userId ?? _userIdentity.UserId);
        var res = await _sender.Send(command);
        return MatchResult(res);
    }

    [HttpPost]
    [Route("use-code")]
    public async Task<IActionResult> UseVerificationCode([FromQuery] int? userId,
        [FromBody] VerificationCodeInformation codeInformation)
    {
        var command = new UseVerificationCodeCommand(userId ?? _userIdentity.UserId, codeInformation);
        var res = await _sender.Send(command);
        return MatchResult(res);
    }
}