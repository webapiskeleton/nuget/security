﻿using Microsoft.AspNetCore.Mvc;
using WebApiSkeleton.SearchUtilities;
using WebApiSkeleton.Security.Contracts;
using WebApiSkeleton.Security.Contracts.Helpers;
using WebApiSkeleton.Security.Core.Models.DTO;

namespace WebApiSkeleton.Security.Service.Controllers;

[Route("api/[controller]")]
public class RoleController : SecurityControllerBase
{
    private readonly ISender _sender;

    public RoleController(ISender sender)
    {
        _sender = sender;
    }

    [HttpGet]
    [Route("{id:int}")]
    public async Task<IActionResult> Get([FromRoute] int id)
    {
        var command = new GetRoleQuery(id);
        var res = await _sender.Send(command);
        return MatchResult(res, x => x is null ? NotFound() : Ok(x));
    }

    [HttpGet]
    public async Task<IActionResult> GetAll([FromQuery] int pageNumber, [FromQuery] int pageSize)
    {
        var command = new ListRolesQuery(new PaginationInfo(pageNumber, pageSize));
        var res = await _sender.Send(command);
        return MatchResult(res);
    }

    [HttpPost]
    public async Task<IActionResult> Create([FromQuery] string roleName)
    {
        var command = new CreateRoleCommand(roleName);
        var res = await _sender.Send(command);
        return MatchResult(res, role => Created($"/api/role/{role.Id}", role));
    }

    [HttpPatch]
    public async Task<IActionResult> UpdateName([FromQuery] string oldName, [FromQuery] string newName)
    {
        var command = new UpdateRoleNameCommand(oldName, newName);
        var res = await _sender.Send(command);
        return MatchResult(res);
    }

    [HttpGet]
    [Route("{id:int}/users")]
    public async Task<IActionResult> GetRoleUsers([FromRoute] int id, [FromQuery] int pageNumber,
        [FromQuery] int pageSize)
    {
        var command = new GetRoleUsersQuery(id, new PaginationInfo(pageNumber, pageSize));
        var res = await _sender.Send(command);
        return MatchResult(res);
    }

    [HttpGet]
    [Route("{id:int}/has-permissions")]
    public async Task<IActionResult> CheckHasPermissions([FromRoute] int id,
        [FromQuery] IEnumerable<string> permissionNames)
    {
        var command = new RoleHasPermissionsQuery(id, permissionNames);
        var res = await _sender.Send(command);
        return MatchResult(res);
    }
    
    [HttpGet]
    [Route("permission")]
    public async Task<IActionResult> ListPermissions([FromQuery] int roleId)
    {
        var command = new ListRolePermissionsQuery(roleId);
        var res = await _sender.Send(command);
        return MatchResult(res);
    }

    [HttpPost]
    [Route("permission")]
    public async Task<IActionResult> AddPermission([FromQuery] int roleId,
        [FromBody] IEnumerable<PermissionDto> permissions)
    {
        var command = new AddPermissionsCommand(new RoleNameOrIdFinder(roleId), permissions);
        var res = await _sender.Send(command);
        return MatchResult(res);
    }

    [HttpDelete]
    [Route("permission")]
    public async Task<IActionResult> RemovePermission([FromQuery] int roleId,
        [FromBody] IEnumerable<PermissionDto> permissions)
    {
        var command =
            new RemovePermissionsCommand(new RoleNameOrIdFinder(roleId), permissions);
        var res = await _sender.Send(command);
        return MatchResult(res);
    }
}