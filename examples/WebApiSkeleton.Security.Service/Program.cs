using System.Reflection;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using StackExchange.Redis;
using WebApiSkeleton.Contracts.Validation;
using WebApiSkeleton.DistributeLockUtilities.Settings;
using WebApiSkeleton.SearchSqlGeneration;
using WebApiSkeleton.SearchSqlGeneration.Postgres;
using WebApiSkeleton.Security;
using WebApiSkeleton.Security.Attributes;
using WebApiSkeleton.Security.Core.Extensions.DatabaseContextExtensions;
using WebApiSkeleton.Security.Core;
using WebApiSkeleton.Security.PostgresMigrations;
using WebApiSkeleton.Security.Service;
using WebApiSkeleton.Security.Core.Settings;
using WebApiSkeleton.Security.SqliteMigrations;
using WebApiSkeleton.Security.Core.UserIdentity;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
var configuration = builder.Configuration;
builder.Services.AddHttpContextAccessor();
builder.Services.AddScoped<IUserIdentityProvider, HttpUserIdentityProvider>();
builder.Services.AddSwaggerGen(c =>
{
    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        Description =
            "Get token from [POST] /login endpoint and paste it here with this template: Bearer {Your token}",
        Name = "Authorization",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.ApiKey,
        Scheme = "Bearer"
    });

    c.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type = ReferenceType.SecurityScheme,
                    Id = "Bearer"
                },
                Scheme = "oauth2",
                Name = "Bearer",
                In = ParameterLocation.Header,
            },
            new List<string>()
        }
    });
});
builder.Services.AddMediatR(x => { x.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly()); })
    .AddPermissionPipelineBehaviorsForRequestsInAssemblies(typeof(SecurityRequirementsAttribute).Assembly)
    .AddValidationPipelineBehaviorsFromAssembly(typeof(SecurityRequirementsAttribute).Assembly);

var tokenValidationParameters = new TokenValidationParameters
{
    ValidateIssuerSigningKey = true,
    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(configuration["JwtSettings:SigningKey"]!)),
    ValidateIssuer = false,
    ValidateAudience = false,
    ValidateLifetime = false,
    RequireExpirationTime = false,
    ClockSkew = TimeSpan.FromDays(1)
};
builder.Services.AddSecurityCore(config =>
    {
        config.UseJwtEncryption = false;
        config.PasswordEncryptionKey = configuration["EncryptionSettings:PasswordEncryptionKey"];
        config.RedisConnectionSettings = new RedisSettings
        {
            ConnectionMultiplexer = ConnectionMultiplexer.Connect(configuration.GetConnectionString("Redis")),
            DatabaseNumber = 1
        };
        config.JwtSettings = new JwtSettings
        {
            SigningKey = configuration["JwtSettings:SigningKey"]!,
            AccessTokenLifetime = TimeSpan.Parse(configuration["JwtSettings:AccessTokenLifetime"]!),
            RefreshTokenMonthLifetime = int.Parse(configuration["JwtSettings:RefreshTokenMonthLifetime"]!),
            EncryptionKey = configuration["JwtSettings:EncryptionKey"]!,
            Audience = "localhost",
            Issuer = "WebApiSkeleton.Security.Service",
            TokenValidationParameters = tokenValidationParameters
        };
        config.UseJwtEncryption = true;
        config.ValidationSettings = new ValidationSettings
        {
            Password = { RequireNonAlphanumeric = false }
        };

        var provider = configuration["ConnectionStrings:DatabaseProvider"] ?? "PostgreSQL";
        config.DatabaseOptions = new IdentityDatabaseOptions
        {
            OptionsAction = provider switch
            {
                "PostgreSQL" => options => options.UseNpgsql(configuration.GetConnectionString("Postgres"),
                        npgsqlOptions => npgsqlOptions
                            .UseQuerySplittingBehavior(QuerySplittingBehavior.SingleQuery)
                            .MigrationsHistoryTable("identity_migrations",
                                "identity")
                            .MigrationsAssembly(typeof(PostgresMigrations).Assembly.FullName))
                    .UseSnakeCaseNamingConvention()
                    .UseDefaultSchema("identity"),
                "Sqlite" => options => options.UseSqlite(configuration.GetConnectionString("Sqlite"),
                    sqliteOptions => sqliteOptions
                        .UseQuerySplittingBehavior(QuerySplittingBehavior.SingleQuery)
                        .MigrationsHistoryTable("IdentityMigrations")
                        .MigrationsAssembly(typeof(SqliteMigrations).Assembly.FullName)),
                _ => throw new Exception("Unknown provider")
            },
            UseRequestTransactions = true
        };
        config.IncludeUserClaimsInJwt = true;
    })
    .AddSecurityMediatR(config => config.StoreContractPermissions = true)
    .AddAuthentication(options =>
    {
        options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
        options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    })
    .AddJwtBearer(jwt =>
    {
        jwt.SaveToken = true;
        jwt.TokenValidationParameters = tokenValidationParameters;
    });
builder.Services.TryAddScoped<ISearchSqlConverter, PostgresSearchSqlConverter>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();