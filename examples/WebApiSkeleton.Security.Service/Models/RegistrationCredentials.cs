namespace WebApiSkeleton.Security.Service.Models;

public class RegistrationCredentials
{
    public string Username { get; set; } = null!;
    public string Email { get; set; } = null!;
    public string Password { get; set; } = null!;
    public required bool IsEnabled { get; set; }
    public string? PhoneNumber { get; set; }
    public string[] RoleNames { get; set; } = Array.Empty<string>();
}