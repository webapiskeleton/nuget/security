namespace WebApiSkeleton.Security.Service.Models;

public class RefreshCredentials
{
    public required string AccessToken { get; set; }
    public required string RefreshToken { get; set; }
}