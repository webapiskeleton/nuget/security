namespace WebApiSkeleton.Security.Service.Models;

public class AuthorizationCredentials
{
    public required string Login { get; set; }
    public required string Password { get; set; }
}