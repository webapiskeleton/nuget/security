using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using WebApiSkeleton.Security.Core.Data;

namespace WebApiSkeleton.Security.Service;

internal class DesignTimeIdentityContext : IDesignTimeDbContextFactory<IdentityContext>
{
    public IdentityContext CreateDbContext(string[] args)
    {
        var builder = new DbContextOptionsBuilder<IdentityContext>();

        switch (args[1])
        {
            case "PostgreSQL":
                builder.UseNpgsql("fake",
                        opt => opt.MigrationsAssembly(typeof(PostgresMigrations.PostgresMigrations).Assembly.FullName))
                    .UseSnakeCaseNamingConvention();
                return new IdentityContext(builder.Options);
            case "Sqlite":
                builder.UseSqlite("fake",
                    opt => opt.MigrationsAssembly(typeof(SqliteMigrations.SqliteMigrations).Assembly.FullName));
                return new IdentityContext(builder.Options);
            default:
                throw new Exception("No provider found");
        }
    }
}